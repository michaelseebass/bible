# README #

### About ###

An offline bible app for Android focusing on UI design and efficiency/speed

### Local project setup ###

Just clone the repository locally and open in Android Studio (nothing too special with this project)

Please setup your environment to use UTF-8 encoding.

### Contribution guidelines ###

* Alway use the log methods via the class `BibleLog`

***Example:***

instead of

```
#!java

Log.i(TAG, "msg");
```
use `BibleLog`

```
#!java

BibleLog.i(TAG, "msg");
```

* Encoding: `UTF-8`

* Branch of `develop`, use a prefix and naming for your branch (eg. `fix/your_hotfix` or `feature/my_new_feature`) and create a Pull request with reasonable fescription of what this PR does.

* Request access: write an email to request access: *development.michael.seebass@gmail.com*

### Who do I talk to? ###

* Repo owner/admin: Michael Seeba�