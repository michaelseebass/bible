package de.mseebass.bible.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "history_entries")
data class HistoryEntry (
        @ColumnInfo(name = "bible_id") val bibleId: Long,
        @ColumnInfo(name = "verse_id") val verseId : Long,
        @ColumnInfo(name = "timestamp") val timeStamp : Long,
        @ColumnInfo(name = "counter") val counter : Int
        )
{

    @PrimaryKey(autoGenerate = true)
    var id : Long? = null

    @Ignore
    lateinit var verse: Verse

    override fun toString(): String {
        val verseStr = if (this::verse.isInitialized) {
            "verse = $verse"
        } else {
            "verseId = $verseId"
        }
        val timeStr = SimpleDateFormat.getDateTimeInstance().format(Date(timeStamp))
        return "HistoryEntry(bibleId = $bibleId, $verseStr, time = $timeStr)"
    }

}