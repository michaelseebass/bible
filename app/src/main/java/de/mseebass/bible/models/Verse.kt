package de.mseebass.bible.models

import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import de.mseebass.bible.providers.BibleProvider.Bookmarks
import de.mseebass.bible.utils.BibleLog
import java.util.*

data class Verse(var id: Long,
                 var bibleId: Long,
                 var bookNumber: Int,
                 var bookName: String,
                 var chapterNumber: Int,
                 var number: Int,
                 var text: String) : Parcelable {

    private val TAG = "Verse"

    // --- only used for bookmarks ---
    var bookMarkId: Long = -1
    var bibleShortName: String? = null
    var labels: ArrayList<String>? = ArrayList()
    var highLightColor = Bookmarks.COLOR_NONE
    var notes: String? = null


    constructor(source: Parcel) : this (
            source.readLong(),
            source.readLong(),
            source.readInt(),
            source.readString()?:"",
            source.readInt(),
            source.readInt(),
            source.readString()?:"") {

        bookMarkId = source.readLong()
        bibleShortName = source.readString() ?: ""
        source.readStringList(labels!!)
        highLightColor = source.readInt()
    }


    /**
     * Returns the number of labels associated with this verse
     *
     * @return integer the number of labels
     */
    fun getLabelCount(): Int {
        return if (labels != null) {
            labels!!.size
        } else 0
    }

    /**
     * Wird nur für Lesezeichen verwendet
     */
    fun getCommaSeparatedLabels(): String? {
        if (labels == null || labels!!.size == 0) {
            return null
        }
        var labelString = labels!![0]
        for (i in 1 until labels!!.size) {
            labelString += "," + labels!![i]
        }
        return labelString
    }

    /**
     * Returns a locator for this verse
     *
     * @return a string formated like "Bookname X, Y"
     */
    fun getLocator(): String {
        return "$bookName $chapterNumber, $number"
    }

    /**
     * Returns a locator for this verse, with the book name shortened to 3 letters
     *
     * @return a string formated like "Bkn X, Y"
     */
    fun getShortLocator(): String {
        // 1. Mose // 1 Samuel etc --> not only first 3, but "1. Mos"
        var endIndex = 3
        try {
            if (Character.isDigit(bookName.toCharArray()[0])) {
                endIndex = 3 + bookName.indexOf(' ')
            }
        } catch (e : Exception) {
            BibleLog.w(TAG, "Failed to parse bookname for leading numeric: ", e)
        }


        return bookName!!.substring(0, endIndex) + " " + chapterNumber + ", " + number
    }

    // verse in bold
    fun getSpannable(): Spannable {
        val src = SpannableString("${getLocator()} $text")
        src.setSpan(StyleSpan(android.graphics.Typeface.BOLD), 0, getLocator().length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return src
    }

    fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getLocator())
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        return shareIntent
    }



    /**
     * Wird nur für Lesezeichen verwendet
     */
    @Throws(DuplicateLabelException::class, IllegalLabelFormatException::class)
    fun addLabel(newLabel: String) {
        if (newLabel.contains(",")) {
            throw IllegalLabelFormatException("Illegal character ',' in Label: '$newLabel'")
        }
        for (checkLabel in labels!!) {
            if (checkLabel == newLabel) {
                throw DuplicateLabelException("Verse already contains label: '$newLabel'")
            }
        }
        this.labels!!.add(newLabel)
    }

    /**
     * Wird nur für Lesezeichen verwendet
     */
    fun setLabels(commaSeparatedLabels: String?) {
        if (commaSeparatedLabels != null) {
            val array = commaSeparatedLabels.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            this.labels!!.addAll(Arrays.asList(*array))
        }
    }

    /**
     * Wird nur für Lesezeichen verwendet
     */
    fun removeLabel(label: String): Boolean {
        return this.labels!!.remove(label)
    }

    /**
     * Wird nur für Lesezeichen verwendet
     */
    fun containsLabel(label: Label): Boolean {
        return containsLabel(label.name)
    }

    /**
     * Wird nur für Lesezeichen verwendet
     */
    fun containsLabel(label: String): Boolean {
        if (labels == null || labels!!.size == 0) {
            return false
        }
        for (i in labels!!.indices) {
            val currentLabelName = labels!![i]
            if (currentLabelName == label) {
                return true
            }
        }
        return false
    }

    fun matches(other: Verse): Boolean {
        return (this.bibleId == other.bibleId
                && this.bookNumber == other.bookNumber
                && this.chapterNumber == other.chapterNumber
                && this.number == other.number)
    }

    fun mergeLabelsAndColor(importedVerse: Verse) { // TODO
        if (highLightColor == Bookmarks.COLOR_NONE) {
            highLightColor = importedVerse.highLightColor
        }

        for (importLabel in importedVerse.labels!!) {
            try {
                addLabel(importLabel)
            } catch (ignored: DuplicateLabelException) {
                // if this label already exists, we just ignore the exception
            } catch (e: IllegalLabelFormatException) {
                BibleLog.w(TAG, "Error importing $importedVerse - label: $importLabel", e)
            }
        }
        if (notes.isNullOrBlank() && !importedVerse.notes.isNullOrBlank()) {
            notes = importedVerse.notes
        }
    }

    override fun toString(): String {
        return "[$bibleShortName $bookName $chapterNumber, $number]"
    }

    override fun equals(obj: Any?): Boolean {
        if (obj !is Verse) {
            return false
        }
        val other = obj as Verse?
        return matches(other!!) && bookMarkId == other.bookMarkId
    }

    override fun hashCode(): Int {
        return (bibleId.toInt() * 31 // TODO

                + bookNumber * 31
                + chapterNumber * 31
                + number * 31
                + bookMarkId.toInt() * 31)
    }

    // Parcel

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeLong(bibleId)
        dest.writeInt(bookNumber)
        dest.writeString(bookName)
        dest.writeInt(chapterNumber)
        dest.writeInt(number)
        dest.writeString(text)

        dest.writeLong(bookMarkId)
        dest.writeString(bibleShortName)
        dest.writeStringList(labels)
        dest.writeInt(highLightColor)
    }

    /**
     * Exception thrown when a new label is already contained in the label list of the verse
     *
     * @author Michael Seebaß B.Eng.
     */
    class DuplicateLabelException
    /**
     * Constructs a new [IllegalLabelFormatException].
     *
     * @param detailMsg String - the detail message for this exception
     */
    (detailMsg: String) : Exception(detailMsg) {
        companion object {
            private val serialVersionUID = 1L
        }
    }


    /**
     * Exception thrown when a new label contains a comma character (',').
     * The comma character is used as a separator between labels for a verse in the database
     * and is therefore considered an illegal character for label names.
     *
     * @author Michael Seebaß B.Eng.
     */
    class IllegalLabelFormatException
    /**
     * Constructs a new [IllegalLabelFormatException].
     *
     * @param detailMsg String - the detail message for this exception
     */
    (detailMsg: String) : Exception(detailMsg) {
        companion object {
            private val serialVersionUID = 2L
        }
    }

    companion object CREATOR: Parcelable.Creator<Verse> {
        override fun createFromParcel(source: Parcel): Verse {
            return Verse(source)
        }

        override fun newArray(size: Int): Array<Verse?> {
            return arrayOfNulls(size)
        }
    }

}