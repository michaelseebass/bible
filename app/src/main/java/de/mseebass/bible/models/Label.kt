package de.mseebass.bible.models

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import de.mseebass.bible.R
import de.mseebass.bible.providers.BibleProvider.Bookmarks
import java.util.*

data class Label(val name : String, val bookMarks : Vector<Verse>) : Parcelable {

    var isMarkedAsDeleted = false

    constructor(name: String) : this (name, Vector())

    constructor(otherLabel: Label) : this (otherLabel.name, Vector(otherLabel.bookMarks))

    constructor(source: Parcel) : this(source.readString() ?: "", Vector() ){
        val verseArray = source.createTypedArray(Verse.CREATOR)
        for (i in verseArray!!.indices) {
            bookMarks.add(verseArray[i])
        }
        this.isMarkedAsDeleted = source.readByte().toInt() != 0
    }

    fun getDisplayName(context: Context): String {
        return if (name == Bookmarks.LABEL_GENERAL) {
            context.getString(R.string.bookmarks_default_group_name)
        } else name
    }

    fun size(): Int {
        return bookMarks.size
    }

    fun getBookMark(index: Int): Verse {
        return bookMarks[index]
    }

    fun addBookMark(verse: Verse) {
        this.bookMarks.add(verse)
    }

    // --- Parcel ---

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(name)
        val bookmarks = arrayOfNulls<Verse>(bookMarks.size)
        for (i in bookMarks.indices) {
            bookmarks[i] = bookMarks[i]
        }
        dest.writeTypedArray<Verse>(bookmarks, flags)
        dest.writeByte((if (isMarkedAsDeleted) 1 else 0).toByte())
    }

    companion object CREATOR: Parcelable.Creator<Verse> {
        override fun createFromParcel(source: Parcel): Verse {
            return Verse(source)
        }

        override fun newArray(size: Int): Array<Verse?> {
            return arrayOfNulls(size)
        }
    }
}