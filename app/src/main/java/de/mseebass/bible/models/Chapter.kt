package de.mseebass.bible.models

import android.os.Parcel
import android.os.Parcelable
import de.mseebass.bible.utils.BookmarkUtils
import java.util.*

data class Chapter(var number: Int, var bookNumber: Int, var verses : Vector<Verse>) : Parcelable {



//    constructor(number: Int, bookNumber: Int, verses: Vector<Verse>) {
//        this.number = number
//        this.bookNumber = bookNumber
//        this.verses = verses
//    }

//    constructor(number: Int, bookNumber: Int) {
//        this.number = number
//        this.bookNumber = bookNumber
//    }

    constructor(source: Parcel) : this (source.readInt(), source.readInt(), Vector()) {
        val verseArray = source.createTypedArray(Verse.CREATOR)
        for (i in verseArray!!.indices) {
            verses.add(verseArray[i])
        }
    }


    fun size(): Int {
        return verses.size
    }

    fun getVerse(verseIndex: Int): Verse {
        return verses[verseIndex]
    }

    fun addVerse(verse: Verse) {
        this.verses.add(verse)
    }

    // --- Parcelable ---

    /**
     * Searches for matches of the chapters verses and the supplied list of verses and sets the appropriate data for the chapters verse
     *
     * @param bookmarkedVerses the verses containing bookmark data
     * @see BookmarkUtils.setBookMarkedVerses
     */
    @Synchronized
    fun setBookMarkedVerses(bookmarkedVerses: Vector<Verse>) {
        BookmarkUtils.setBookMarkedVerses(verses, bookmarkedVerses)
    }

    // --- Parcel ---

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(number)
        dest.writeInt(bookNumber)
        val verses = arrayOfNulls<Verse>(this.verses.size)
        for (i in this.verses.indices) {
            verses[i] = this.verses[i]
        }
        dest.writeTypedArray<Verse>(verses, flags)
    }

    companion object CREATOR : Parcelable.Creator<Chapter> {
        override fun createFromParcel(source: Parcel): Chapter {
            return Chapter(source)
        }

        override fun newArray(size: Int): Array<Chapter?> {
            return arrayOfNulls(size)
        }
    }
}