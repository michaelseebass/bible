package de.mseebass.bible.models

import android.os.Parcel
import android.os.Parcelable

//@Parcelize
data class Book(var bibleId: Long, var number: Int, var name: String?, var size: Int) : Parcelable {

    constructor(source: Parcel) : this (
        source.readLong(), source.readInt(), source.readString(), source.readInt())


    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, arg1: Int) {
        dest.writeLong(bibleId)
        dest.writeInt(number)
        dest.writeString(name)
        dest.writeInt(size)
    }

    companion object CREATOR : Parcelable.Creator<Book> {
        override fun createFromParcel(parcel: Parcel): Book {
            return Book(parcel)
        }

        override fun newArray(size: Int): Array<Book?> {
            return arrayOfNulls(size)
        }
    }

}