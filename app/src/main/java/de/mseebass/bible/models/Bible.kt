package de.mseebass.bible.models

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class Bible : Parcelable {
    var id: Long = 0
        private set
    var name: String? = null
        private set
    var shortName: String? = null
        private set
    var language: String? = null
        private set
    var books = Vector<Book>()

    constructor(id: Long, name: String, shortName: String, language: String) {
        this.id = id
        this.name = name
        this.shortName = shortName
        this.language = language
    }

    constructor(source: Parcel) {
        this.id = source.readLong()
        this.name = source.readString()
        this.shortName = source.readString()
        this.language = source.readString()
        //		source.readTypedList(mBooks, Book.CREATOR);
        val bookArray = source.createTypedArray(Book)
        for (i in bookArray!!.indices) {
            books.add(bookArray[i])
        }
    }

    // Parcel

    fun getBook(bookIndex: Int): Book {
        return books[bookIndex]
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(name)
        dest.writeString(shortName)
        dest.writeString(language)
        //		dest.writeTypedList(mBooks);
        val books = arrayOfNulls<Book>(this.books.size)
        for (i in this.books.indices) {
            books[i] = this.books[i]
        }
        dest.writeTypedArray<Book>(books, flags)
    }

    //val TAG = "Bible"
    companion object  CREATOR: Parcelable.Creator<Bible> {
        override fun createFromParcel(source: Parcel): Bible {
            return Bible(source)
        }

        override fun newArray(size: Int): Array<Bible?> {
            return arrayOfNulls(size)
        }
    }

}