package de.mseebass.bible.logic

import de.mseebass.bible.models.HistoryEntry
import kotlin.math.abs

interface IHistoryEntryComparator {
    fun areHistoryEntriesSimilar(entry1: HistoryEntry, entry2: HistoryEntry) : Boolean
    fun isHistoryEntryStartOfSameBook(entryNew: HistoryEntry, entry2: HistoryEntry) : Boolean
}

class HistoryEntryComparatorImpl: IHistoryEntryComparator {
    override fun areHistoryEntriesSimilar(entry1: HistoryEntry, entry2: HistoryEntry) : Boolean {
        if (entry1.bibleId != entry2.bibleId)
            return false

        if (entry1.verse.bookNumber != entry2.verse.bookNumber)
            return false

        if (entry1.verse.chapterNumber != entry2.verse.chapterNumber)
            return false

        val diff = abs(entry1.verse.number - entry2.verse.number)
        if (diff > 10)
            return false

        return true
    }

    override fun isHistoryEntryStartOfSameBook(entryNew: HistoryEntry, entry2: HistoryEntry) : Boolean {
        if (entryNew.bibleId != entry2.bibleId)
            return false

        if (entryNew.verse.bookNumber != entry2.verse.bookNumber)
            return false

        return entry2.verse.chapterNumber == 1
    }
}