package de.mseebass.bible.utils

import android.util.Base64
import org.apache.commons.codec.binary.Hex
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.IOException

import java.io.UnsupportedEncodingException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Created by mseebass on 14.07.2016.
 */
object EncodingUtils {

    /**
     * Log-TAG
     */
    private val TAG = EncodingUtils::class.java.simpleName


    @Throws(NoSuchAlgorithmException::class)
    fun getMD5(source: ByteArray): ByteArray {
        val messageDigest = MessageDigest.getInstance("MD5")
        return messageDigest.digest(source)
    }

    @Throws(NoSuchAlgorithmException::class, IOException::class)
    fun getMD5(file: File) : ByteArray {
        val byteArray = FileUtils.readFileToByteArray(file)
        return getMD5(byteArray)
    }

//    public static byte[] getMD5(File file) throws NoSuchAlgorithmException, IOException {
//        byte[] bytes = FilesUtils.readFileToByteArray(file);
//        return getMD5(bytes);
//    }

    fun getHex(source : ByteArray) : String {
        return String(Hex.encodeHex(source))
    }

    @Throws(NoSuchAlgorithmException::class, IOException::class)
    fun getHexEncodedMD5(source : ByteArray) : String {
        val md5Bytes = getMD5(source)
        return getHex(md5Bytes)
    }

    @Throws(NoSuchAlgorithmException::class, IOException::class)
    fun getHexEncodedMD5(file : File) : String {
        val md5Bytes = getMD5(file)
        return getHex(md5Bytes)
    }

    //
    //    public static String getHexEncodedMD5(byte[] source) throws NoSuchAlgorithmException {
    //        byte[] md5Bytes = getMD5(source);
    //        return getHex(md5Bytes);
    //    }

    //    public static String getHexEncodedMD5(File file) throws NoSuchAlgorithmException, IOException {
    //        byte[] md5Bytes = getMD5(file);
    //        return getHex(md5Bytes);
    //    }

    fun encodeBase64(input: ByteArray?): ByteArray? {
        return if (input == null) {
            null
        } else Base64.encode(input, Base64.DEFAULT)
    }

    @Throws(UnsupportedEncodingException::class)
    fun encodeBase64ToString(input: ByteArray?): String? {
        return if (input == null) {
            null
        } else Base64.encodeToString(input, Base64.DEFAULT)
//        return new String(encodeBase64(input), "UTF-8");
    }

    @Throws(UnsupportedEncodingException::class)
    fun encodeBase64ToString(input: String?): String? {
        return if (input == null) {
            null
        } else encodeBase64ToString(input.toByteArray(charset("UTF-8")))
//        return new String(encodeBase64(input.getBytes("UTF-8")), "UTF-8");
    }

    fun decodeBase64(str: String?): ByteArray? {
        return if (str == null) {
            null
        } else Base64.decode(str, Base64.DEFAULT)
    }

    @Throws(UnsupportedEncodingException::class)
    fun decodeBase64ToString(str: String?): String? {
        return if (str == null) {
            null
        } else String(decodeBase64(str)!!, charset("UTF-8"))
    }

}