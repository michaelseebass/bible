package de.mseebass.bible.utils

import android.content.Context
import com.github.marlonlom.utilities.timeago.TimeAgo
import java.util.*

fun Date.getDescription(c: Context) : String {
    return this.time.getDescription(c)
}

fun Long.getDescription(c: Context) : String {

    //return DateUtils.getRelativeDateTimeString(c, this, DateUtils.MINUTE_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL).toString();
    //return getTimeAgo(this)
    return TimeAgo.using(this) // TODO lizenz https://github.com/marlonlom/timeago
}


// this is the google way from IO app
/*
// GOOGLE
const val SECOND_MILLIS = 1000
const val MINUTE_MILLIS = 60 * SECOND_MILLIS
const val HOUR_MILLIS = 60 * MINUTE_MILLIS
const val DAY_MILLIS = 24 * HOUR_MILLIS

// GOOGLE
private fun getTimeAgo(timeStamp: Long) : String {
    var time = timeStamp
    if (time < 1000000000000L) {
        // if timestamp given in seconds, convert to millis
        time *= 1000;
    }


    val now: Long = System.currentTimeMillis()
    if (time > now || time <= 0) {
        return "(Invalid date)"
    }

    // TODO: localize
    val diff = now - time
    return if (diff < MINUTE_MILLIS) {
        "just now"
    } else if (diff < 2 * MINUTE_MILLIS) {
        "a minute ago"
    } else if (diff < 50 * MINUTE_MILLIS) {
        (diff / MINUTE_MILLIS).toString() + " minutes ago"
    } else if (diff < 90 * MINUTE_MILLIS) {
        "an hour ago"
    } else if (diff < 24 * HOUR_MILLIS) {
        (diff / HOUR_MILLIS).toString() + " hours ago"
    } else if (diff < 48 * HOUR_MILLIS) {
        "yesterday"
    } else {
        (diff / DAY_MILLIS).toString() + " days ago"
    }
}
*/