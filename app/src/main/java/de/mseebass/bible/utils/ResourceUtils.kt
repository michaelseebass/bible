package de.mseebass.bible.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import de.mseebass.bible.R
import java.util.*

object ResourceUtils {

    val TAG = "ResourceUtils"

    private val HIGHLIGHTED_VERSES_ALPHA_DISPLAY = 0.75f
    private val HIGHLIGHTED_VERSES_ALPHA_TEXTBACKGROUND = 0.5f

    fun setStatusBarColorDefault(activity: Activity) {
        //set status bar color
        val color = activity.resources.getColor(android.R.color.transparent)
        activity.window.statusBarColor = color
    }

    fun setStatusBarColorActionMode(activity: Activity) {
        //set status bar color
        val color = activity.resources.getColor(R.color.color_primary_dark_actionmode)
        activity.window.statusBarColor = color
    }

    fun setStatusBarColorSolid(activity: Activity) {
        //set status bar color
        val colorResId = getThemedResourceId(R.attr.colorPrimaryDark, activity)
        val color = activity.resources.getColor(colorResId)
        activity.window.statusBarColor = color
    }

    /**
     * Converts a given DPI size into pixels, based on the device display density
     *
     * @param dpi     density independent pixels
     * @param context a context
     * @return int the number of display pixels
     */
    fun getPixel(dpi: Int, context: Context): Int {
        return (dpi * context.resources.displayMetrics.density + 0.5f).toInt()
    }

    /**
     * Returns the themed resource ID for a given attribute
     * @param attr
     * @param context
     * @return
     */
    fun getThemedResourceId(attr: Int, context: Context): Int {
        val attrTypedArray = context.theme.obtainStyledAttributes(intArrayOf(attr))
        val resourceId = attrTypedArray.getResourceId(0, 0)
        attrTypedArray.recycle()
        return resourceId
    }


    fun getSolidHighlightColorsArray(context: Context): ArrayList<Int> {
        val res = context.resources
        val colorValues = res.obtainTypedArray(R.array.highlight_colors)

        val colorList = ArrayList<Int>()
        for (i in 0 until colorValues.length()) {
            val color = colorValues.getColor(i, 0)
            colorList.add(color)
        }
        colorValues.recycle()

        return colorList
    }

    fun getDisplayHighlightColor(originalColor: Int): Int {
        return adjustAlpha(originalColor, HIGHLIGHTED_VERSES_ALPHA_DISPLAY)
    }

    fun getTextBackgroundHighlightColor(originalColor: Int): Int {
        return adjustAlpha(originalColor, HIGHLIGHTED_VERSES_ALPHA_TEXTBACKGROUND)
    }

    private fun adjustAlpha(color: Int, factor: Float): Int {
        val alpha = Math.round(Color.alpha(color) * factor)
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)
        return Color.argb(alpha, red, green, blue)
    }
}
