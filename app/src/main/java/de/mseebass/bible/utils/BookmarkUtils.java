package de.mseebass.bible.utils;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import de.mseebass.bible.models.Verse;

public class BookmarkUtils {

    /** Log-TAG */
    public static final String TAG = BookmarkUtils.class.getSimpleName();

    /**
     * Sub path for the backup of the bookmarks/highlighted verses database file on the external storage
     */
    private static final String BACKUP_DIRECTORY_PATH = "/Bible/";

    /**
     * File name for the backup of the bookmarks/highlighted verses database file on the external storage
     */
    private static final String BACKUP_FILE_NAME_PREFIX = "bible_backup_";

    public static String getBackupFileName() {
        return getBackupFileName(new Date(), Locale.getDefault());
    }

    public static String getBackupFileName(Date date, Locale locale) {
        DateFormat sdf = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, locale);
        String dateTimeStr = sdf.format(date);
        dateTimeStr = dateTimeStr.replaceAll("[ /.:]", "_");
        dateTimeStr = dateTimeStr.replaceAll(",", "");
        return BACKUP_FILE_NAME_PREFIX + dateTimeStr;
    }

    /**
     * Searches for matches of the chapters verses and the supplied list of verses and sets the
     * appropriate data for the chapters verse
     * @param verses the list of verses to set the bookmarks on
     * @param bookmarkedVerses the verses containing bookmark data
     */
    public static synchronized void setBookMarkedVerses(Vector<Verse> verses, Vector<Verse> bookmarkedVerses) {
        if (verses == null || bookmarkedVerses == null) {
            return;
        }
        for (int i = 0; i < verses.size(); i++) {
            Verse verse = verses.get(i);
            // nach Labeln für diesen Vers suchen
            for (int j = 0; j < bookmarkedVerses.size(); j++) {
                Verse currentBookmarkedVerse = bookmarkedVerses.get(j);
                // bei Übereinstimmung
                if (verse.getBibleId() == currentBookmarkedVerse.getBibleId() && verse.getId() == currentBookmarkedVerse.getId()) {
                    verse.setBookMarkId(currentBookmarkedVerse.getBookMarkId());
                    verse.setBibleShortName(currentBookmarkedVerse.getBibleShortName());
                    verse.setLabels(currentBookmarkedVerse.getCommaSeparatedLabels());
                    verse.setHighLightColor(currentBookmarkedVerse.getHighLightColor());
                    verse.setNotes(currentBookmarkedVerse.getNotes());
                }
            }
        }
    }

    /**
     * Returns a directory that the bookmarks/highlighted verses database file can be copied to when it is backed up.
     * The directory will be located on the external storage.
     * If the directory doesn't exist, it is created.
     * @return An existing directory, not {@code null}
     * @throws IOException when the directory could not be created. This might be due to unavailability of the external storage.
     * @see BookmarkUtils#BACKUP_DIRECTORY_PATH
     */
    private static File getBackupDirectory() throws IOException {
        File backupDirectory = new File(Environment.getExternalStorageDirectory(), BACKUP_DIRECTORY_PATH);
        backupDirectory.mkdirs();
        if (!backupDirectory.exists()) {
            throw new IOException("Backup directory could not be created. External storage might be unavailable.");
        }
        return backupDirectory;
    }

    /**
     * Returns a non-existing File (not {@code null} that the bookmarks/highlighted verses database file can be copied to.
     * The File is located in the backup directory on external storage. If the directory doesnt exist, it is created.
     * @return backup File. Not {@code null}, but not existing on storage
     * @throws IOException IOException when the directory could not be created. This might be due to unavailability of the external storage.
     */
    public static File getBackupFile() throws IOException {
        File backupDirectory = getBackupDirectory();
        File backupFile = new File(backupDirectory, getBackupFileName());
        if (backupFile.exists()) {
            /*
            this code could be used to re-use an already exported file
            however, for now the file is deleted anyway
            */
//            Date exportedFileLastModified = new Date(destinationFile.lastModified());
//            Date bookmarksDbLastModified = new Date(Settings.getBookmarksLastEdited(mContext));
//            if (exportedFileLastModified.before(bookmarksDbLastModified)) {
//            } else {
//            }
            backupFile.delete();
        }
        return backupFile;
    }

    /**
     * Copies the bytes from one file to an other
     * @param sourceFile file to be copied. Not {@code null}
     * @param destinationFile file to copy to. Must not exist. Not {@code null}
     * @throws IOException Unknown error regarding files/streams
     */
    public static void copyFile(File sourceFile, File destinationFile) throws IOException {
        // Open file channels
        FileChannel source = new FileInputStream(sourceFile).getChannel();
        FileChannel destination = new FileOutputStream(destinationFile).getChannel();
        // Copy
        destination.transferFrom(source, 0, source.size());
        // Close streams
        source.close();
        destination.close();
    }

//    private File copyFileToExternal() {
//        String fileName = getDatabasePath(BibleProvider.FILE_NAME_BOOKMARKS).getAbsolutePath();
//        try {
//            File newDirectory = new File(Environment.getExternalStorageDirectory(), "/Bible/");
//            boolean created = newDirectory.mkdirs();
//            BibleLog.d(TAG,  "created: " + created);
//            BibleLog.d(TAG,  "exists: " + newDirectory.exists());
//            BibleLog.d(TAG, Environment.getExternalStorageState());
//
//            FileInputStream inputStream = new FileInputStream(fileName);
//            FileOutputStream outputStream = new FileOutputStream(newDirectory.getAbsolutePath() + "/backup");
//
//            byte[] buffer = new byte[1024];
//            int len1 = 0;
//            while ((len1 = inputStream.read(buffer)) != -1) {
//                outputStream.write(buffer, 0, len1);
//            }
//            inputStream.close();
//            outputStream.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }


}
