package de.mseebass.bible.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import de.mseebass.bible.R;

public class Settings {

    public static final String TAG = "Settings";

    private static final String KEY_LAST_KNOWN_APP_VERSION = "pref_key_last_known_app_version";
    private static final String KEY_BIBLE_ID = "pref_key_bible_id";
    private static final String KEY_BOOK_INDEX = "pref_key_book_index";
    private static final String KEY_CHAPTER_INDEX = "pref_key_chapter_index";
    private static final String KEY_VERSE_INDEX = "pref_key_verse_index";
    private static final String KEY_KEEP_SCREEN_ON = "pref_key_keep_screen_on";
    private static final String KEY_FONT_SCALE = "pref_key_font_scale";
    private static final String KEY_THEME = "pref_key_theme";
    private static final String KEY_SHOW_CASE_ACKNOWLEDGED_PREFIX = "pref_key_show_case_acknowledged_";
    private static final String KEY_NAVIGATION_DRAWER_ACKNOWLEDGED = "pref_key_nav_drawer_acknowledged";
    private static final String KEY_BOOKMARKS_DB_LAST_EDITED = "pref_key_bookmarks_db_last_edited";

    @Deprecated
    public static int SHOWCASE_READER_ACTION_ITEM_OPEN = 0;
    @Deprecated
    public static int SHOWCASE_READER_PAGER_SWIPE = 1;
    @Deprecated
    private static final String KEY_APP_OLD_SHORTCUT_REMOVED = "pref_key_app_old_shortcut_removed";


    public static boolean isKeepScreenOnEnabled(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(KEY_KEEP_SCREEN_ON, false);
    }

    public static int getFontScale(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String scale = sharedPref.getString(KEY_FONT_SCALE, "1");
        return Integer.valueOf(scale);
    }

    public static String getFontScaleDescription(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int scale = Integer.valueOf(sharedPref.getString(KEY_FONT_SCALE, "1"));
        return context.getResources().getStringArray(R.array.font_scale_strings)[scale];
    }

    public static int getThemeId(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String scale = sharedPref.getString(KEY_THEME, "0");
        if (Integer.valueOf(scale) == 0) {
            return R.style.Bible_Theme_Light;
        } else {
            return R.style.Bible_Theme_Dark;
        }
    }

    public static String getThemeDescription(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int selection = Integer.valueOf(sharedPref.getString(KEY_THEME, "0"));
        return context.getResources().getStringArray(R.array.theme_strings)[selection];
    }

    public static boolean isNavigationDrawerAcknowledged(Context context) {
        if (context == null) {
            return true;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(KEY_NAVIGATION_DRAWER_ACKNOWLEDGED, false);
    }

    public static void saveNavigationDrawerAcknowledged(Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Editor
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // sichern
        editor.putBoolean(KEY_NAVIGATION_DRAWER_ACKNOWLEDGED, true);
        // Aenderungen bestaetigen
        editor.commit();
    }

    // ### BIBLE-ID ###

    public static long getBibleId(Context context) {
        if (context == null) {
            return -1;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getLong(KEY_BIBLE_ID, context.getResources().getInteger(R.integer.default_bible_id));
    }

    public static void saveBibleId(long bibleId, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Editor
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // Id sichern
        editor.putLong(KEY_BIBLE_ID, bibleId);
        // Aenderungen bestaetigen
        editor.commit();
    }

    // ### BOOK-INDEX ###

    public static int getBookIndex(Context context) {
        if (context == null) {
            return 0;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(KEY_BOOK_INDEX, 0);
    }

    public static void saveBookIndex(int bookIndex, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Editor
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // Id sichern
        editor.putInt(KEY_BOOK_INDEX, bookIndex);
        // Aenderungen bestaetigen
        editor.commit();
    }

    // ### CHAPTER-INDEX ###

    public static int getChapterIndex(Context context) {
        if (context == null) {
            return 0;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(KEY_CHAPTER_INDEX, 0);
    }

    public static void saveChapterIndex(int chapterIndex, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Editor
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // Id sichern
        editor.putInt(KEY_CHAPTER_INDEX, chapterIndex);
        // Aenderungen bestaetigen
        editor.commit();
    }

    // ### CHAPTER-INDEX ###

    public static int getVerseIndex(Context context) {
        if (context == null) {
            return 0;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(KEY_VERSE_INDEX, -1);
    }

    public static void saveVerseIndex(int verseIndex, Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Editor
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // Id sichern
        editor.putInt(KEY_VERSE_INDEX, verseIndex);
        // Aenderungen bestaetigen
        editor.commit();
    }

    public static long getBookmarksLastEdited(Context context) {
        if (context == null) {
            return -1;
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getLong(KEY_BOOKMARKS_DB_LAST_EDITED, -1);
    }

    public static void saveBookmarksLastEdited(Context context) {
        if (context == null) {
            return;
        }
        // current time
        long timestamp = System.currentTimeMillis();
        // Shared Prefs
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Editor
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // Id sichern
        editor.putLong(KEY_BOOKMARKS_DB_LAST_EDITED, timestamp);
        // Aenderungen bestaetigen
        editor.commit();
    }

    /**
     * Helper method to determine if queried bookmarks (and highlighted verses) are still valid,
     * or if the database has been edited meanwhile (indicating that the queried verses need to
     * be reloaded)
     * @param context Context
     * @param dbQueryTime timestamp of the time the database was queried (miliseconds)
     * @return {@code true} if bookmarks have not been edited yet
     * or the last bookmarks edit time is before the last database query time.
     */
    public static boolean areQueriedBookmarksValid(Context context, long dbQueryTime) {
        if (context == null) {
            return false;
        }
        long bookmarksLastEdited = getBookmarksLastEdited(context);
//        BibleLog.w(TAG, "dbQueryTime: " + dbQueryTime);
//        BibleLog.w(TAG, "bookmarksLastEdited: " + bookmarksLastEdited);
        if (dbQueryTime == -1) {
            // items not queried yet, no reason to invalidate
            return true;
        }
        return bookmarksLastEdited == -1 || dbQueryTime - bookmarksLastEdited > 0;
    }

    public static int getLastKnownAppVersion(Context context) {
        if (context == null) {
            return -1;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getInt(KEY_LAST_KNOWN_APP_VERSION, -1);
    }

    public static void saveLastKnownAppVersion(Context context, int lastKnownAppVersion) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(KEY_LAST_KNOWN_APP_VERSION, lastKnownAppVersion);
        editor.commit();
    }

    public static void removeDeprecatedShowCaseSettings(Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.remove(KEY_SHOW_CASE_ACKNOWLEDGED_PREFIX + SHOWCASE_READER_ACTION_ITEM_OPEN);
        editor.remove(KEY_SHOW_CASE_ACKNOWLEDGED_PREFIX + SHOWCASE_READER_PAGER_SWIPE);
        editor.commit();
    }

    public static void removeAppOldShortcutRemoved(Context context) {
        if (context == null) {
            return;
        }
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        // Editor
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // save boolean
        editor.remove(KEY_APP_OLD_SHORTCUT_REMOVED);
        // Aenderungen bestaetigen
        editor.commit();
    }

}