package de.mseebass.bible.utils;

import android.os.StatFs;

import java.io.File;
import java.text.DecimalFormat;

public class FilesUtils {

    private static final String TAG = "FilesUtils";


    public static String getFileSizeString(File file, boolean round) {
        if (file == null) {
            return "null";
        }
        long fileSize = file.length();
        return getFileSizeString(fileSize, round);
    }

    public static String getFileSizeString(long fileSize, boolean round) {
        int unitLevel = 0; // 0 = bytes, 1 = KB, 2 = MB, 3 = GB ..
        float fileSizeRelative = (float) fileSize;
        while (fileSizeRelative > 1024 && unitLevel <= 3) {
            fileSizeRelative = fileSizeRelative / 1024f;
            unitLevel++;
        }
        String unit;
        switch (unitLevel) {
            case 0:
                unit = "bytes";
                break;
            case 1:
                unit = "KB";
                break;
            case 2:
                unit = "MB";
                break;
            case 3:
                unit = "GB";
                break;
            case 4:
                unit = "TB";
                break;
            default:
                unit = "<unknown unit>";
                break;
        }

        DecimalFormat df = new DecimalFormat();
        if (round) {
            df.setMaximumFractionDigits(1);
            df.setMinimumFractionDigits(1);
        } else {
            df.setMaximumFractionDigits(15);
            df.setMinimumFractionDigits(4);
        }
        return df.format(fileSizeRelative) + " " + unit;
    }

    /**
     * @param dir Directory
     * @return available storage in bytes
     */
    public static long getPublicAvailableSpace(File dir) {
//		long startTime = System.nanoTime();

        if (dir == null) { // MSE 18.09.2017: Absturz über Google Play Dev Console gemeldet
            BibleLog.w(TAG, "Verfügbarer Speicherplatz konnte nicht ermittelt werden. dir == null !");
            return -1;
        }
        try { // MSE 18.09.2017: Absturz über Google Play Dev Console gemeldet
            StatFs stat = new StatFs(dir.getPath());
            stat.restat(dir.getPath());

//			long endTime = System.nanoTime();
//			float executionTimeMillis = ((float)(endTime - startTime)) / 1000000f;
//			ALog.debug(TAG, "Scanning for available space took " + executionTimeMillis + " ms");

            return stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        } catch (Exception e) {
            BibleLog.w(TAG, "Verfügbarer Speicherplatz konnte nicht ermittelt werden.", e);
        }
        return -1;
    }

}