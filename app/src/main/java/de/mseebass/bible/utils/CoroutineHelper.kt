package de.mseebass.bible.utils

import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.repository.IHistoryRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CoroutineHelper {

    fun saveHistoryAsync(repo: IHistoryRepository, entry : HistoryEntry) {
        try {
            GlobalScope.launch {
                try {
                    repo.insert(entry)
                } catch (e : Exception) {
                    BibleLog.e("CoroutineHelper", "Failed to create history entry!", e);
                }
            }
        } catch (e : Exception) {
            BibleLog.e("CoroutineHelper", "Failed to create history entry!", e);
        }
    }

}