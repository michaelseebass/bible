package de.mseebass.bible.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

//@Suppress("UNCHECKED_CAST")
internal inline fun <reified T: Fragment> AppCompatActivity.findFragment(tag: String): T? {
    return supportFragmentManager.findFragmentByTag(tag) as T?
}