package de.mseebass.bible.utils;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BibleLog {

    private static final boolean LOGGING_ENABLED = false; //TODO

    public static void e(String tag, String msg) {
        Log.e(tag, msg);
    }

    public static void e(String tag, Throwable thr) {
        Log.e(tag, "Bible encountered an unknown error:", thr);
    }

    public static void e(String tag, String msg, Throwable thr) {
        Log.e(tag, msg, thr);
    }

    public static void i(String tag, String msg) {
        if (LOGGING_ENABLED) Log.i(tag, msg);
    }

    public static void i(String tag, String msg, Throwable thr) {
        if (LOGGING_ENABLED) Log.i(tag, msg, thr);
    }

    public static void d(String tag, String msg) {
        if (LOGGING_ENABLED) Log.d(tag, msg);
    }

    public static void d(String tag, String msg, Throwable thr) {
        if (LOGGING_ENABLED) Log.d(tag, msg, thr);
    }

    public static void v(String tag, String msg) {
        if (LOGGING_ENABLED) Log.v(tag, msg);
    }

    public static void v(String tag, String msg, Throwable thr) {
        if (LOGGING_ENABLED) Log.v(tag, msg, thr);
    }

    public static void w(String tag, String msg) {
        if (LOGGING_ENABLED) Log.w(tag, msg);
    }

    public static void w(String tag, String msg, Throwable thr) {
        if (LOGGING_ENABLED) Log.w(tag, msg, thr);
    }

    public static String getInstanceSpecificLogTag(String tagPrefix) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMAN);
        Date date = new Date(System.currentTimeMillis());
        return tagPrefix + " - " + dateFormat.format(date);
    }

    public static void printSizeOf(String tag, Object object) {
        if (object == null) {
            BibleLog.w(tag, "Error logging object: object is null");
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream;
            try {
                objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

                objectOutputStream.writeObject(object);
                objectOutputStream.flush();
                objectOutputStream.close();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                if (byteArray != null) {
                    BibleLog.w(tag, "Object is " + byteArray.length + " bytes large");
                } else {
                    BibleLog.w(tag, "Error logging object");
                }
            } catch (IOException e) {
                BibleLog.w(tag, "Error logging object");
                e.printStackTrace();
            }
        }
    }
}