package de.mseebass.bible.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import de.mseebass.bible.ui.backup.BackupBaseFragment;

/**
 * @author Michael Seebaß on 28.10.2016.
 */
public class StoragePermissionManager {

    /**
     * Log-TAG
     */
    private static final String TAG = StoragePermissionManager.class.getSimpleName();

    public static final int PERMISSION_REQUEST_CODE = 23;


    private Activity mActivity;

    private static StoragePermissionManager sInstance;


    private StoragePermissionManager(Activity activity) {
        this.mActivity = activity;
    }

    public static StoragePermissionManager get(Activity activity) {
        if (sInstance == null) {
            sInstance = new StoragePermissionManager(activity);
        }
        return sInstance;
    }

    public static void release() {
        sInstance = null;
    }


    public void onStoragePermissionRequired(BackupBaseFragment caller) {
        BibleLog.d(TAG, "onStoragePermissionRequired()");
        if (!checkStoragePermissionGranted()) {
            requestStoragePermission(caller);
        } else {
            onPermissionGranted(caller);
        }
    }

    private void onPermissionGranted(BackupBaseFragment caller) {
        BibleLog.d(TAG, "onPermissionGranted()");
        caller.doAction();
    }

    private void requestStoragePermission(BackupBaseFragment caller) {
        BibleLog.d(TAG, "requestStoragePermission()");
        /* get permission state */
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            caller.displayPermissionExplanation();
            BibleLog.i(TAG, "displaying WRITE_EXTERNAL_STORAGE permission rationale to provide additional context");
        } else {
            /* requesting permission */
//            ActivityCompat.requestPermissions(mActivity, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, PERMISSION_REQUEST_CODE);
            caller.requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, PERMISSION_REQUEST_CODE);
        }
    }

    public boolean checkStoragePermissionGranted() {
        return ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults, BackupBaseFragment caller) {
        BibleLog.d(TAG, "onRequestPermissionsResult() called with " + "requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                boolean granted = false;
                // If request is cancelled, the result arrays are empty --> check length
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    granted = true;
                }
                BibleLog.v(TAG, "onRequestPermissionsResult(): " + granted);
                onStoragePermissionResult(granted, caller);
                break;
            }
            default:
                break;
        }
    }

    private void onStoragePermissionResult(boolean granted, BackupBaseFragment caller) {
        BibleLog.d(TAG, "onStoragePermissionResult() called with " + "granted = [" + granted + "]");
        if (granted) {
            onPermissionGranted(caller);
        } else {
            caller.displayPermissionExplanation();
        }
    }

}