package de.mseebass.bible.utils;

public class DebuggingUtils {

    public static final boolean DEBUGGING_TASKS_ENABLED = false;
    public static final int DEBUGGING_TASKS_DELAY = 5000;


    public static void debugSleepThread() {
        if (DEBUGGING_TASKS_ENABLED) {
            try {
                Thread.sleep(DEBUGGING_TASKS_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
