package de.mseebass.bible.application

import android.app.Application
import android.content.pm.PackageManager
import androidx.core.content.pm.PackageInfoCompat
import de.mseebass.bible.tasks.AppUpdateTask
import de.mseebass.bible.tasks.QueryBooksTask
import de.mseebass.bible.tasks.QueryChapterTask
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.Settings
import de.mseebass.bible.utils.StoragePermissionManager


class BibleApplication : Application() {
    /**
     * TODO
     * suche nach mehreren wörtern
     * Day-Night-Themes
     * focus mode
     * Migrate to Room
     * UpgradeFragment nur bei Upgrades? oder automatisch weiter
     */
    /*
     * (non-Javadoc)
     * called after de.mseebass.bible.providers.BibleProvider#onCreate()
     * @see android.app.Application#onCreate()
     */
    override fun onCreate() {
        BibleLog.v(TAG, "BibleApplication - onCreate()")
        val lastKnownAppVersion = Settings.getLastKnownAppVersion(this)
        try {
            val packageInfo = packageManager.getPackageInfo(packageName, 0)
            val currentAppVersion = PackageInfoCompat.getLongVersionCode(packageInfo).toInt()
            // check if this is an app update
            if (lastKnownAppVersion < currentAppVersion) {
                BibleLog.d(TAG, "upgrading")
                if (!AppUpdateTask.isRunning()) {
                    AppUpdateTask.getInstance(this).apply {
                        setLastKnownAppVersion(lastKnownAppVersion)
                        execute()
                    }
                }
                // save new app version
                Settings.saveLastKnownAppVersion(this, currentAppVersion)
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        super.onCreate()
    }

    override fun onLowMemory() {
        BibleLog.w(TAG, "BibleApplication - onLowMemory()")
        QueryChapterTask.releaseAllInstances()
        QueryBooksTask.releaseAllInstances()
        StoragePermissionManager.release()
        super.onLowMemory()
    }

    companion object {
        const val TAG = "BibleApplication"
    }
}