package de.mseebass.bible.interfaces

interface OnColorSelectedListener {

    fun onColorSelected(solidColor: Int)

}