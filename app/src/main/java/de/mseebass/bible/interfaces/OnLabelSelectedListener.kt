package de.mseebass.bible.interfaces

interface OnLabelSelectedListener {

    fun onLabelSelected(newLabel: String)

}