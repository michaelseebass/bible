package de.mseebass.bible.interfaces

/**
 * Callback interface for the BookmarksFragment to get informed of changes in label list.
 *
 * @author Michael Seebaß B.Eng., 2014
 */
interface OnLabelCountChangedListener {

    /**
     * Called when an item in the label list is deleted or restored
     *
     * @param groupCount the number of remaining groups
     */
    fun onLabelCountChanged(groupCount: Int)

    /**
     * Called when an item is deleted
     */
    fun onItemDeleted()
}