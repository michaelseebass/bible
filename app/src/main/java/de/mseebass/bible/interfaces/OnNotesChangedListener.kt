package de.mseebass.bible.interfaces

interface OnNotesChangedListener {
    fun onNotesChanged()
}