package de.mseebass.bible.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import de.mseebass.bible.utils.BibleLog;
import de.mseebass.bible.utils.EncodingUtils;
import de.mseebass.bible.utils.Settings;

public class BibleProvider extends ContentProvider {
    // Log-TAG
    public static final String TAG = "BibleProvider";
    // Authority
    public static final String AUTHORITY = "de.mseebass.bible";
    public static final String FILE_NAME_BIBLES = "bible_data_base_bibles";
    public static final String FILE_NAME_BOOKMARKS = "bible_data_base_bookmarks";
    public static final String FILE_NAME_VERSES_PREFIX = "bible_data_base_verses_";
    // Types
    private static final int TYPE_BIBLES = 0;
    private static final int TYPE_BIBLE_ID = 1;
    private static final int TYPE_VERSES = 2;
    private static final int TYPE_VERSE_ID = 3;
    private static final int TYPE_VERSE_DISTINCT = 4;
    private static final int TYPE_BOOKMARKS = 5;
    private static final int TYPE_BOOKMARKS_ID = 6;
    @Deprecated
    private static final int TYPE_BOOKMARKS_LEGACY = 7;
    private static final int TYPE_BOOKMARKS_RESTORE = 8;
    private static final int TYPE_BOOKMARKS_EXPORT = 9; // just closes the database
    // URI matcher
    private static final UriMatcher sUriMatcher;
    // initialize the URI matcher
    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, Bibles.TABLE_NAME, TYPE_BIBLES);
        sUriMatcher.addURI(AUTHORITY, Bibles.TABLE_NAME + "/#", TYPE_BIBLE_ID);
        sUriMatcher.addURI(AUTHORITY, Verses.TABLE_NAME + "/" + Verses.BIBLE_ID + "/#", TYPE_VERSES);
        sUriMatcher.addURI(AUTHORITY, Verses.TABLE_NAME + "/" + Verses.BIBLE_ID + "/#/#", TYPE_VERSE_ID);
        sUriMatcher.addURI(AUTHORITY, Verses.TABLE_NAME + "/distinct/" + Verses.BIBLE_ID + "/#", TYPE_VERSE_DISTINCT);
        sUriMatcher.addURI(AUTHORITY, Bookmarks.TABLE_NAME, TYPE_BOOKMARKS);
        sUriMatcher.addURI(AUTHORITY, Bookmarks.TABLE_NAME + "/#", TYPE_BOOKMARKS_ID);
        sUriMatcher.addURI(AUTHORITY, Bookmarks.TABLE_NAME + "/legacy", TYPE_BOOKMARKS_LEGACY);
        sUriMatcher.addURI(AUTHORITY, Bookmarks.TABLE_NAME + "/restore/*", TYPE_BOOKMARKS_RESTORE);
        sUriMatcher.addURI(AUTHORITY, Bookmarks.TABLE_NAME + "/export", TYPE_BOOKMARKS_EXPORT);
    }
    // file name
    private static final String FILE_NAME_LEGACY = "bible_data_base";
    // database version
    private static final int DATABASE_VERSION_BIBLES = 1;
    private static final int DATABASE_VERSION_BOOKMARKS = 2;
    private static final int DATABASE_VERSION_VERSES = 1;
    SQLiteOpenHelper mHelper;


    public static synchronized File getDeprecatedDatabaseFile(Context context) {
        return context.getDatabasePath(FILE_NAME_LEGACY);
    }

//	public synchronized void resetDatabase() {
//		if (mHelper != null) {
//			mHelper.close();
//		}
//	    mHelper = new DbHelper();
//	    mSqLiteDatabase = mHelper.getWritableDatabase();
//	}

    public static synchronized List<String> getAvailableVerseDatabaseFileNames(Context context) throws IOException {
        List<String> availableDbFileNames = new ArrayList<>();
        String[] fileNames = context.getAssets().list("");
        for (String currentFileName : fileNames) {
            if (currentFileName.startsWith(BibleProvider.FILE_NAME_VERSES_PREFIX)) {
                availableDbFileNames.add(currentFileName.replace(".zip", ""));
            }
        }
        return availableDbFileNames;
    }

    public static synchronized boolean dataBaseExists(Context context, String fileName, boolean checkFileSize, long expectedMinFileSize) {
        if (context == null) {
            throw new IllegalArgumentException("The supplied context is null");
        }
        File dbFile = context.getDatabasePath(fileName);
        if (!dbFile.exists()) {
            return false;
        }
        long fileSize = dbFile.length();
        BibleLog.i(TAG, "fileSize: " + fileSize + " expected min size: " + expectedMinFileSize);
        if (checkFileSize && fileSize < expectedMinFileSize) { // TODO
            // try to delete invalid files
            boolean deleted = dbFile.delete();
            if (!deleted) {
                throw new IllegalStateException("Failed to delete database file " + fileName + " - size: " + fileSize);
            }
            return false;
        }
        try {
            SQLiteDatabase checkDB = SQLiteDatabase.openDatabase(dbFile.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY);
            if (checkDB != null) {
                checkDB.close();
                return true;
            }
        } catch (SQLiteException e) {
            BibleLog.v(TAG, "Database doesn't exist - " + e.getMessage());
        }
        return false;
    }

    @Override
    public synchronized String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case TYPE_BIBLES:
                return "vnd.android.cursor.dir/vnd.seebass.bibles";
            case TYPE_BIBLE_ID:
                return "vnd.android.cursor.item/vnd.seebass.bibles";
            case TYPE_VERSES:
                return "vnd.android.cursor.dir/vnd.seebass.verses";
            case TYPE_VERSE_ID:
                return "vnd.android.cursor.item/vnd.seebass.verses";
            case TYPE_VERSE_DISTINCT:
                return "vnd.android.cursor.dir/vnd.seebass.verses";
            case TYPE_BOOKMARKS:
                return "vnd.android.cursor.item/vnd.seebass.bookmarks";
            case TYPE_BOOKMARKS_ID:
                return "vnd.android.cursor.dir/vnd.seebass.bookmarks";
            case TYPE_BOOKMARKS_LEGACY:
                return "vnd.android.cursor.dir/vnd.seebass.bookmarks";
            case TYPE_BOOKMARKS_RESTORE:
            case TYPE_BOOKMARKS_EXPORT:
                return "vnd.android.cursor.dir/vnd.seebass.bookmarks";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
        }
    }

    /*
     * (non-Javadoc)
     * called before de.mseebass.bible.application.BibleApplication#onCreate()
     * @see android.content.ContentProvider#onCreate()
     */
    @Override
    public synchronized boolean onCreate() {
        BibleLog.v(TAG, "BibleProvider - onCreate()");
        return true;
    }

    @Override
    public synchronized int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        int numInserted = 0;
        String tableName;
        String bibleId;
        int type = sUriMatcher.match(uri);
        switch (type) {
            case TYPE_VERSES:
                bibleId = uri.getLastPathSegment();
                tableName = Verses.TABLE_NAME + "_" + bibleId;
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
        }
        instantiateSQLiteOpenHelper(type, bibleId, null);
        SQLiteDatabase db = mHelper.getWritableDatabase();
        // begin the transaction
        db.beginTransaction();
        try {
            // insert all the rows
            for (ContentValues cv : values) {
                long newID = db.insertOrThrow(tableName, null, cv);
                if (newID <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            // mark the transaction as successful
            db.setTransactionSuccessful();
//            if (type == TYPE_BOOKMARKS) { //type == TYPE_BOOKMARKS_ID
//                Settings.saveBookmarksLastEdited(getContext());
//            }
            getContext().getContentResolver().notifyChange(uri, null);
            numInserted = values.length;
        } finally {
            // end the transaction
            db.endTransaction();
        }
        return numInserted;
    }

    @Override
    public synchronized Uri insert(@NonNull Uri uri, ContentValues values) {
        String tableName;
        String bibleId = null;
        int type = sUriMatcher.match(uri);
        switch (type) {
            case TYPE_BIBLES:
                tableName = Bibles.TABLE_NAME;
                break;
            case TYPE_VERSES:
                bibleId = uri.getLastPathSegment();
                tableName = Verses.TABLE_NAME + "_" + bibleId;
                break;
            case TYPE_BOOKMARKS:
                tableName = Bookmarks.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
        }

        instantiateSQLiteOpenHelper(type, bibleId, null);
        SQLiteDatabase db = mHelper.getWritableDatabase();
        long id = db.insert(tableName, null, values);
        // if added successfully
        if (id >= 0) {
            if (type == TYPE_BOOKMARKS) { //type == TYPE_BOOKMARKS_ID
                Settings.saveBookmarksLastEdited(getContext());
            }
//			if (type == TYPE_BIBLES) {
//				// create the verses database file for this new bible
//				SQLiteOpenHelper verseHelper = openDataBase(type, bibleId);
//				verseHelper.getWritableDatabase();
//				verseHelper.close();
//			}
            Uri uriWithAppendedId = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(uriWithAppendedId, null);
            return uriWithAppendedId;
        }
        if (values == null) {
            throw new IllegalArgumentException("The ContentValues object supplied to BibleProvider.insert() is null");
        } else if (values.size() == 0) {
            throw new IllegalArgumentException("The ContentValues object supplied to BibleProvider.insert() is empty");
        } else if (BibleProviderUtils.bookmarkExists(getContext(), id)) {
            throw new IllegalArgumentException("The bookmark " + values.toString() + " was inserted with the id " + id);
        }
        throw new SQLException("Failed to insert row with values " + values.toString()
                + "  into " + uri
                + " - Returned id " + id);
    }

//	private synchronized void createVerseTable(long bibleId) {
//		SQLiteDatabase db = openDataBase(TYPE_VERSES);
//		db.execSQL("CREATE TABLE " + Verses.TABLE_NAME + "_" + bibleId + " (" +
//				Verses._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
//				Verses.BIBLE_ID + " INTEGER ," +
//				Verses.BOOK_NUMBER + " INTEGER ," +
//				Verses.BOOK_NAME + " TEXT ," +
//				Verses.CHAPTER_NUMBER + " INTEGER ," +
//				Verses.NUMBER + " INTEGER ," +
//				Verses.TEXT + " TEXT);");
//	}

    @Override
    public synchronized Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        BibleLog.v(TAG, "query(" + uri.toString() + ")");
        SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
        String bibleId = null;
        String filePath = null;
        int type = sUriMatcher.match(uri);
        switch (type) {
            case TYPE_BIBLES:
                sqlBuilder.setTables(Bibles.TABLE_NAME);
                break;
            case TYPE_BIBLE_ID:
                sqlBuilder.setTables(Bibles.TABLE_NAME);
                sqlBuilder.appendWhere(Bibles._ID + " = " + uri.getLastPathSegment());
                break;
            case TYPE_VERSES: {
                bibleId = uri.getLastPathSegment();
                String tableName = Verses.TABLE_NAME + "_" + bibleId;
                sqlBuilder.setTables(tableName);
                break;
            }
            case TYPE_VERSE_ID: {
                // URI content://de.mseebass.bible/verses/bible_id/#/#
                bibleId = uri.getPathSegments().get(2);
                String tableName = Verses.TABLE_NAME + "_" + bibleId;
                sqlBuilder.setTables(tableName);
                sqlBuilder.appendWhere(Verses._ID + " = " + uri.getPathSegments().get(3));
                break;
            }
            case TYPE_VERSE_DISTINCT: {
                bibleId = uri.getLastPathSegment();
                String tableName = Verses.TABLE_NAME + "_" + bibleId;
                sqlBuilder.setTables(tableName);
                sqlBuilder.setDistinct(true);
                break;
            }
            case TYPE_BOOKMARKS:
                sqlBuilder.setTables(Bookmarks.TABLE_NAME);
                break;
            case TYPE_BOOKMARKS_ID:
                sqlBuilder.setTables(Bookmarks.TABLE_NAME);
                sqlBuilder.appendWhere(Bookmarks._ID + " = " + uri.getLastPathSegment());
                break;
            case TYPE_BOOKMARKS_LEGACY:
                sqlBuilder.setTables(Bookmarks.TABLE_NAME);
                break;
            case TYPE_BOOKMARKS_RESTORE: {
                sqlBuilder.setTables(Bookmarks.TABLE_NAME);
                String encodedPath = uri.getLastPathSegment();

                try {
                    filePath = EncodingUtils.INSTANCE.decodeBase64ToString(encodedPath);
                } catch (UnsupportedEncodingException e) {
                    BibleLog.w(TAG, "Error decoding filePath: " + encodedPath, e);
                    return null;
                }

                try {
                    File file = new File(filePath);
                    if (!file.exists()) {
                        BibleLog.w(TAG, "Tried to restore Bible Bookmarks from an invalid or unreachable filePath (doesn't exist): " + filePath);
                        return null;
                    }
                } catch (Exception e) {
                    BibleLog.w(TAG, "Tried restore Bible Bookmarks from an invalid or unreachable filePath (catched Exception): " + filePath, e);
                    return null;
                }
                break;
            }
            case TYPE_BOOKMARKS_EXPORT:
                if (mHelper != null) mHelper.close();
                return null;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
        }
        instantiateSQLiteOpenHelper(type, bibleId, filePath);
        SQLiteDatabase db = mHelper.getWritableDatabase();
        // query
        Cursor cursor = sqlBuilder.query(
                db,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        // register to watch URI for changes
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public synchronized int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int rowCount;
        String tableName;
        String bibleId = null;
        int type = sUriMatcher.match(uri);
        switch (type) {
            case TYPE_BIBLES: {
                tableName = Bibles.TABLE_NAME;
                break;
            }
            case TYPE_BIBLE_ID: {
                tableName = Bibles.TABLE_NAME;
                selection = Bibles._ID + " = " + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            }
            case TYPE_VERSES: {
                bibleId = uri.getLastPathSegment();
                tableName = Verses.TABLE_NAME + "_" + bibleId;
                break;
            }
            case TYPE_VERSE_ID: {
                // URI content://de.mseebass.bible/verses/bible_id/#/#
                bibleId = uri.getPathSegments().get(2);
                tableName = Verses.TABLE_NAME + "_" + bibleId;
                selection = Verses._ID + " = " + uri.getPathSegments().get(3) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            }
            case TYPE_VERSE_DISTINCT: {
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
            }
            case TYPE_BOOKMARKS: {
                tableName = Bookmarks.TABLE_NAME;
                break;
            }
            case TYPE_BOOKMARKS_ID: {
                tableName = Bookmarks.TABLE_NAME;
                selection = Bookmarks._ID + " = " + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            }
            default: {
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
            }
        }
        instantiateSQLiteOpenHelper(type, bibleId, null);
        SQLiteDatabase db = mHelper.getWritableDatabase();
        // update database
        rowCount = db.update(tableName, values, selection, selectionArgs);
        if (rowCount > 0) {
            if (type == TYPE_BOOKMARKS || type == TYPE_BOOKMARKS_ID) {
                Settings.saveBookmarksLastEdited(getContext());
            }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowCount;
    }

    @Override
    public synchronized int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        String tableName;
        String bibleId = null;
        int type = sUriMatcher.match(uri);
        switch (type) {
            case TYPE_BIBLES: {
                tableName = Bibles.TABLE_NAME;
                break;
            }
            case TYPE_BIBLE_ID: {
                tableName = Bibles.TABLE_NAME;
                selection = Bibles._ID + " = " + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            }
            case TYPE_VERSES: {
                bibleId = uri.getLastPathSegment();
                tableName = Verses.TABLE_NAME + "_" + bibleId;
                break;
            }
            case TYPE_VERSE_ID: {
                // URI content://de.mseebass.bible/verses/bible_id/#/#
                bibleId = uri.getPathSegments().get(2);
                tableName = Verses.TABLE_NAME + "_" + bibleId;
                selection = Verses._ID + " = " + uri.getPathSegments().get(3) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            }
            case TYPE_VERSE_DISTINCT: {
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
            }
            case TYPE_BOOKMARKS: {
                tableName = Bookmarks.TABLE_NAME;
                break;
            }
            case TYPE_BOOKMARKS_ID: {
                tableName = Bookmarks.TABLE_NAME;
                selection = Bookmarks._ID + " = " + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            }
            default: {
                throw new IllegalArgumentException("Unsupported URI: " + uri.toString());
            }
        }
        instantiateSQLiteOpenHelper(type, bibleId, null);
        SQLiteDatabase db = mHelper.getWritableDatabase();
        // delete
        int deletedrows = db.delete(tableName, selection, selectionArgs);
        if (deletedrows > 0) {
            if (type == TYPE_BOOKMARKS || type == TYPE_BOOKMARKS_ID) {
                Settings.saveBookmarksLastEdited(getContext());
            }
        }
        return deletedrows;
    }

    /**
     * Makes sure the SQLiteOpenHelper is instantiated correctly
     *
     * @param type    the request type
     * @param bibleId the bible ID
     */
    private void instantiateSQLiteOpenHelper(int type, String bibleId, String filePath) {
//        BibleLog.i(TAG, "instantiateSQLiteOpenHelper(" + getTypeString(type) + ", " + bibleId + ", " + filePath + ")");
        switch (type) {
            case TYPE_BIBLES:
            case TYPE_BIBLE_ID:
                if (mHelper != null) {
                    // re-use this helper if possible
                    if (mHelper instanceof DbBiblesHelper) {
                        BibleLog.v(TAG, "DbBiblesHelper allready exists");
                        break;
                    } else {
                        // close the database connection
                        mHelper.close();
                    }
                }
                // new helper
                mHelper = new DbBiblesHelper();
                break;
            case TYPE_VERSES:
            case TYPE_VERSE_ID:
            case TYPE_VERSE_DISTINCT:
                if (bibleId == null) {
                    throw new IllegalArgumentException("The supplied bibleId String is null");
                }
                if (mHelper != null) {
                    // re-use this helper if possible
                    if (mHelper instanceof DbVersesHelper && mHelper.getDatabaseName().equals(FILE_NAME_VERSES_PREFIX + bibleId)) {
                        BibleLog.v(TAG, "DbVersesHelper allready exists");
                        break;
                    } else {
                        // close the database connection
                        mHelper.close();
                    }
                }
                // new helper
                mHelper = new DbVersesHelper(bibleId);
                break;
            case TYPE_BOOKMARKS:
            case TYPE_BOOKMARKS_ID:
                if (mHelper != null) {
                    // re-use this helper if possible
                    if (mHelper instanceof DbBookmarksHelper) {
                        BibleLog.v(TAG, "DbBookmarksHelper allready exists");
                        break;
                    } else {
                        // close the database connection
                        mHelper.close();
                    }
                }
                // new helper
                mHelper = new DbBookmarksHelper();
                break;
            case TYPE_BOOKMARKS_LEGACY:
                if (mHelper != null) {
                    mHelper.close();
                }
                mHelper = new DbBookmarksLegacyHelper();
                break;
            case TYPE_BOOKMARKS_RESTORE:
                if (mHelper != null) {
                    mHelper.close();
                }
                mHelper = new DbBookmarksRestoreHelper(filePath);
                break;
            default:
                throw new IllegalArgumentException("Unsupported type: " + String.valueOf(type));
        }
    }

    /** Uncomemented for logging */
//    private String getTypeString(int type) {
//        switch (type) {
//            case TYPE_BIBLES: return "TYPE_BIBLES";
//            case TYPE_BIBLE_ID : return "TYPE_BIBLE_ID";
//            case TYPE_VERSES: return "TYPE_VERSES";
//            case TYPE_VERSE_DISTINCT: return "TYPE_VERSE_DISTINCT";
//            case TYPE_BOOKMARKS: return "TYPE_BOOKMARKS";
//            case TYPE_BOOKMARKS_ID: return "TYPE_BOOKMARKS_ID";
//            default: return "other"; // TODO
//        }
//    }

    public static final class Bibles {
        public static final Uri CONTENT_URI = Uri.parse("content://de.mseebass.bible/bibles");
        public static final String _ID = "_id";
        public static final String NAME = "name";
        public static final String SHORT_NAME = "short_name";
        public static final String LANGUAGE = "language";
        private static final String TABLE_NAME = "bibles";
    }

    public static final class Verses {
        public static final Uri CONTENT_URI = Uri.parse("content://de.mseebass.bible/verses");
        public static final Uri CONTENT_URI_DISTINCT = Uri.parse("content://de.mseebass.bible/verses/distinct");
        public static final String _ID = "_id";
        public static final String BIBLE_ID = "bible_id";
        public static final String BOOK_NUMBER = "book_number";
        public static final String BOOK_NAME = "book_name";
        public static final String CHAPTER_NUMBER = "chapter_number";
        public static final String NUMBER = "number";
        public static final String TEXT = "text";
        private static final String TABLE_NAME = "verses";
    }

    public static final class Bookmarks {
        public static final Uri CONTENT_URI = Uri.parse("content://de.mseebass.bible/bookmarks");
        @Deprecated
        public static final Uri CONTENT_URI_LEGACY = Uri.parse("content://de.mseebass.bible/bookmarks/legacy");
        public static final Uri CONTENT_URI_RESTORE = Uri.parse("content://de.mseebass.bible/bookmarks/restore/");
        public static final Uri CONTENT_URI_EXPORT = Uri.parse("content://de.mseebass.bible/bookmarks/export"); // close Db object
        public static final int COLOR_NONE = 0x00000000;
        public static final String LABEL_GENERAL = "_label_general";
        public static final String _ID = "_id";
        public static final String BIBLE_ID = "bible_id";
        public static final String BIBLE_SHORT_NAME = "bible_short_name";
        public static final String BOOK_NUMBER = "book_number";
        public static final String BOOK_NAME = "book_name";
        public static final String CHAPTER_NUMBER = "chapter_number";
        public static final String VERSE_ID = "verse_id";
        public static final String VERSE_NUMBER = "number";
        public static final String VERSE_TEXT = "text";
        public static final String LABELS = "labels";
        public static final String HIGHLIGHT_COLOR = "highlight_color";
        public static final String NOTES = "notes";
        private static final String TABLE_NAME = "bookmarks";
    }

    private class DbBiblesHelper extends SQLiteOpenHelper {

        private DbBiblesHelper() {
            super(getContext(), FILE_NAME_BIBLES, null, DATABASE_VERSION_BIBLES);
            BibleLog.v(TAG, "new DbBiblesHelper()");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Bibles.TABLE_NAME + " (" +
                            Bibles._ID + " INTEGER PRIMARY KEY, " +
                            Bibles.NAME + " TEXT ," +
                            Bibles.SHORT_NAME + " TEXT ," +
                            Bibles.LANGUAGE + " TEXT);"
            );

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        @Override
        public synchronized void close() {
            BibleLog.v(TAG, "DbBiblesHelper.close()");
            super.close();
        }

    }

    private class DbVersesHelper extends SQLiteOpenHelper {

        private String mBibleId;

        private DbVersesHelper(String bibleId) {
            super(getContext(), FILE_NAME_VERSES_PREFIX + bibleId, null, DATABASE_VERSION_VERSES);
            mBibleId = bibleId;
            BibleLog.v(TAG, "new DbVersesHelper()");
        }

        @Override
        public synchronized void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Verses.TABLE_NAME + "_" + mBibleId + " (" +
                    Verses._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Verses.BIBLE_ID + " INTEGER ," +
                    Verses.BOOK_NUMBER + " INTEGER ," +
                    Verses.BOOK_NAME + " TEXT ," +
                    Verses.CHAPTER_NUMBER + " INTEGER ," +
                    Verses.NUMBER + " INTEGER ," +
                    Verses.TEXT + " TEXT);");
        }

        @Override
        public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        @Override
        public synchronized void close() {
            BibleLog.v(TAG, "DbVersesHelper.close()");
            super.close();
        }
    }

    private class DbBookmarksHelper extends SQLiteOpenHelper {

        private DbBookmarksHelper() {
            super(getContext(), FILE_NAME_BOOKMARKS, null, DATABASE_VERSION_BOOKMARKS);
            BibleLog.v(TAG, "new DbBookmarksHelper()");
        }

        @Override
        public synchronized void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Bookmarks.TABLE_NAME + " (" +
                            Bookmarks._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            Bookmarks.BIBLE_ID + " INTEGER ," +
                            Bookmarks.BIBLE_SHORT_NAME + " TEXT ," +
                            Bookmarks.BOOK_NUMBER + " INTEGER ," +
                            Bookmarks.BOOK_NAME + " TEXT ," +
                            Bookmarks.CHAPTER_NUMBER + " INTEGER ," +
                            Bookmarks.VERSE_ID + " INTEGER ," +
                            Bookmarks.VERSE_NUMBER + " INTEGER ," +
                            Bookmarks.VERSE_TEXT + " TEXT ," +
                            Bookmarks.LABELS + " TEXT ," +
                            Bookmarks.HIGHLIGHT_COLOR + " INTEGER ," +
                            Bookmarks.NOTES + " TEXT);"
            );
        }

        @Override
        public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // add column
            if (oldVersion == 1 && newVersion == 2) {
                db.execSQL("ALTER TABLE " + Bookmarks.TABLE_NAME + " ADD COLUMN " + Bookmarks.NOTES + " TEXT");
            }
        }

        @Override
        public synchronized void close() {
            BibleLog.v(TAG, "DbBookmarksHelper.close()");
            super.close();
        }
    }

    private class DbBookmarksLegacyHelper extends SQLiteOpenHelper {

        private DbBookmarksLegacyHelper() {
            super(getContext(), FILE_NAME_LEGACY, null, DATABASE_VERSION_BOOKMARKS);
            BibleLog.v(TAG, "new DbBookmarksLegacyHelper()");
        }

        @Override
        public synchronized void onCreate(SQLiteDatabase db) {
        }

        @Override
        public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        @Override
        public synchronized void close() {
            BibleLog.v(TAG, "DbBookmarksLegacyHelper.close()");
            super.close();
        }
    }

    private class DbBookmarksRestoreHelper extends SQLiteOpenHelper {

        private DbBookmarksRestoreHelper(String fileName) {
            super(getContext(), fileName, null, DATABASE_VERSION_BOOKMARKS);
            BibleLog.v(TAG, "new DbBookmarksLegacyHelper()");
        }

        @Override
        public synchronized void onCreate(SQLiteDatabase db) {
        }

        @Override
        public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        @Override
        public synchronized void close() {
            BibleLog.v(TAG, "DbBookmarksRestoreHelper.close()");
            super.close();
        }
    }
}