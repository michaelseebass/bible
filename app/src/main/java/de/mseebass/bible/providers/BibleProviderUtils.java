package de.mseebass.bible.providers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.LongSparseArray;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import de.mseebass.bible.models.Bible;
import de.mseebass.bible.models.Book;
import de.mseebass.bible.models.Chapter;
import de.mseebass.bible.models.HistoryEntry;
import de.mseebass.bible.models.Label;
import de.mseebass.bible.models.Verse;
import de.mseebass.bible.providers.BibleProvider.Bibles;
import de.mseebass.bible.providers.BibleProvider.Bookmarks;
import de.mseebass.bible.providers.BibleProvider.Verses;
import de.mseebass.bible.utils.BibleLog;


public class BibleProviderUtils {

    public static final String TAG = "BibleProviderUtils";
    public static final int ID_NONE = -1;

    private static final Object lock = false;


    // CREATE

    /*
     * These Methods are commented out as they are only needed to add more bible translations
     */

//	public static synchronized long insertBible(Bible bible, Context context) {
//		ContentValues cv = new ContentValues();
//		cv.put(Bibles._ID, bible.getId());
//		cv.put(Bibles.NAME, bible.getName());
//		cv.put(Bibles.SHORT_NAME, bible.getShortName());
//		cv.put(Bibles.LANGUAGE, bible.getLanguage());
//		ContentResolver cr = context.getContentResolver();
//		Uri insertUri = cr.insert(Bibles.CONTENT_URI, cv);
//		return ContentUris.parseId(insertUri);
//	}

//	public static synchronized long insertVerse(Verse verse, Context context) {
//		ContentValues cv = new ContentValues();
//		cv.put(Verses.BIBLE_ID, verse.getBibleId());
//		cv.put(Verses.BOOK_NAME, verse.getBookName());
//		cv.put(Verses.BOOK_NUMBER, verse.getBookNumber());
//		cv.put(Verses.CHAPTER_NUMBER, verse.getChapterNumber());
//		cv.put(Verses.NUMBER, verse.getNumber());
//		cv.put(Verses.TEXT, verse.getText());
//		ContentResolver cr = context.getContentResolver();
//		// URI zusammensetzen
//		Uri uri = getVersesUri(verse.getBibleId(), false);
//		Uri rowUri = cr.insert(uri, cv);
//		return ContentUris.parseId(rowUri);
//	}

//	public static synchronized int bulkInsertVerses(Vector<Verse> verses, long bibleId, Context context) {
//		ContentValues[] cvArray = new ContentValues[verses.size()];
//		for (int i = 0; i < verses.size(); i++) {
//			Verse currentVerse = verses.get(i);
//			ContentValues cv = new ContentValues();
//			cv.put(Verses.BIBLE_ID, currentVerse.getBibleId());
//			cv.put(Verses.BOOK_NAME, currentVerse.getBookName());
//			cv.put(Verses.BOOK_NUMBER, currentVerse.getBookNumber());
//			cv.put(Verses.CHAPTER_NUMBER, currentVerse.getChapterNumber());
//			cv.put(Verses.NUMBER, currentVerse.getNumber());
//			cv.put(Verses.TEXT, currentVerse.getText());
//			cvArray[i] = cv;
//		}
//		ContentResolver cr = context.getContentResolver();
//		// URI zusammensetzen
//		Uri uri = getVersesUri(bibleId, false);
//		int createdRows = cr.bulkInsert(uri, cvArray);
//		return createdRows;
//	}

//	public static synchronized int bulkInsertVerses(ContentValues[] cvArray, long bibleId, Context context) {
//		ContentResolver cr = context.getContentResolver();
//		// URI zusammensetzen
//		Uri uri = getVersesUri(bibleId, false);
//		int createdRows = cr.bulkInsert(uri, cvArray);
//		return createdRows;
//	}

    /**
     * Inserts a new bookmark entry into the provider.
     * Sets the id of the newly created entry as the verse objects bookmark id
     *
     * @param verse   the verse object containing the labels and/or highlight color
     * @param context a Context
     * @return long - the id of the newly created entry
     */
    public static synchronized long insertBookmark(Verse verse, Context context) {
        ContentValues cv = new ContentValues();
        cv.put(Bookmarks.BIBLE_ID, verse.getBibleId());
        cv.put(Bookmarks.BIBLE_SHORT_NAME, verse.getBibleShortName());
        cv.put(Bookmarks.BOOK_NAME, verse.getBookName());
        cv.put(Bookmarks.BOOK_NUMBER, verse.getBookNumber());
        cv.put(Bookmarks.CHAPTER_NUMBER, verse.getChapterNumber());
        cv.put(Bookmarks.VERSE_ID, verse.getId());
        cv.put(Bookmarks.VERSE_NUMBER, verse.getNumber());
        cv.put(Bookmarks.VERSE_TEXT, verse.getText());
        cv.put(Bookmarks.LABELS, verse.getCommaSeparatedLabels());
        cv.put(Bookmarks.HIGHLIGHT_COLOR, verse.getHighLightColor());
        cv.put(Bookmarks.NOTES, verse.getNotes());
        // insert
        Uri insertUri;
        synchronized (lock) {
            ContentResolver cr = context.getContentResolver();
            insertUri = cr.insert(Bookmarks.CONTENT_URI, cv);
        }
        long id = ContentUris.parseId(insertUri);
        // set resulting bookmark ID
        verse.setBookMarkId(id);
        return id;
    }

    // READ

    public static synchronized List<Bible> queryBibles(Context context, boolean closeCursor, String caller) {
//        BibleLog.d(TAG, "queryBibles() called by " + caller);
        String[] projection = new String[]{Bibles._ID, Bibles.NAME, Bibles.SHORT_NAME, Bibles.LANGUAGE};
        String sortOrder = Bibles._ID;
        Cursor cursor;
        synchronized (lock) {
            ContentResolver cr = context.getContentResolver();
            cursor = cr.query(Bibles.CONTENT_URI, projection, null, null, sortOrder);
        }
        // read data from cursor
        return getBibles(cursor, closeCursor);
    }

    private static List<Bible> getBibles(Cursor cursor, boolean closeCursor) {
        // column index
        int idColumnIndex = cursor.getColumnIndex(Bibles._ID);
        int nameColumnIndex = cursor.getColumnIndex(Bibles.NAME);
        int shortNameColumnIndex = cursor.getColumnIndex(Bibles.SHORT_NAME);
        int langColumnIndex = cursor.getColumnIndex(Bibles.LANGUAGE);
        // initialize vector
        List<Bible> bibles = new ArrayList<>();
        // read data from cursor
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            long id = cursor.getLong(idColumnIndex);
            String name = cursor.getString(nameColumnIndex);
            String shortName = cursor.getString(shortNameColumnIndex);
            String language = cursor.getString(langColumnIndex);
            // create bible object and add it to the vector
            Bible bible = new Bible(id, name, shortName, language);
            bibles.add(bible);
        }
        if (closeCursor) {
            cursor.close();
        }
        return bibles;
    }

    public static synchronized Bible getBible(long bibleId, Context context, boolean closeCursor) {
        String[] projection = new String[]{Bibles._ID, Bibles.NAME, Bibles.SHORT_NAME, Bibles.LANGUAGE};
        String sortOrder = Bibles._ID;
        ContentResolver cr = context.getContentResolver();
        Uri uri = ContentUris.withAppendedId(Bibles.CONTENT_URI, bibleId);
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(uri, projection, null, null, sortOrder);
        }
        // read data from cursor
        List<Bible> bibles = getBibles(cursor, closeCursor);
        if (bibles.size() == 1) {
            return bibles.get(0);
        }
        return null;
    }

    public static synchronized Vector<Book> queryBooks(long bibleId, Context context) {
        if (bibleId == ID_NONE) {
            return null;
        }
        String[] projection = new String[]{
                Verses.BOOK_NUMBER,
                Verses.BOOK_NAME,
                Verses.CHAPTER_NUMBER};
        String sortOrder = Verses._ID;
        ContentResolver cr = context.getContentResolver();
        Uri uri = getVersesUri(bibleId, true);
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(uri, projection, null, null, sortOrder);
        }
        // column index
        int bookNrColumnIndex = cursor.getColumnIndex(Verses.BOOK_NUMBER);
        int bookNameColumnIndex = cursor.getColumnIndex(Verses.BOOK_NAME);
        // init vector
        Vector<Book> books = new Vector<Book>();
        // read data from cursor
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            int bookNr = cursor.getInt(bookNrColumnIndex);
            String bookName = cursor.getString(bookNameColumnIndex);
            // nach Buch in Vector suchen
            boolean bookFound = false;
            for (int i = 0; i < books.size(); i++) {
                Book currentBook = books.get(i);
                // bei �bereinstimmung
                if (currentBook.getName().equals(bookName)) {
                    // Kapitelanzahl aktualisieren
                    currentBook.setSize(currentBook.getSize() + 1);
                    bookFound = true;
                }
            }
            if (!bookFound) {
                // neues Buch hinzuf�gen
                Book newBook = new Book(bibleId, bookNr, bookName, 1);
                books.add(newBook);
            }
        }
        cursor.close();
        // Rückgabewert
        return books;
    }

    public static synchronized Chapter queryChapter(long bibleId, int bookNumber, int chapterNumber, Context context, boolean closeCursor) {
        if (bibleId == ID_NONE) {
            return null;
        }
        String[] projection = new String[]{
                Verses._ID,
                Verses.BOOK_NAME,
                Verses.NUMBER,
                Verses.TEXT};
        String selection = Verses.BOOK_NUMBER + " = ? AND " + Verses.CHAPTER_NUMBER + " = ?";
        String[] selArgs = new String[]{String.valueOf(bookNumber), String.valueOf(chapterNumber)};
        String sortOrder = Verses._ID;
        Uri uri = getVersesUri(bibleId, false);
        ContentResolver cr = context.getContentResolver();
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(uri, projection, selection, selArgs, sortOrder);
        }
        return getChapter(cursor, bibleId, bookNumber, chapterNumber, closeCursor);
    }

    @Deprecated
    private static synchronized Chapter getChapter(Cursor cursor, long bibleId, int bookNumber, int chapterNumber, boolean closeCursor) {
        // column index
        int idColumnIndex = cursor.getColumnIndex(Verses._ID);
        int bookNameColumnIndex = cursor.getColumnIndex(Verses.BOOK_NAME);
        int numberColumnIndex = cursor.getColumnIndex(Verses.NUMBER);
        int textColumnIndex = cursor.getColumnIndex(Verses.TEXT);
        // init vector
        Vector<Verse> verses = new Vector<Verse>();
        // read data from cursor
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

            try {
                long id = cursor.getLong(idColumnIndex);
                String bookName = cursor.getString(bookNameColumnIndex);
                int number = cursor.getInt(numberColumnIndex);
                String text = new String(cursor.getString(textColumnIndex).getBytes(), "UTF-8");
//				String text = cursor.getString(textColumnIndex);
                Verse verse = new Verse(id, bibleId, bookNumber, bookName, chapterNumber, number, text);
                // zum Vector hinzuf�gen
                verses.add(verse);
            } catch (UnsupportedEncodingException e) {
                BibleLog.e(TAG, "Fehler beim Abfragen des Kapitels aus der DB: " +
                        "bibleId = " + bibleId + " bookNr = " + bookNumber + " chapterNr = " + chapterNumber + " closeCursor = " + closeCursor, e);
            }
//			
        }
        if (closeCursor) {
            cursor.close();
        }
        // R�ckgabewert
        return new Chapter(chapterNumber, bookNumber, verses);
    }

    public static synchronized Vector<Verse> search(long bibleId, List<Integer> selectedBookNumbers, List<String> queries, Context context, boolean closeCursor) {
        if (bibleId == ID_NONE) {
            return null;
        }
        String[] projection = new String[]{
                Verses._ID,
                // Bible-ID als Argument mitgeliefert --> nicht ben�tigt
                Verses.BOOK_NAME,
                Verses.BOOK_NUMBER,
                Verses.CHAPTER_NUMBER,
                Verses.NUMBER,
                Verses.TEXT};
//		String selection = Verses.TEXT + " LIKE '%" + query + "%'";
//		int[] arrayOfBookNumbers = new int [] { 3 , 9 };
        String commaSeperatedBookNumbers = null;
        StringBuilder builder = new StringBuilder();
        for (int bookNumber : selectedBookNumbers) {
            builder.append(String.valueOf(bookNumber));
            builder.append(",");
        }
        if (builder.length() > 0) {
            commaSeperatedBookNumbers = builder.substring(0, builder.length() - 1);
        }
        String selection = "";
        for (String query : queries) {
            if (!selection.isEmpty()) {
                selection += " AND ";
            }
            selection += Verses.TEXT + " LIKE '%" + query + "%'";
        }
        selection += " AND " + Verses.BOOK_NUMBER + " IN (" + commaSeperatedBookNumbers + ")";
        String sortOrder = Verses._ID;
        ContentResolver cr = context.getContentResolver();
        Uri uri = getVersesUri(bibleId, false);
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(uri, projection, selection, null, sortOrder);
        }
        // column index
        int idColumnIndex = cursor.getColumnIndex(Verses._ID);
        int bookNameColumnIndex = cursor.getColumnIndex(Verses.BOOK_NAME);
        int bookNumberColumnIndex = cursor.getColumnIndex(Verses.BOOK_NUMBER);
        int chapterNumberColumnIndex = cursor.getColumnIndex(Verses.CHAPTER_NUMBER);
        int numberColumnIndex = cursor.getColumnIndex(Verses.NUMBER);
        int textColumnIndex = cursor.getColumnIndex(Verses.TEXT);
        // init vector
        Vector<Verse> verses = new Vector<Verse>();
        // read data from cursor
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            long id = cursor.getLong(idColumnIndex);
            String bookName = cursor.getString(bookNameColumnIndex);
            int bookNumber = cursor.getInt(bookNumberColumnIndex);
            int chapterNumber = cursor.getInt(chapterNumberColumnIndex);
            int number = cursor.getInt(numberColumnIndex);
            String text = cursor.getString(textColumnIndex);
            verses.add(new Verse(id, bibleId, bookNumber, bookName, chapterNumber, number, text));
        }
        if (closeCursor) {
            cursor.close();
        }
        return verses;
    }

    public static List<HistoryEntry> getVersesForHistory(Context context, List<HistoryEntry> historyEntries) {
        List<HistoryEntry> results = new ArrayList<>();

        Map<Long, List<HistoryEntry>> historyEntriesPerBible = new HashMap<>();
        for (HistoryEntry historyEntry : historyEntries) {
            Long bibleId = historyEntry.getBibleId();
            List<HistoryEntry> list;
            if (!historyEntriesPerBible.containsKey(bibleId)) {
                list = new ArrayList<>();
                historyEntriesPerBible.put(bibleId, list);
            }
            else {
                list = historyEntriesPerBible.get(bibleId);
            }
            list.add(historyEntry);
        }

        // querries all bibles! Todo only load needed bibles?
        LongSparseArray<Bible> bibleMap = new LongSparseArray<>();
        try {
            List<Bible> bibles = BibleProviderUtils.queryBibles(context, true, "getVersesForHistory");

            for (Bible bible : bibles) {
                bibleMap.put(bible.getId(), bible);
            }
        } catch (Exception e) {
            BibleLog.w(TAG, "Failed to fetch bibles in getVersesForHistory()", e);
            // if this fails, we continue anyways. This just means we wont have bible short names in the history screen
        }
        // verse nach bibeln sortieren
        // dann für jede Bibel Verses abrufen


        for (Long bibleId : historyEntriesPerBible.keySet()) {
            List<HistoryEntry> entriesForBible = historyEntriesPerBible.get(bibleId);
            Bible bible = bibleMap.get(bibleId);
            // get a comma-separeted list of ids for verses for this bible
            Set<Long> verseIds = new HashSet<>();
            for (HistoryEntry entry : entriesForBible) {
                verseIds.add(entry.getVerseId());
            }
            ContentResolver cr = context.getContentResolver();
            Uri uri = getVersesUri(bibleId, false);

            String commaSeparatedVerseIds = null;
            StringBuilder builder = new StringBuilder();
            for (Long verseId : verseIds) {
                builder.append(verseId);
                builder.append(",");
            }
            if (builder.length() > 0) {
                commaSeparatedVerseIds = builder.substring(0, builder.length() - 1);
            }
            String sortOrder = Verses._ID;
            String where = Verses._ID + " IN (" + commaSeparatedVerseIds + ")";

            Cursor cursor;
            synchronized (lock) {
                cursor = cr.query(uri, null, where, null, sortOrder);
            }
            // column index
            int idColumnIndex = cursor.getColumnIndex(Verses._ID);
            int bookNameColumnIndex = cursor.getColumnIndex(Verses.BOOK_NAME);
            int bookNumberColumnIndex = cursor.getColumnIndex(Verses.BOOK_NUMBER);
            int chapterNumberColumnIndex = cursor.getColumnIndex(Verses.CHAPTER_NUMBER);
            int numberColumnIndex = cursor.getColumnIndex(Verses.NUMBER);
            int textColumnIndex = cursor.getColumnIndex(Verses.TEXT);
            // init vector
            Map<Long, Verse> verses = new HashMap<>();
            // read data from cursor
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                long id = cursor.getLong(idColumnIndex);
                String bookName = cursor.getString(bookNameColumnIndex);
                int bookNumber = cursor.getInt(bookNumberColumnIndex);
                int chapterNumber = cursor.getInt(chapterNumberColumnIndex);
                int number = cursor.getInt(numberColumnIndex);
                String text = cursor.getString(textColumnIndex);

                Verse v = new Verse(id, bibleId, bookNumber, bookName, chapterNumber, number, text);
                v.setBibleShortName(bible.getShortName());
                verses.put(id, v);
            }
            cursor.close();

            for (HistoryEntry entry : entriesForBible) {
                long verseId = entry.getVerseId();
                Verse v = verses.get(verseId);
                entry.verse = v;

                results.add(entry);
            }
        }

        return results;
    }

    private static Vector<Verse> getBookmarkedVerses(Cursor cursor, boolean closeCursor) {
        if (cursor == null) return null;
        // column index
        int idColumnIndex = cursor.getColumnIndex(Bookmarks._ID);
        int bibleIdColumnIndex = cursor.getColumnIndex(Bookmarks.BIBLE_ID);
        int bibleShortNameColumnIndex = cursor.getColumnIndex(Bookmarks.BIBLE_SHORT_NAME);
        int bookNumberColumnIndex = cursor.getColumnIndex(Bookmarks.BOOK_NUMBER);
        int bookNameColumnIndex = cursor.getColumnIndex(Bookmarks.BOOK_NAME);
        int chapterNumberColumnIndex = cursor.getColumnIndex(Bookmarks.CHAPTER_NUMBER);
        int verseIdColumnIndex = cursor.getColumnIndex(Bookmarks.VERSE_ID);
        int verseNumberColumnIndex = cursor.getColumnIndex(Bookmarks.VERSE_NUMBER);
        int verseTextColumnIndex = cursor.getColumnIndex(Bookmarks.VERSE_TEXT);
        int labelColumnIndex = cursor.getColumnIndex(Bookmarks.LABELS);
        int colorColumnIndex = cursor.getColumnIndex(Bookmarks.HIGHLIGHT_COLOR);
        int notesColumnIndex = cursor.getColumnIndex(Bookmarks.NOTES);
        // init vector
        Vector<Verse> verses = new Vector<Verse>();
        // read data from cursor
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            long id = cursor.getLong(idColumnIndex);
            long verseId = cursor.getLong(verseIdColumnIndex);
            long bibleId = cursor.getLong(bibleIdColumnIndex);
            String bookName = cursor.getString(bookNameColumnIndex);
            int bookNumber = cursor.getInt(bookNumberColumnIndex);
            int chapterNumber = cursor.getInt(chapterNumberColumnIndex);
            int number = cursor.getInt(verseNumberColumnIndex);
            String text = cursor.getString(verseTextColumnIndex);
            String verseLabels = cursor.getString(labelColumnIndex);
            String bibleShortName = cursor.getString(bibleShortNameColumnIndex);
            int color = cursor.getInt(colorColumnIndex);
            String notes = cursor.getString(notesColumnIndex);
            Verse verse = new Verse(verseId, bibleId, bookNumber, bookName, chapterNumber, number, text);
            verse.setBookMarkId(id);
            verse.setBibleShortName(bibleShortName);
            verse.setLabels(verseLabels);
            verse.setHighLightColor(color);
            verse.setNotes(notes);
            // hinzuf�gen
            verses.add(verse);
        }
        if (closeCursor) {
            cursor.close();
        }
        return verses;
    }

    public enum QueryBookmarksPathType {
        DEFAULT,
        LEGACY,
        RESTORE;
    }

    public enum QueryBookmarksFilterType {
        ALL,
        ONLY_HIGHLIGHTED,
        ONLY_NOTES
//        ONLY_BOOKMARKS // TODO??
    }

    public static Vector<Verse> queryBookmarkedAndHighlightedVerses(
            Context context,
            QueryBookmarksFilterType filterType,
            QueryBookmarksPathType pathType,
            String encodedPath) {
        String[] projection = new String[]{
                Bookmarks._ID,
                Bookmarks.BIBLE_ID,
                Bookmarks.BIBLE_SHORT_NAME,
                Bookmarks.BOOK_NUMBER,
                Bookmarks.BOOK_NAME,
                Bookmarks.CHAPTER_NUMBER,
                Bookmarks.VERSE_ID,
                Bookmarks.VERSE_NUMBER,
                Bookmarks.VERSE_TEXT,
                Bookmarks.LABELS,
                Bookmarks.HIGHLIGHT_COLOR,
                Bookmarks.NOTES
        };
        String selection = null;
        String[] selArgs = null;
        switch (filterType) {
            case ONLY_HIGHLIGHTED:
                selection = Bookmarks.HIGHLIGHT_COLOR + " != ?"; //TODO
                selArgs = new String[]{String.valueOf(Bookmarks.COLOR_NONE)};
                break;
            case ONLY_NOTES:
                selection = Bookmarks.NOTES + " IS NOT NULL AND " + Bookmarks.NOTES +" != \"\"";
                break;
            case ALL:
            default:
                break;
        }
        String sortOrder = Bookmarks.VERSE_ID;
        ContentResolver cr = context.getContentResolver();
        Uri contentUri = null;
        switch (pathType) {
            case LEGACY:
                contentUri = Bookmarks.CONTENT_URI_LEGACY;
                break;
            case RESTORE:
                contentUri = Bookmarks.CONTENT_URI_RESTORE;
                contentUri = contentUri.buildUpon().appendPath(encodedPath).build();
                BibleLog.i(TAG, "Query mit URI: " + contentUri);
                break;
            case DEFAULT:
            default:
                contentUri = Bookmarks.CONTENT_URI;
                break;
        }
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(contentUri, projection, selection, selArgs, sortOrder);
        }
        if (cursor == null) return null;
        return getBookmarkedVerses(cursor, true);
    }

    public static Vector<Label> getLabelGroups(Context context) {
        Vector<Verse> verses = queryBookmarkedAndHighlightedVerses(context, QueryBookmarksFilterType.ALL, QueryBookmarksPathType.DEFAULT, null); // TODO not all!
        // Verse in Labels sortieren
        Vector<Label> labels = new Vector<Label>();
        // Verse durchlaufen
        for (int i = 0; i < verses.size(); i++) {
            Verse verse = verses.get(i);
            // f�r aktuellen Verse hinterlegte Label durchlaufen
            for (int j = 0; j < verse.getLabels().size(); j++) {
                String currentVerseLabelName = verse.getLabels().get(j);
                // nach passenden Labeln suchen
                boolean matchingLabelFound = false;
                for (int k = 0; k < labels.size(); k++) {
                    Label currentLabel = labels.get(k);
                    // vergleichen
                    if (currentLabel.getName().equals(currentVerseLabelName)) {
                        matchingLabelFound = true;
                        // zum Label hinzuf�gen
                        labels.get(k).addBookMark(verse);
                    }
                }
                // neues Label
                if (!matchingLabelFound) {
                    Label newLabel = new Label(currentVerseLabelName);
                    newLabel.addBookMark(verse);
                    labels.add(newLabel);
                }
            }
        }
        Collections.sort(labels, new Comparator<Label>() {
            public int compare(Label label1, Label label2) {
                return label1.getName().compareToIgnoreCase(label2.getName());
            }
        });
        // R�ckgabewert
        return labels;
    }

    public static Verse getVerse(Context context, long bibleId, int bookNr, int chapterNr, int verseNr) {
        String[] projection = new String[]{
                Verses._ID,
                Verses.BOOK_NAME,
                Verses.TEXT};
        String selection = Verses.BOOK_NUMBER + " = ? AND " + Verses.CHAPTER_NUMBER + " = ? AND " + Verses.NUMBER + " = ?";
        String[] selArgs = new String[]{String.valueOf(bookNr), String.valueOf(chapterNr), String.valueOf(verseNr)};
        String sortOrder = Verses._ID;
        Uri uri = getVersesUri(bibleId, false);
        ContentResolver cr = context.getContentResolver();
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(uri, projection, selection, selArgs, sortOrder);
        }
        // columns
        int idColumnIndex = cursor.getColumnIndex(Verses._ID);
        int bookNameColumnIndex = cursor.getColumnIndex(Verses.BOOK_NAME);
        int textColumnIndex = cursor.getColumnIndex(Verses.TEXT);
        Verse verse = null;
        if (cursor.getCount() == 1) {
            // read data from cursor
            cursor.moveToFirst();
            long id = cursor.getLong(idColumnIndex);
            String bookName = cursor.getString(bookNameColumnIndex);
            String text = cursor.getString(textColumnIndex);
            verse = new Verse(id, bibleId, bookNr, bookName, chapterNr, verseNr, text);
        }
        cursor.close();
        return verse;
    }

    // UPDATE

    /**
     * Updates the providers database according to the verse objects labels and highlight color.
     * If no labels or color are set, the verse is deleted from the bookmarks databse.
     * If the verse has no bookmark id, a new database entry is created. If labels or color are set and an id exists, the existing entry is updated.
     *
     * @param verse   the verse object
     * @param context a Context
     * @return long the id of the entry, or ID_NONE if the entry was deleted
     */
    public static long setBookmarks(Verse verse, Context context) {
        if (verse == null) return ID_NONE;
        BibleLog.d(TAG, "Setting bookmarks for " + verse.getLocator());
        if (verse.getHighLightColor() == Bookmarks.COLOR_NONE && verse.getLabels().size() == 0
                && (verse.getNotes() == null || verse.getNotes().isEmpty())) {
            // l�schen wenn leer
            deleteBookMark(verse.getBookMarkId(), context);
            verse.setBookMarkId(ID_NONE);
            BibleLog.d(TAG, "deleted " + verse.getLocator());
            return ID_NONE;
        }
        if (verse.getBookMarkId() == ID_NONE) {
            BibleLog.d(TAG, "inserting " + verse.getLocator());
            return BibleProviderUtils.insertBookmark(verse, context);
        } else {
            BibleLog.d(TAG, "updating " + verse.getLocator());
            // Uri with ID
            Uri uri = ContentUris.withAppendedId(Bookmarks.CONTENT_URI, verse.getBookMarkId());
            // values
            ContentValues values = new ContentValues();
            values.put(Bookmarks.LABELS, verse.getCommaSeparatedLabels());
            values.put(Bookmarks.HIGHLIGHT_COLOR, verse.getHighLightColor());
            values.put(Bookmarks.NOTES, verse.getNotes());
            // update
            ContentResolver cr = context.getContentResolver();
            int updatedRows;
            synchronized (lock) {
                updatedRows = cr.update(uri, values, null, null);
            }
            if (updatedRows == 0) {
                // this item is not present in the database
                return BibleProviderUtils.insertBookmark(verse, context);
            } else {
                return verse.getBookMarkId();
            }
        }
    }

    // DELETE

    /**
     * deletes a bookmarked verse from the provider
     *
     * @param bookMarkId long - the id of the bookmarked verse in the bookmarks table
     * @param context    a Context
     * @return int the number of deleted rows
     */
    public static void deleteBookMark(long bookMarkId, Context context) {
        if (bookMarkId == ID_NONE) {
            return;
        }
        // ID des Kalenders an URI anh�ngen
        Uri deleteUri = ContentUris.withAppendedId(Bookmarks.CONTENT_URI, bookMarkId);
        // ContentResolver
        ContentResolver cr = context.getContentResolver();
        // l�schen
        synchronized (lock) {
            cr.delete(deleteUri, null, null);
        }
    }

    public static void deleteLabel(Label deleteLabel, Context context) {
        BibleLog.d(TAG, "deleteLabel() called for '" + deleteLabel.getName() + "'");
        for (Verse verse : deleteLabel.getBookMarks()) {
            setBookmarks(verse, context);
        }
    }

    // HELPER METHODS

    private static Uri getVersesUri(long bibleId, boolean distinct) {
        Uri contentUri;
        if (distinct) {
            contentUri = Verses.CONTENT_URI_DISTINCT;
        } else {
            contentUri = Verses.CONTENT_URI;
        }
        // URI zusammensetzen
        Uri.Builder builder = contentUri.buildUpon();
        builder.appendPath(Verses.BIBLE_ID);
        builder.appendPath(String.valueOf(bibleId));
        return builder.build();
    }

    /**
     * For resarch only.
     * Called by BibleProvider.insert()
     */
    public static boolean bookmarkExists(Context context, long id) {
        String[] projection = new String[]{Bookmarks.VERSE_NUMBER};
        ContentResolver cr = context.getContentResolver();
        Uri uri = ContentUris.withAppendedId(Bookmarks.CONTENT_URI, id);
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(uri, projection, null, null, null);
        }
        if (cursor != null && cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Tells the ContentProvider to close the Database
     */
    public static void closeDb(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor;
        synchronized (lock) {
            cursor = cr.query(Bookmarks.CONTENT_URI_EXPORT, null, null, null, null);
        }
        if (cursor != null) cursor.close();
    }
}