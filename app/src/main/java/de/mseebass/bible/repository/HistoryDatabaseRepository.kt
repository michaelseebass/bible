package de.mseebass.bible.repository

import android.content.Context
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.providers.BibleProviderUtils

class HistoryDatabaseRepository(val context : Context) : IHistoryRepository {

    private val db : AppDatabase = AppDatabase.getInstance(context)


    override suspend fun getHistoryEntries(): RepoResult<List<HistoryEntry>> {
        try {
            //delay(5000)
            val response = db.historyDao().getHistoryEntries()

            val entriesNew = BibleProviderUtils.getVersesForHistory(context, response)
            // sort by timestamp
            entriesNew.sortWith(compareByDescending { it.timeStamp })

            return Success(entriesNew)
        } catch (e: Exception) {
            return Error(e)
        }
    }

    override suspend fun insert(entry: HistoryEntry) {
        val id = db.historyDao().insert(entry)
        entry.id = id
    }

    override suspend fun update(entry: HistoryEntry) {
        db.historyDao().update(entry)
    }

    override suspend fun delete(entry: HistoryEntry) {
        db.historyDao().delete(entry)
    }

    override suspend fun deleteAll() {
        db.historyDao().deleteAll()
    }

}