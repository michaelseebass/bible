package de.mseebass.bible.repository

import de.mseebass.bible.models.HistoryEntry

interface IHistoryRepository {

    @Throws(Exception::class)
    suspend fun getHistoryEntries() : RepoResult<List<HistoryEntry>>

    @Throws(Exception::class)
    suspend fun insert(entry : HistoryEntry)

    @Throws(Exception::class)
    suspend fun update(entry : HistoryEntry)

    @Throws(Exception::class)
    suspend fun delete(entry : HistoryEntry)

    @Throws(Exception::class)
    suspend fun deleteAll()

}