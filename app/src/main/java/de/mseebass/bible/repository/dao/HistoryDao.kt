package de.mseebass.bible.repository.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import de.mseebass.bible.models.HistoryEntry

@Dao
interface HistoryDao {

    @Query("SELECT * FROM history_entries")
    suspend fun getHistoryEntries() : List<HistoryEntry> //LiveData

    @Insert(onConflict = REPLACE)
    fun insert(entry : HistoryEntry) : Long

    @Update
    fun update(entry: HistoryEntry)

    @Delete
    fun delete(entry : HistoryEntry)

    @Query("DELETE FROM history_entries")
    fun deleteAll()

}