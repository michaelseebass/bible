package de.mseebass.bible.repository

sealed class RepoResult<out T : Any>
class Success<T : Any>(var data: T) : RepoResult<T>()
class Error(val exception: Throwable, val message: String = exception.localizedMessage ?: "Undefined error") : RepoResult<Nothing>()