package de.mseebass.bible.repository

import android.content.Context
import de.mseebass.bible.logic.HistoryEntryComparatorImpl
import de.mseebass.bible.logic.IHistoryEntryComparator
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.utils.BibleLog

class ManagedHistoryRepository(
        private val repo : IHistoryRepository,
        private val comparator: IHistoryEntryComparator,
        private val maxEntryCount: Int = MAX_ENTRY_COUNT
) : IHistoryRepository {

    companion object {
        // Log-TAG
        private const val TAG = "ManagedHistoryRepository"

        const val MAX_ENTRY_COUNT = 100

        fun createImpl(context: Context) = // TODO use DI
            ManagedHistoryRepository(
                    HistoryDatabaseRepository(context),
                    HistoryEntryComparatorImpl(),
                    MAX_ENTRY_COUNT)
    }

    private var cache : HashMap<Long, HistoryEntry> = HashMap();
    private var cacheInitialized = false
    private var cacheError : Throwable? = null


    private suspend fun initCache() {
        if (cacheInitialized)
            return

        when (val result = repo.getHistoryEntries()) {
            is Success<List<HistoryEntry>> -> {
                result.data.forEach { entry ->
                    insertInCache(entry)
                }
                cacheInitialized = true
            }
        }
    }

    private fun insertInCache(entry : HistoryEntry) {
        cache[entry.id!!] = entry
    }

    override suspend fun getHistoryEntries(): RepoResult<List<HistoryEntry>> {
        initCache()

        return if (cacheError != null) {
            Error(cacheError!!)
        } else {
            Success(cache.values.toList())
        }
    }

    override suspend fun insert(entry: HistoryEntry) {
        initCache()

        // delete "Chapter 1" if just opened before the actual chapter the user wanted (while navigating)
        deletePreviousOf(entry)

        // check if this is similar to a previous verse
        // if yes, remove the previous verse
        deleteDuplicates(entry)

        limitLength()

        repo.insert(entry)
        cache[entry.id!!] = entry
    }

    private suspend fun deleteDuplicates(entry: HistoryEntry) {
        try {
            val duplicates = cache.values.filterIndexed { _, compareEntry -> //TODO
                comparator.areHistoryEntriesSimilar(entry, compareEntry)
            }

            BibleLog.d(TAG, "Deleting ${duplicates.size} duplicates")
            duplicates.forEach { duplicateEntry ->
                // TODO bulk delete?
                try {
                    delete(duplicateEntry)
                } catch (e: Exception) {
                    BibleLog.e(TAG, "Error deleting duplicate HistoryEntry: $duplicateEntry", e)
                }
            }
        } catch (e: Exception) {
            BibleLog.e(TAG, "Error deleting duplicates for HistoryEntry: $entry", e)
        }
    }

    private suspend fun deletePreviousOf(entry: HistoryEntry) {
        try {
            BibleLog.i(TAG, "deletePreviousOf($entry)")
            val previousEntry = cache.values.maxBy { it.timeStamp }
            previousEntry?.let {
                if (comparator.isHistoryEntryStartOfSameBook(entry, it)) {
                    delete(it)
                }
            }
        } catch (e: Exception) {
            BibleLog.e(TAG, "Error deleting previous for HistoryEntry: $entry", e)
        }
    }

    private suspend fun limitLength() {
        while (cache.size >= maxEntryCount) {
            // delete last element (sorted by time)
            val last = cache.values.minBy { it.timeStamp }!!
            delete(last)
        }
    }

    override suspend fun update(entry: HistoryEntry) {
        if (entry.id == null) {
            BibleLog.w(TAG, "Cannot update HistoryEntry: " + entry.verse)
            return
        }

        initCache()

        repo.update(entry)
        cache[entry.id!!] = entry
    }

    override suspend fun delete(entry: HistoryEntry) {
        if (entry.id == null) {
            BibleLog.w(TAG, "Cannot delete HistoryEntry: " + entry.verse)
            return
        }

        initCache()

        repo.delete(entry)
        cache.remove(entry.id!!)
    }

    override suspend fun deleteAll() {
        initCache()

        repo.deleteAll()
        cache.clear()
    }

}