package de.mseebass.bible.repository

import androidx.core.os.postDelayed
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.CoroutineHelper

/**
 * Delays saving operations for 2 seconds.
 * If a new saving operation is posted, the previous pending operation is canceled.
 *
 * This prevents a chain off history entries when swiping through chapters quickly.
 * It is basically assumed that if a chapter is only opened for less than 2 seconds, the user didn't
 * really read anything there and there is no need to have that in the history.
 */
class DelayedRepoHandler(private val repo: IHistoryRepository) {

    private val handler = android.os.Handler()

    /**
     * Save the history entry after 2 seconds, if no new save operation is called in the meantime.
     * If the previous save call is still pending, it's canceled.
     */
    fun saveDelayed(entry: HistoryEntry) {
        BibleLog.i(TAG, "saveDelayed(): $entry")
        // first cancel pending save operations
        cancelPending()

        handler.postDelayed(HISTORY_SAVE_DELAY) {
            BibleLog.i(TAG, "...saving entry: $entry now")
            CoroutineHelper().saveHistoryAsync(repo, entry)
        }
    }

    private fun cancelPending() {
        BibleLog.i(TAG, "cancelPending()")
        handler.removeCallbacksAndMessages(null)
    }

    companion object {
        val HISTORY_SAVE_DELAY = 2000L
        val TAG = "DelayedRepoHandler"
    }

}