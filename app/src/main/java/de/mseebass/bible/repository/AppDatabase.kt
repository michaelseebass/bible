package de.mseebass.bible.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.repository.dao.HistoryDao

@Database(entities = [HistoryEntry::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun historyDao(): HistoryDao


    // https://medium.com/mindorks/android-architecture-components-room-and-kotlin-f7b725c8d1d

    companion object { // TODO ist das so gut????
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase::class.java, FILE_NAME_HISTORY)
                            .build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        private const val FILE_NAME_HISTORY = "bible_data_base_history";
    }

}