package de.mseebass.bible.tasks;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import de.mseebass.bible.models.Verse;
import de.mseebass.bible.providers.BibleProvider;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.utils.BibleLog;
import de.mseebass.bible.utils.ResourceUtils;
import de.mseebass.bible.utils.Settings;

public class AppUpdateTask extends AsyncTask<Void, Integer, Boolean> {

    public static final String TAG = "AppUpdateTask";
    public static final int PROGRESS_TYPE_QUERYING_BOOKMARKS = 2000;
    public static final int PROGRESS_TYPE_COPYING_DATABASE = 2001;
    public static final int PROGRESS_TYPE_UPDATING_BOOKMARKS = 2002;
    public static final int PROGRESS_TYPE_DONE = 2003;

    private static AppUpdateTask sTaskInstance;
    private Context mContext;
    private Listener mListener;
    private int mLastKnownAppVersion = -1;


    public interface Listener {
        void onAppUpdateTaskStarted();
        void onAppUpdateTaskProgress(int progressType, int percent);
        void onAppUpdateTaskFinished(boolean isUpdate);
    }

    /**
     * Private constructor for initialization of the singleton
     *
     * @param context a context for task instantiation
     */
    private AppUpdateTask(Context context) {
        this.mContext = context;
    }

    /**
     * Returns the single static instance of this task.
     * If the task has not been initialized yet, the task will be initialized first.
     * Otherwise the task is returned without any modification.
     *
     * @param context a context for task instantiation
     * @return the instance of CopyDatabaseTask
     */
    public static AppUpdateTask getInstance(Context context) {
        if (sTaskInstance == null) {
            sTaskInstance = new AppUpdateTask(context);
        }
        return sTaskInstance;
    }

    /**
     * Returns true if the task is currently running or allready finished
     *
     * @return boolean
     */
    public static boolean isRunning() {
        if (sTaskInstance == null) {
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.PENDING) {
            // Task has not started yet
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.RUNNING) {
            // Task is currently doing work in doInBackground()
            return true;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.FINISHED) {
            // Task is done and onPostExecute was called
            return true;
        }
        return false;
    }

    /**
     * Sets a listener callback for this task. The task will call
     * the corresponding listener methods during execution
     *
     * @param listener the callback interface
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    public void setLastKnownAppVersion(int lastKnownAppVersion) {
        this.mLastKnownAppVersion = lastKnownAppVersion;
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onAppUpdateTaskStarted();
        }
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            publishProgress(PROGRESS_TYPE_QUERYING_BOOKMARKS, -1);

            Vector<Verse> bookMarkedVerses = null;


            if (mLastKnownAppVersion != -1) {

                if (mLastKnownAppVersion < 13) {

                    File deprecatedDbFile = BibleProvider.getDeprecatedDatabaseFile(mContext);
                    if (deprecatedDbFile != null && deprecatedDbFile.exists()) { // TODO geht das so??????????????????????????????????????????????????
                        // TODO check if table exists
                        bookMarkedVerses = BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                                mContext,
                                BibleProviderUtils.QueryBookmarksFilterType.ALL,
                                BibleProviderUtils.QueryBookmarksPathType.LEGACY,
                                null);
                        // delete the old database
                        boolean deleted = deprecatedDbFile.delete();
                        if (!deleted) {
                            throw new IllegalStateException("Old database could not be deleted");
                        }
                    }
                }

//                if (mLastKnownAppVersion < 14) {
//                    ...remove empty db files
//                }
            }

            // copy databases
            copyMissingDatabases();

            publishProgress(PROGRESS_TYPE_DONE, 100);

            if (mLastKnownAppVersion != -1) {

                publishProgress(PROGRESS_TYPE_UPDATING_BOOKMARKS, -1);

                // update bookmarked verses

                if (mLastKnownAppVersion < 8) {
                    bookMarkedVerses = UpdateUtils.removeDuplicateLabels(bookMarkedVerses);
                }

                if (mLastKnownAppVersion < 10) {
                    bookMarkedVerses = UpdateUtils.updateMissingBibleShortNames(bookMarkedVerses);
                }

                if (mLastKnownAppVersion < 11) {
                    Settings.removeDeprecatedShowCaseSettings(mContext);
                    if (Settings.getBibleId(mContext) == 1) {
                        Settings.saveBibleId(1000, mContext);
                    }
                }

                if (mLastKnownAppVersion < 12) {
                    bookMarkedVerses = UpdateUtils.updateBibleIds(bookMarkedVerses);
                    bookMarkedVerses = UpdateUtils.updateElb71BookMarksV12(mContext, bookMarkedVerses);
                }

                if (mLastKnownAppVersion < 13) {
                    bookMarkedVerses = UpdateUtils.updateLut12BookMarks(mContext, bookMarkedVerses);
                    bookMarkedVerses = UpdateUtils.updateElb71BookMarksV13(mContext, bookMarkedVerses);
                    bookMarkedVerses = UpdateUtils.updateHighLightColors(mContext, bookMarkedVerses);
                    Settings.removeAppOldShortcutRemoved(mContext);
                }

                // write updated bookmarks to database
                if (bookMarkedVerses != null) {
                    for (int i = 0; i < bookMarkedVerses.size(); i++) {
                        Verse currentVerse = bookMarkedVerses.get(i);
                        if (currentVerse != null) {
                            long id = BibleProviderUtils.insertBookmark(currentVerse, mContext);
                            BibleLog.d(TAG, "inserting " + currentVerse.getLocator() + " - " + currentVerse.getBibleShortName() + "returns id: " + id);

                        } else {
                            BibleLog.w(TAG, "Verse is null !!!");
                        }
                    }
                }
            }

            publishProgress(PROGRESS_TYPE_DONE, 100);
            return true;
        } catch (IOException e) {
//			BibleProvider.deleteDatabase(mContext);
            BibleLog.d(TAG, "catched Exception:");
            e.printStackTrace();
        }
        return false;
    }

    private void copyMissingDatabases() throws IOException {
        BibleLog.d(TAG, "copyMissingDatabases()");
        List<String> availableDbFileNames = BibleProvider.getAvailableVerseDatabaseFileNames(mContext);
        int totalCount = availableDbFileNames.size() + 1;

        boolean checkFileSize = mLastKnownAppVersion != -1 && mLastKnownAppVersion < 14;
        BibleLog.i(TAG, checkFileSize ? "checking for file size" : "ignoring file size");

        if (!BibleProvider.dataBaseExists(mContext, BibleProvider.FILE_NAME_BIBLES, checkFileSize, 15000)) { //TODO file size
            copyDatabase(BibleProvider.FILE_NAME_BIBLES, totalCount, 0);
        }

        for (int i = 0; i < availableDbFileNames.size(); i++) {
            String fileName = availableDbFileNames.get(i);
            if (!BibleProvider.dataBaseExists(mContext, fileName, checkFileSize, 4500000)) { //TODO file size
                copyDatabase(fileName, totalCount, i + 1);
            }
        }
    }

    /**
     * Copies the database from the APK assets to the internal device storage
     * TODO test what happens if storage hasn't got enough space for the database to be fully copied
     *
     * @throws IOException
     */
    private void copyDatabase(String fileName, int progressItemCount, int progressItemIndex) throws IOException {
        BibleLog.v(TAG, "copyDatabase(): " + fileName);
        InputStream in = mContext.getAssets().open(fileName + ".zip");
        ZipInputStream zipIn = new ZipInputStream(new BufferedInputStream(in));
        try {
            ZipEntry zipEntry = zipIn.getNextEntry();

            long size = zipEntry.getSize();
            BibleLog.v(TAG, "size of ZIP: " + size);
            int copied = 0;
            int totalItemPercent = 0;

            File dbFile = mContext.getDatabasePath(fileName.replace(".zip", ""));
            dbFile.getParentFile().mkdir();
            dbFile.createNewFile();
            OutputStream out = new FileOutputStream(dbFile.getAbsolutePath());

            byte[] buffer = new byte[1024];
            int count;
            while ((count = zipIn.read(buffer)) != -1) {
                out.write(buffer, 0, count);

                copied += count;
                float itemProgress = (float) copied / (float) size;
                int newItemPercent = (int) (itemProgress * 100);

                if (newItemPercent - totalItemPercent >= (5 * progressItemCount)) {
                    totalItemPercent = newItemPercent;
                    // progress up until this item
                    float totalProgressGeneral = (float) (progressItemIndex) / (float) progressItemCount;
                    int totalPercentGeneral = (int) (totalProgressGeneral * 100);
                    float itemPercentRelative = (float) totalItemPercent / progressItemCount;
                    int totalPercentCurrent = totalPercentGeneral + (int) itemPercentRelative;
                    publishProgress(PROGRESS_TYPE_COPYING_DATABASE, totalPercentCurrent);
                }
            }
            BibleLog.v(TAG, "Copied " + copied + " bytes");
            if (copied == 0) {
                throw new IllegalStateException("No bytes were copied. Filename: + " + fileName + "Size: " + size);
            }

            out.flush();
            out.close();
        } finally {
            zipIn.close();
            in.close();
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if (mListener != null) {
            mListener.onAppUpdateTaskFinished(mLastKnownAppVersion > 0);
        }
        sTaskInstance = null;
        super.onPostExecute(success);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (mListener != null) {
            int progressType = values[0];
            int percent = values[1];
            mListener.onAppUpdateTaskProgress(progressType, percent);
        }
        super.onProgressUpdate(values);
    }

    private static class UpdateUtils {

        private static Vector<Verse> updateHighLightColors(Context context, Vector<Verse> bookMarkedVerses) {
            BibleLog.d(TAG, "updateHighLightColors()");
            for (int i = 0; i < bookMarkedVerses.size(); i++) {
                Verse currentVerse = bookMarkedVerses.get(i);

                // Legacy color codes
                final int colorHoloBlue = -1473006107;
                final int colorHoloGreen = -1466315776;
                final int colorHolorRed = -1459665852;
                final int colorHoloOrange = -1459635405;
                final int colorHoloPurple = -1465227572;

                ArrayList<Integer> newColors = ResourceUtils.INSTANCE.getSolidHighlightColorsArray(context);

                int oldColor = currentVerse.getHighLightColor();
                BibleLog.d(TAG, "oldColor: " + oldColor);

                switch (oldColor) {
                    case colorHoloBlue:
                        currentVerse.setHighLightColor(newColors.get(2));
                        break;
                    case colorHoloGreen:
                        currentVerse.setHighLightColor(newColors.get(4));
                        break;
                    case colorHolorRed:
                        currentVerse.setHighLightColor(newColors.get(0));
                        break;
                    case colorHoloOrange:
                        currentVerse.setHighLightColor(newColors.get(6));
                        break;
                    case colorHoloPurple:
                        currentVerse.setHighLightColor(newColors.get(1));
                        break;
                    case BibleProvider.Bookmarks.COLOR_NONE:
                        // do nothing
                        break;
                    default:
                        // unknown color found, lets set it to dark green
                        currentVerse.setHighLightColor(newColors.get(3));
                        break;
                }
            }
            return bookMarkedVerses;
        }

        private static Vector<Verse> updateElb71BookMarksV12(Context context, Vector<Verse> bookMarkedVerses) {
            Vector<Verse> updatedVerses = new Vector<Verse>();
            for (int i = 0; i < bookMarkedVerses.size(); i++) {
                Verse currentVerse = bookMarkedVerses.get(i);

                BibleLog.d(TAG, "before update: " + currentVerse.getLocator() + " " + currentVerse.getText() + " - " + currentVerse.getBibleShortName());

                if (currentVerse.getBibleId() == 0 && currentVerse.getBookNumber() == 66 && currentVerse.getChapterNumber() == 12 && currentVerse.getId() >= 30911) {
                    BibleLog.w(TAG, "skipping " + currentVerse.getLocator() + " - " + currentVerse.getBibleShortName());
                } else {
                    // find the corresponding verse in the new database
                    Verse newVerse = BibleProviderUtils.getVerse(context,
                            currentVerse.getBibleId(),
                            currentVerse.getBookNumber(),
                            currentVerse.getChapterNumber(),
                            currentVerse.getNumber());

                    if (newVerse == null) {
                        BibleLog.w(TAG, "updated: null");
                    } else {
                        newVerse.setLabels(currentVerse.getCommaSeparatedLabels());
                        newVerse.setHighLightColor(currentVerse.getHighLightColor());
                        newVerse.setBibleShortName(currentVerse.getBibleShortName());
                        // add this verse (may be null)
                        updatedVerses.add(newVerse);
                        BibleLog.d(TAG, "updated: " + newVerse.getLocator() + " " + newVerse.getText() + " - " + currentVerse.getBibleShortName());
                    }
                }
            }
            return updatedVerses;
        }

        private static Vector<Verse> updateElb71BookMarksV13(Context context, Vector<Verse> bookMarkedVerses) {
            Vector<Verse> updatedVerses = new Vector<Verse>();
            for (int i = 0; i < bookMarkedVerses.size(); i++) {
                Verse currentVerse = bookMarkedVerses.get(i);

                BibleLog.d(TAG, "before update: " + currentVerse.getLocator() + " " + currentVerse.getText() + " - " + currentVerse.getBibleShortName());

                // find the corresponding verse in the new database
                Verse newVerse = BibleProviderUtils.getVerse(context,
                        currentVerse.getBibleId(),
                        currentVerse.getBookNumber(),
                        currentVerse.getChapterNumber(),
                        currentVerse.getNumber());

                if (newVerse == null) {
                    BibleLog.w(TAG, "updated: null");
                } else {
                    newVerse.setLabels(currentVerse.getCommaSeparatedLabels());
                    newVerse.setHighLightColor(currentVerse.getHighLightColor());
                    newVerse.setBibleShortName(currentVerse.getBibleShortName());
                    // add this verse (may be null)
                    updatedVerses.add(newVerse);
                    BibleLog.d(TAG, "updated: " + newVerse.getLocator() + " " + newVerse.getText() + " - " + currentVerse.getBibleShortName());
                }
            }
            return updatedVerses;
        }

        private static Vector<Verse> updateLut12BookMarks(Context context, Vector<Verse> bookMarkedVerses) {
            Vector<Verse> updatedVerses = new Vector<Verse>();
            for (int i = 0; i < bookMarkedVerses.size(); i++) {
                Verse currentVerse = bookMarkedVerses.get(i);

                BibleLog.d(TAG, "before update: " + currentVerse.getLocator() + " " + currentVerse.getText() + " - " + currentVerse.getBibleShortName());

                // find the corresponding verse in the new database
                Verse newVerse = BibleProviderUtils.getVerse(context,
                        currentVerse.getBibleId(),
                        currentVerse.getBookNumber(),
                        currentVerse.getChapterNumber(),
                        currentVerse.getNumber());

                if (newVerse == null) {
                    BibleLog.w(TAG, "updated: null");
                } else {
                    newVerse.setLabels(currentVerse.getCommaSeparatedLabels());
                    newVerse.setHighLightColor(currentVerse.getHighLightColor());
                    newVerse.setBibleShortName(currentVerse.getBibleShortName());
                    // add this verse (may be null)
                    updatedVerses.add(newVerse);
                    BibleLog.d(TAG, "updated: " + newVerse.getLocator() + " " + newVerse.getText() + " - " + currentVerse.getBibleShortName());
                }
            }
            return updatedVerses;
        }

        private static Vector<Verse> updateBibleIds(Vector<Verse> bookMarkedVerses) {
            for (int i = 0; i < bookMarkedVerses.size(); i++) {
                Verse currentVerse = bookMarkedVerses.get(i);

                if (currentVerse.getBibleId() == 1) {
                    currentVerse.setBibleId(1000);
                }
            }
            return bookMarkedVerses;
        }

        private static Vector<Verse> updateMissingBibleShortNames(Vector<Verse> bookMarkedVerses) {
            String webShortName = "WEB";
            String elb71ShortName = "ELB71";

            for (int i = 0; i < bookMarkedVerses.size(); i++) {
                Verse currentVerse = bookMarkedVerses.get(i);

                if (currentVerse.getBibleId() == 0) {
                    currentVerse.setBibleShortName(elb71ShortName);
                } else if (currentVerse.getBibleId() == 1) {
                    currentVerse.setBibleShortName(webShortName);
                } else {
                    BibleLog.e(TAG, "unknown bible ID: " + currentVerse.getBibleId());
                }
            }
            return bookMarkedVerses;
        }

        private static Vector<Verse> removeDuplicateLabels(Vector<Verse> bookMarkedVerses) {
            BibleLog.d(TAG, "removeDuplicateLabels()");
            for (int i = 0; i < bookMarkedVerses.size(); i++) {
                Verse verse = bookMarkedVerses.get(i);

                ArrayList<String> labels = verse.getLabels();
                Collections.sort(labels);

                ArrayList<String> cleanedLabels = new ArrayList<String>();

                String lastKnownLabelName = "";
                for (String currentLabelName : labels) {
                    if (!currentLabelName.equals(lastKnownLabelName)) {
                        cleanedLabels.add(currentLabelName);
                        lastKnownLabelName = currentLabelName;
                    }
                }
                verse.setLabels(cleanedLabels);
            }
            return bookMarkedVerses;
        }
    }

}