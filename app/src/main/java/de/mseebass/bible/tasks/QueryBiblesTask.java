package de.mseebass.bible.tasks;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import de.mseebass.bible.models.Bible;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.utils.DebuggingUtils;

public class QueryBiblesTask extends AsyncTask<Void, Integer, List<Bible>> {
    public static final String TAG = "QueryBiblesTask";
    private static QueryBiblesTask sTaskInstance;
    private Context mContext;
    private Listener mListener;


    public interface Listener {
        void onQueryBiblesTaskStarted();
        void onQueryBiblesTaskFinished(List<Bible> results);
    }

    /**
     * Private constructor for initialization of the singleton
     *
     * @param context a context for task instantiation
     */
    private QueryBiblesTask(Context context) {
        this.mContext = context;
    }

    /**
     * Returns the single static instance of this task.
     * If the task has not been initialized yet, the task will be initialized first.
     * Otherwise the task is returned without any modification.
     *
     * @param context a context for task instantiation
     * @return the instance of QueryBiblesTask
     */
    public static QueryBiblesTask getInstance(Context context) {
        if (sTaskInstance == null) {
            sTaskInstance = new QueryBiblesTask(context);
        }
        return sTaskInstance;
    }

    /**
     * Returns a reference to an already existing task.
     *
     * @return a QueryBiblesTask instance, or null if no task exists
     */
    public static QueryBiblesTask getExistingInstance() {
        return sTaskInstance;
    }

    /**
     * Returns true if the task is currently running or already finished
     *
     * @return boolean
     */
    public static boolean isRunning() {
        if (sTaskInstance == null) {
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.PENDING) {
            // Task has not started yet
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.RUNNING) {
            // Task is currently doing work in doInBackground()
            return true;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.FINISHED) {
            // Task is done and onPostExecute was called
            return true;
        }
        return false;
    }

    /**
     * Sets a listener callback for this task. The task will call
     * the corresponding listener methods during execution
     *
     * @param listener the callback interface
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onQueryBiblesTaskStarted();
        }
        super.onPreExecute();
    }

    @Override
    protected List<Bible> doInBackground(Void... params) {
        try {
            DebuggingUtils.debugSleepThread();
            return BibleProviderUtils.queryBibles(mContext, true, "QueryBiblesTask");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Bible> results) {
        if (mListener != null) {
            mListener.onQueryBiblesTaskFinished(results);
        }
        sTaskInstance = null;
        super.onPostExecute(results);
    }

}