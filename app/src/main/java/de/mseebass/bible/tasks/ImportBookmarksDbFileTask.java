package de.mseebass.bible.tasks;

import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import de.mseebass.bible.application.BibleApplication;
import de.mseebass.bible.models.Verse;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.utils.BibleLog;

/**
 * Android device configuration changes result in a re-creation of the UI classes.
 * In these cases references to a already running task instances are lost.
 * This task class makes use of the singleton pattern to avoid this problem and
 * offers an easy-to-use callback interface for the UI.
 */
public class ImportBookmarksDbFileTask extends AsyncTask<Void, Integer, ImportBookmarksDbFileTask.Result> {

    /** TAG for printing Debug-Information to the LogCat */
    private static final String TAG = "ImportTask";

    /** The singleton instance of this task. */
    private static ImportBookmarksDbFileTask sTaskInstance;

    /** reference to the registered callback interface implementation */
    private Listener mListener;

    /** Context */
    private BibleApplication mContext;
    private String mEncodedPath;


    /**
     * Declaration of the callback interface
     */
    public interface Listener {
        void onImportBookmarksDbFileTaskStarted();
        void onImportBookmarksDbFileTaskFinished(Result result);
        void onImportBookmarksDbFileTaskProgress(int progressType, int percent);
    }

    public static final class Result {
        private int totalCount;
        private int successCount;
        private Map<Verse, Exception> errorVerses = new HashMap<>();
        private Exception exception = null;

        private Result() {
        }

        public int getTotalCount() {
            return totalCount;
        }

        private Result withTotalCount(int totalCount) {
            this.totalCount = totalCount;
            return this;
        }

        public int getSuccessCount() {
            return successCount;
        }

        private Result withSuccessCount(int successCount) {
            this.successCount = successCount;
            return this;
        }

        public Map<Verse, Exception> getErrorVerses() {
            return errorVerses;
        }

        private Result addErrorVerse(Verse errorVerse, Exception e) {
            this.errorVerses.put(errorVerse, e);
            return this;
        }

        public Exception getException() {
            return exception;
        }

        public Result withException(Exception e) {
            this.exception = e;
            return this;
        }
    }

    /**
     * Private Constructor. Only used internally for singleton creation.
     * @param context Context
     */
    private ImportBookmarksDbFileTask(BibleApplication context, String encodedPath) {
        this.mContext = context;
        this.mEncodedPath = encodedPath;
    }

    /**
     * Returns the singleton instance of this task and initializes it, if it doenst already exist.
     * @param context Context
     * @return the singleton instance of this task, never {@code null}
     */
    public static ImportBookmarksDbFileTask getInstance(BibleApplication context, String encodedPath) {
        if (sTaskInstance == null) {
            sTaskInstance = new ImportBookmarksDbFileTask(context, encodedPath);
        }
        return sTaskInstance;
    }

    /**
     * Returns the singleton instance of this task if it exists, else {@code null}
     * @return the singleton instance of this task, or {@code null}
     */
    public static ImportBookmarksDbFileTask getExistingInstance() {
        return sTaskInstance;
    }

    /**
     * Registers the callback interface of the UI. The task only accepts one listener at a time.
     * Calling this method will override any existing listener.
     * Caution: set this reference to {@code null} in onDestroy()-methods of Fragment/Activity
     * classes to avoid memory leaks.
     * @param listener the interface to register
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    /**
     * Checks, if the instance of this task is currently initialzed and running.
     * @return {@code true}, if the instance exists and is running, else {@code false}
     */
    public static boolean isRunning() {
        if (sTaskInstance == null) {
            return false;
        }
        if (sTaskInstance.getStatus() == Status.PENDING) {
            return false;
        }
        if (sTaskInstance.getStatus() == Status.RUNNING) {
            return true;
        }
        if (sTaskInstance.getStatus() == Status.FINISHED) {
            return true;
        }
        return false;
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onImportBookmarksDbFileTaskStarted();
        }
        super.onPreExecute();
    }

    @Override
    protected Result doInBackground(Void... params) {
        publishProgress(-1, -1);
        int totalCount = 0;
        int successCount = 0;
        Result result = new Result();

        try {
            // Einträge aus importierter DB
            Vector<Verse> importedVerses = BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                    mContext, BibleProviderUtils.QueryBookmarksFilterType.ALL, BibleProviderUtils.QueryBookmarksPathType.RESTORE, mEncodedPath);
            // Lokale DB
            Vector<Verse> existingBookmarks = BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                    mContext, BibleProviderUtils.QueryBookmarksFilterType.ALL, BibleProviderUtils.QueryBookmarksPathType.DEFAULT, null);

            /*
            BibleLog.i(TAG, "Importing Bookmarks............");
            BibleLog.i(TAG, "Verses in imported DB: " + importedVerses.size());
            for (Verse existingVerse : importedVerses) BibleLog.d(TAG, "IMPORTED: " + existingVerse);
            BibleLog.i(TAG, "Verses in existing DB: " + existingBookmarks.size());
            for (Verse existingVerse : existingBookmarks) BibleLog.d(TAG, "EXISTING: " + existingVerse);
            */

            totalCount = importedVerses.size();
            int bookmarkIndex = 0;
            float lastPublishedPercent = 0.0f;

            importLoop:
            for (; bookmarkIndex < totalCount;) {
                Verse importedVerse = importedVerses.get(bookmarkIndex);

                try {
                    for (Verse existingVerse : existingBookmarks) {
                        // on conflict : merge
                        if (importedVerse.matches(existingVerse)) {
                            BibleLog.d(TAG, "Match: " + existingVerse + " - " + importedVerse);
                            existingVerse.mergeLabelsAndColor(importedVerse);
                            // save
                            BibleProviderUtils.setBookmarks(existingVerse, mContext);

//                            bookmarkIndex++;
//                            lastPublishedPercent = publishProgressInternal(bookmarkIndex, totalCount, lastPublishedPercent);
                            successCount++;
                            continue importLoop;
                        }
                    }
                    BibleLog.d(TAG, "No Match: " + importedVerse);
//                  bookmarkIndex++;
//                  lastPublishedPercent = publishProgressInternal(bookmarkIndex, totalCount, lastPublishedPercent);
                    // save
                    BibleProviderUtils.setBookmarks(importedVerse, mContext);
                    successCount++;
                } catch (Exception e) {
                    // Innerer catch, um nicht wg. einem Fehler alle Einträge zu verwerfen
                    BibleLog.e(TAG, "Error processing imported bookmarks:", e);
                    result.addErrorVerse(importedVerse, e);
                } finally {
                    bookmarkIndex++;
                    lastPublishedPercent = publishProgressInternal(bookmarkIndex, totalCount, lastPublishedPercent);
                }
            }

            publishProgress(-1, 100);
        } catch (Exception e) {
            BibleLog.e(TAG, "Error importing : " + mEncodedPath, e);
            result.withException(e);
        } finally {
            // Anzahl der importierten Verse
            result.withTotalCount(totalCount)
                    .withSuccessCount(successCount);
        }
        return result;
    }

    private float publishProgressInternal(int currentIndex, int totalCount, float lastPublishedPercent) {
        float nr = currentIndex + 1;
        float percent = (nr / (float) totalCount) * 100.0f;
        if (lastPublishedPercent == 0.0f || percent - lastPublishedPercent >= 1.0f) {
            lastPublishedPercent = percent;
            publishProgress(-1, (int) lastPublishedPercent);
        }
        return lastPublishedPercent;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Log.d(TAG, "onProgressUpdate() called with: values = [" + String.valueOf(values[1]) + "]");
        if (mListener != null) {
            int progressType = values[0];
            int percent = values[1];
            mListener.onImportBookmarksDbFileTaskProgress(progressType, percent);
        }
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Result result) {
        if (mListener != null) {
            mListener.onImportBookmarksDbFileTaskFinished(result);
            mListener = null;
        }
        sTaskInstance = null;
        super.onPostExecute(result);
    }

    @Override
    protected void onCancelled(Result result) {
        mListener = null;
        sTaskInstance = null;
    }

}