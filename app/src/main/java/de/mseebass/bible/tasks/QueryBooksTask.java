package de.mseebass.bible.tasks;

import android.content.Context;
import android.os.AsyncTask;

import java.util.Vector;

import de.mseebass.bible.models.Book;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.utils.BibleLog;
import de.mseebass.bible.utils.DebuggingUtils;

/**
 * @author Michael Seeba� B.Eng., 2013
 */
public class QueryBooksTask extends AsyncTask<Void, Integer, Vector<Book>> {

    public static final String TAG = "QueryBooksTask";
    private static Vector<QueryBooksTask> sTaskInstances = new Vector<QueryBooksTask>();
    private Context mContext;
    private long mBibleId;
    private Listener mListener;


    public interface Listener {
        void onQueryBooksTaskStarted();
        void onQueryBooksTaskFinished(Vector<Book> results);
    }

    /**
     * Private constructor
     *
     * @param context Context
     * @param bibleId ID of the bible translation
     */
    private QueryBooksTask(Context context, long bibleId) {
        this.mContext = context;
        this.mBibleId = bibleId;
    }

    /**
     * Returns a reference to an already existing task.
     *
     * @param bibleId the ID of the bible translation
     * @return a QueryBooksTask instance, or null if no task exists for the given arguments
     */
    public static QueryBooksTask getExistingInstance(long bibleId) {
        for (int i = 0; i < sTaskInstances.size(); i++) {
            QueryBooksTask task = sTaskInstances.get(i);
            if (task != null && task.mBibleId == bibleId) {
                return task;
            }
        }
        return null;
    }

    /**
     * Returns a reference to a new or already existing task. If a task with the given arguments
     * already exists, the reference to this task is returned. Otherwise a new task is created.
     *
     * @param context a Context
     * @param bibleId the ID of the bible translation
     * @return a QueryBooksTask instance, never null
     */
    public static QueryBooksTask getInstance(Context context, long bibleId) {
        QueryBooksTask task = getExistingInstance(bibleId);
        if (task == null) {
            task = new QueryBooksTask(context, bibleId);
            sTaskInstances.add(task);
        }
//		BibleLog.w(TAG, String.valueOf(sTaskInstances.size()) + " QueryBooksTask instances running");
        return task;
    }

    /**
     * Checks if a task instance is running or completed for the given chapter
     *
     * @param bibleId the bible id
     * @return boolean - true, if the task exists and is running or completed, false otherwise
     */
    public static boolean isInstanceRunning(long bibleId) {
        QueryBooksTask task = getExistingInstance(bibleId);
        if (task == null) {
            return false;
        } else {
            return task.isRunning();
        }
    }

    /**
     * Cancels all existing task instances, unregisters their listeners and releases the references
     * to these tasks, leaving the internal task list empty.
     */
    public static void releaseAllInstances() {
        for (int i = 0; i < sTaskInstances.size(); i++) {
            QueryBooksTask task = sTaskInstances.get(i);
            if (task != null) {
                task.cancel(true);

            }
        }
        sTaskInstances.clear();
    }

//    /**
//     * Unregisters all existing task instance Listeners
//     */
//    public static void unregisterAllListeners() {
//        for (int i = 0; i < sTaskInstances.size(); i++) {
//            QueryBooksTask task = sTaskInstances.get(i);
//            if (task != null) {
//                task.setListener(null);
//            }
//        }
//    }

    /**
     * Returns true if the task is currently running or already finished
     *
     * @return boolean
     */
    public boolean isRunning() {
        if (getStatus() == AsyncTask.Status.PENDING) {
            // Task has not started yet
            return false;
        }
        if (getStatus() == AsyncTask.Status.RUNNING) {
            // Task is currently doing work in doInBackground()
            return true;
        }
        if (getStatus() == AsyncTask.Status.FINISHED) {
            // Task is done and onPostExecute was called
            return true;
        }
        return false;
    }

    /**
     * Sets a listener callback for this task. The task will call
     * the corresponding listener methods during execution
     *
     * @param listener the callback interface
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onQueryBooksTaskStarted();
        } else {
            BibleLog.w(TAG, "Listener is not set");
//			throw new IllegalStateException("Listener is not set for chapter " + mChapterNumber);
        }
        super.onPreExecute();
    }

    @Override
    protected Vector<Book> doInBackground(Void... params) {
        try {
			DebuggingUtils.debugSleepThread();
            return BibleProviderUtils.queryBooks(mBibleId, mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Vector<Book> results) {
        if (mListener != null) {
            mListener.onQueryBooksTaskFinished(results);
        } else {
            BibleLog.w(TAG, "Listener is not set");
//			throw new IllegalStateException("Listener is not set for chapter " + mChapterNumber);
        }
        boolean removed = sTaskInstances.remove(this);
        if (!removed) {
            throw new IllegalStateException("Reference to the QueryBooksTask instance for bibleId " +
                    +mBibleId + " could not be released while executing onPostExecute()");
        }
        super.onPostExecute(results);
    }

    @Override


    protected void onCancelled() {
        boolean removed = sTaskInstances.remove(this);
        if (!removed) {
            throw new IllegalStateException("Reference to the QueryBooksTask instance for bibleId " +
                    +mBibleId + " could not be released while executing onCancelled()");
        }
        super.onCancelled();
    }

    @Override
    protected void onCancelled(Vector<Book> result) {
        boolean removed = sTaskInstances.remove(this);
        if (!removed) {
            throw new IllegalStateException("Reference to the QueryBooksTask instance for bibleId " +
                    +mBibleId + " could not be released while executing onCancelled(Vector<Book>)");
        }
        super.onCancelled(result);
    }

}