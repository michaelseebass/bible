package de.mseebass.bible.tasks;

import android.os.AsyncTask;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import de.mseebass.bible.application.BibleApplication;
import de.mseebass.bible.models.Verse;
import de.mseebass.bible.providers.BibleProvider;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.utils.BibleLog;
import de.mseebass.bible.utils.BookmarkUtils;
import de.mseebass.bible.utils.EncodingUtils;
import de.mseebass.bible.utils.FilesUtils;

/**
 * Android device configuration changes result in a re-creation of the UI classes.
 * In these cases references to a already running task instances are lost.
 * This task class makes use of the singleton pattern to avoid this problem and
 * offers an easy-to-use callback interface for the UI.
 */
public class ExportBookmarksDbFileTask extends AsyncTask<Void, Integer, File> {

    /** TAG for printing Debug-Information to the LogCat */
    private static final String TAG = "ExportBookmarksDbFileTask";

    /** The singleton instance of this task. */
    private static ExportBookmarksDbFileTask sTaskInstance;

    /** reference to the registered callback interface implementation */
    private Listener mListener;

    /** Context */
    private BibleApplication mAppContext;


    private Result mResult = null;


    public enum ExportBookmarksDbFileTaskResultCode {
        EXTERNAL_DIR_NOT_EXISTING,
        RESULT_UNAVAILABLE,
        SUCCESS
    }

    public static final class Result {
        private ExportBookmarksDbFileTaskResultCode resultCode;
        private int successCount;
        private File file;
        private Exception exception = null;

        private Result() {
        }

        public ExportBookmarksDbFileTaskResultCode getResultCode() {
            return resultCode;
        }

        private Result withResultCode(ExportBookmarksDbFileTaskResultCode resultCode) {
            this.resultCode = resultCode;
            return this;
        }

        public Exception getException() {
            return exception;
        }

        public Result withException(Exception e) {
            this.exception = e;
            return this;
        }

        public File getFile() {
            return file;
        }

        public Result withFile(File file) {
            this.file = file;
            return this;
        }
    }


    /**
     * Declaration of the callback interface
     */
    public interface Listener {
        void onExportBookmarksDbFileTaskStarted();
        void onExportBookmarksDbFileTaskFinished(Result result);
    }

    /**
     * Private Constructor. Only used internally for singleton creation.
     * @param appContext BibleApplication
     */
    private ExportBookmarksDbFileTask(BibleApplication appContext) {
        this.mAppContext = appContext;
    }

    /**
     * Returns the singleton instance of this task and initializes it, if it doenst already exist.
     * @param appContext BibleApplication
     * @return the singleton instance of this task, never {@code null}
     */
    public static ExportBookmarksDbFileTask getInstance(BibleApplication appContext) {
        if (sTaskInstance == null) {
            sTaskInstance = new ExportBookmarksDbFileTask(appContext);
        }
        return sTaskInstance;
    }

    /**
     * Returns the singleton instance of this task if it exists, else {@code null}
     * @return the singleton instance of this task, or {@code null}
     */
    public static ExportBookmarksDbFileTask getExistingInstance() {
        return sTaskInstance;
    }

    /**
     * Registers the callback interface of the UI. The task only accepts one listener at a time.
     * Calling this method will override any existing listener.
     * Caution: set this reference to {@code null} in onDestroy()-methods of Fragment/Activity
     * classes to avoid memory leaks.
     * @param listener the interface to register
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    /**
     * Checks, if the instance of this task is currently initialzed and running.
     * @return {@code true}, if the instance exists and is running, else {@code false}
     */
    public static boolean isRunning() {
        if (sTaskInstance == null) {
            return false;
        }
        switch (sTaskInstance.getStatus()) {
            case FINISHED:
                return true;
            case PENDING:
                return false;
            case RUNNING:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onExportBookmarksDbFileTaskStarted();
        }
        super.onPreExecute();
    }

    @Override
    protected File doInBackground(Void... params) {
        return exportBookmarksDbFile();
    }

    private synchronized File exportBookmarksDbFile() {
        mResult = new Result();
        // lets wait for the database to be completely written and closed
        sleep();

        // tell the ContentProvider to close the Database
        BibleProviderUtils.closeDb(mAppContext);

        // lets wait for the database to be completely written and closed
        sleep();

        File currentDbFile = mAppContext.getDatabasePath(BibleProvider.FILE_NAME_BOOKMARKS);
        // this should never happen
//        if (!currentDbFile.exists()) {
//        }
        BibleLog.i(TAG, "Exporting backup: before copy to SD: MD5: " + getMd5(currentDbFile));
        BibleLog.i(TAG, "Exporting backup: before copy to SD: Size: " + FilesUtils.getFileSizeString(currentDbFile, false));

        File destinationFile;
        try {
            // get a File to copy our db file to
            destinationFile = BookmarkUtils.getBackupFile();
//            if (destinationFile.exists()) {
//                FileUtils.deleteQuietly(destinationFile);
//            }
        } catch (IOException e) {
            e.printStackTrace();
            mResult.withException(e)
                    .withResultCode(ExportBookmarksDbFileTaskResultCode.EXTERNAL_DIR_NOT_EXISTING);
            return null;
        }

        try {
            BookmarkUtils.copyFile(currentDbFile, destinationFile);
            if (destinationFile.exists()) {
                // test if exported file can be opened and queried as a database
                verifyCopiedFile(destinationFile);
                BibleLog.i(TAG, "Exporting backup: after copy to SD: MD5: " + getMd5(destinationFile));
                BibleLog.i(TAG, "Exporting backup: after copy to SD: Size: " + FilesUtils.getFileSizeString(destinationFile, true));
                mResult.withResultCode(ExportBookmarksDbFileTaskResultCode.SUCCESS);
                return destinationFile;
            }
        } catch(Exception e) {
            mResult.withException(e);
            e.printStackTrace();
            BibleLog.e(TAG, "Failed to export DB file. Reason: " + e.getMessage());
        }
        if (destinationFile != null && destinationFile.exists()) {
            destinationFile.delete();
        }
        mResult.withResultCode(ExportBookmarksDbFileTaskResultCode.RESULT_UNAVAILABLE);
        return null;
    }

    private String getMd5(File currentDbFile) {
        try {
            return EncodingUtils.INSTANCE.getHexEncodedMD5(currentDbFile);
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }

    private void sleep() {
        try {
            Thread.sleep(500);
        } catch ( Exception ignored) {}
    }

    private void verifyCopiedFile(File file) throws Exception {
        String encodedPath = EncodingUtils.INSTANCE.encodeBase64ToString(file.getAbsolutePath());
        List<Verse> importedVerses = BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                mAppContext,
                BibleProviderUtils.QueryBookmarksFilterType.ALL,
                BibleProviderUtils.QueryBookmarksPathType.RESTORE,
                encodedPath
        );
        if (importedVerses == null) throw new Exception("Verification failed. Result is null!");
        BibleProviderUtils.closeDb(mAppContext);
    }

    @Override
    protected void onPostExecute(File result) {
        if (mListener != null) {
            mResult.withFile(result);
            mListener.onExportBookmarksDbFileTaskFinished(mResult);
            mListener = null;
        }
        sTaskInstance = null;
        super.onPostExecute(result);
    }

    @Override
    protected void onCancelled(File result) {
        mListener = null;
        sTaskInstance = null;
    }

}
