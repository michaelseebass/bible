package de.mseebass.bible.tasks;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;
import java.util.Vector;

import de.mseebass.bible.models.Verse;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.ui.search.SearchWords;
import de.mseebass.bible.utils.BookmarkUtils;
import de.mseebass.bible.utils.DebuggingUtils;

public class SearchTask extends AsyncTask<Void, Integer, Vector<Verse>> {
    public static final String TAG = "SearchTask";
    private static SearchTask sTaskInstance;
    private Context mContext;
    private String mQuery;
    private long mBibleId;
    private List<Integer> mSelectedBookNumbers;
    private Listener mListener;


    public interface Listener {
        void onSearchTaskStarted();
        void onSearchTaskFinished(Vector<Verse> results);
    }


    /**
     * Private constructor for initialization of the singleton
     *
     * @param context             a context for task instantiation
     * @param selectedBookNumbers the books to be included in this search
     */
    private SearchTask(Context context, long bibleId, String query, List<Integer> selectedBookNumbers) {
        this.mContext = context;
        this.mBibleId = bibleId;
        this.mQuery = query;
        this.mSelectedBookNumbers = selectedBookNumbers;
    }

    /**
     * Returns the single static instance of this task.
     * If the task has not been initialized yet, the task will be initialized first.
     * Otherwise the task is returned without any modification.
     *
     * @param context             a context for task instantiation
     * @param selectedBookNumbers the books to be included in this search
     * @return the instance of CopyDatabaseTask
     */
    public static SearchTask getInstance(Context context, long bibleId, String query, List<Integer> selectedBookNumbers) {
        if (sTaskInstance == null) {
            sTaskInstance = new SearchTask(context, bibleId, query, selectedBookNumbers);
        }
        return sTaskInstance;
    }

    /**
     * Returns true if the task is currently running or already finished
     *
     * @return boolean
     */
    public static boolean isRunning() {
        if (sTaskInstance == null) {
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.PENDING) {
            // Task has not started yet
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.RUNNING) {
            // Task is currently doing work in doInBackground()
            return true;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.FINISHED) {
            // Task is done and onPostExecute was called
            return true;
        }
        return false;
    }

    /**
     * Sets a listener callback for this task. The task will call
     * the corresponding listener methods during execution
     *
     * @param listener the callback interface
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onSearchTaskStarted();
        }
        super.onPreExecute();
    }

    @Override
    protected Vector<Verse> doInBackground(Void... params) {
        try {
            DebuggingUtils.debugSleepThread();

            List<String> wordList = new SearchWords(mQuery).split();

            Vector<Verse> searchResults = BibleProviderUtils.search(mBibleId, mSelectedBookNumbers, wordList, mContext, true);

            Vector<Verse> bookmarkedVerses = BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                    mContext, BibleProviderUtils.QueryBookmarksFilterType.ALL, BibleProviderUtils.QueryBookmarksPathType.DEFAULT, null);
            // set the bookmarks
            BookmarkUtils.setBookMarkedVerses(searchResults, bookmarkedVerses);
            // search in the database
            return searchResults;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Vector<Verse> results) {
        if (mListener != null) {
            mListener.onSearchTaskFinished(results);
        }
        sTaskInstance = null;
        super.onPostExecute(results);
    }

}