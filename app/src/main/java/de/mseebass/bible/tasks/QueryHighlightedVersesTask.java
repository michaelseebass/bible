package de.mseebass.bible.tasks;

import android.content.Context;
import android.os.AsyncTask;

import java.util.Vector;

import de.mseebass.bible.models.Verse;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.utils.DebuggingUtils;

public class QueryHighlightedVersesTask extends AsyncTask<Void, Integer, Vector<Verse>> {

    public static final String TAG = "QueryHighlightedVersesTask";
    private static QueryHighlightedVersesTask sTaskInstance;
    private Context mContext;
    private Listener mListener;


    public interface Listener {
        void onQueryHighlightedVersesTaskStarted();
        void onQueryHighlightedVersesTaskFinished(Vector<Verse> results);
    }

    /**
     * Private constructor for initialization of the singleton
     *
     * @param context a context for task instantiation
     */
    private QueryHighlightedVersesTask(Context context) {
        this.mContext = context;
    }

    /**
     * Returns the single static instance of this task.
     * If the task has not been initialized yet, the task will be initialized first.
     * Otherwise the task is returned without any modification.
     *
     * @param context a context for task instantiation
     * @return the instance of QueryHighlightedVersesTask
     */
    public static QueryHighlightedVersesTask getInstance(Context context) {
        if (sTaskInstance == null) {
            sTaskInstance = new QueryHighlightedVersesTask(context);
        }
        return sTaskInstance;
    }

    /**
     * Returns the single static instance of this task.
     *
     * @param context a context for task instantiation
     * @return the instance of QueryHighlightedVersesTask
     */
    public static QueryHighlightedVersesTask getExistingInstance(Context context) {
        return sTaskInstance;
    }

    /**
     * Returns true if the task is currently running or already finished
     *
     * @return boolean
     */
    public static boolean isRunning() {
        if (sTaskInstance == null) {
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.PENDING) {
            // Task has not started yet
            return false;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.RUNNING) {
            // Task is currently doing work in doInBackground()
            return true;
        }
        if (sTaskInstance.getStatus() == AsyncTask.Status.FINISHED) {
            // Task is done and onPostExecute was called
            return true;
        }
        return false;
    }

    /**
     * Sets a listener callback for this task. The task will call
     * the corresponding listener methods during execution
     *
     * @param listener the callback interface
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onQueryHighlightedVersesTaskStarted();
        }
        super.onPreExecute();
    }

    @Override
    protected Vector<Verse> doInBackground(Void... params) {
        try {
            DebuggingUtils.debugSleepThread();
            return BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                    mContext, BibleProviderUtils.QueryBookmarksFilterType.ONLY_HIGHLIGHTED, BibleProviderUtils.QueryBookmarksPathType.DEFAULT, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Vector<Verse> results) {
        if (mListener != null) {
            mListener.onQueryHighlightedVersesTaskFinished(results);
        }
        sTaskInstance = null;
        super.onPostExecute(results);
    }

}