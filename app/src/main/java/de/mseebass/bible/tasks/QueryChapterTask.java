package de.mseebass.bible.tasks;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;

import java.util.Vector;

import de.mseebass.bible.models.Chapter;
import de.mseebass.bible.models.Verse;
import de.mseebass.bible.providers.BibleProviderUtils;
import de.mseebass.bible.utils.DebuggingUtils;

/**
 * @author Michael Seebaß B.Eng., 2013
 */
public class QueryChapterTask extends AsyncTask<Void, Integer, Chapter> {
    public static final String TAG = "QueryChapterTask";
    private static Vector<QueryChapterTask> sTaskInstances = new Vector<QueryChapterTask>();
    private Context mContext;
    private int mChapterNumber;
    private int mBookNumber;
    private long mBibleId;
    private Listener mListener;


    public interface Listener {
        void onQueryChapterTaskStarted();
        void onQueryChapterTaskFinished(Chapter chapter);
    }

    /**
     * Private constructor
     *
     * @param context   Context
     * @param bibleId   ID of the bible translation
     * @param bookNr    the number of the book
     * @param chapterNr the number of the chapter
     */
    private QueryChapterTask(Context context, long bibleId, int bookNr, int chapterNr) {
        this.mContext = context;
        this.mBibleId = bibleId;
        this.mBookNumber = bookNr;
        this.mChapterNumber = chapterNr;
    }

    /**
     * Returns a reference to an already existing task.
     *
     * @param bibleId   the bible id of the given chapter
     * @param bookNr    the book number of the given chapter
     * @param chapterNr the number of the given chapter
     * @return a QueryChapterTask instance, or null if no task exists for the given arguments
     */
    public static QueryChapterTask getExistingInstance(long bibleId, int bookNr, int chapterNr) {
        for (int i = 0; i < sTaskInstances.size(); i++) {
            QueryChapterTask task = sTaskInstances.get(i);
            if (task != null
                    && task.mBibleId == bibleId
                    && task.mBookNumber == bookNr
                    && task.mChapterNumber == chapterNr) {
                return task;
            }
        }
        return null;
    }

    /**
     * Returns a reference to a new or already existing task. If a task with the given arguments
     * already exists, the reference to this task is returned. Otherwise a new task is created.
     *
     * @param context   a Context
     * @param bibleId   the bible id of the given chapter
     * @param bookNr    the book number of the given chapter
     * @param chapterNr the number of the given chapter
     * @return a QueryChapterTask instance, never null
     */
    public static QueryChapterTask getInstance(Context context, long bibleId, int bookNr, int chapterNr) {
        QueryChapterTask task = getExistingInstance(bibleId, bookNr, chapterNr);
        if (task == null) {
            task = new QueryChapterTask(context, bibleId, bookNr, chapterNr);
            sTaskInstances.add(task);
        }
//		BibleLog.w(TAG, String.valueOf(sTaskInstances.size()) + " QueryChapterTask instances running");
        return task;
    }

    /**
     * Checks if a task instance is running or completed for the given chapter
     *
     * @param bibleId   the bible id of the given chapter
     * @param bookNr    the book number of the given chapter
     * @param chapterNr the number of the given chapter
     * @return boolean - true, if the task exists and is running or completed, false otherwise
     */
    public static boolean isInstanceRunning(long bibleId, int bookNr, int chapterNr) {
        QueryChapterTask task = getExistingInstance(bibleId, bookNr, chapterNr);
        if (task == null) {
            return false;
        } else {
            return task.isRunning();
        }
    }

    /**
     * Cancels all existing task instances, unregisters their listeners and releases the references
     * to these tasks, leaving the internal task list empty
     */
    public static void releaseAllInstances() {
        for (int i = 0; i < sTaskInstances.size(); i++) {
            QueryChapterTask task = sTaskInstances.get(i);
            if (task != null) {
                task.setListener(null);
                task.cancel(true);
            }
        }
        sTaskInstances.clear();
    }

//    /**
//     * Unregisters all existing task instance Listeners
//     */
//    public static void unregisterAllListeners() {
//        for (int i = 0; i < sTaskInstances.size(); i++) {
//            QueryChapterTask task = sTaskInstances.get(i);
//            if (task != null) {
//                task.setListener(null);
//            }
//        }
//    }

    /**
     * Returns true if the task is currently running or already finished
     *
     * @return boolean
     */
    public boolean isRunning() {
        if (getStatus() == AsyncTask.Status.PENDING) {
            // Task has not started yet
            return false;
        }
        if (getStatus() == AsyncTask.Status.RUNNING) {
            // Task is currently doing work in doInBackground()
            return true;
        }
        if (getStatus() == AsyncTask.Status.FINISHED) {
            // Task is done and onPostExecute was called
            return true;
        }
        return false;
    }

    /**
     * Sets a listener callback for this task. The task will call
     * the corresponding listener methods during execution
     *
     * @param listener the callback interface
     */
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (mListener != null) {
            mListener.onQueryChapterTaskStarted();
        } else {
//			throw new IllegalStateException("Listener is not set for chapter " + mChapterNumber);
        }
        super.onPreExecute();
    }

    @Override
    protected Chapter doInBackground(Void... params) {
        try {
            DebuggingUtils.debugSleepThread();
            // query the chapter
            Chapter chapter = BibleProviderUtils.queryChapter(mBibleId, mBookNumber, mChapterNumber, mContext, true);
            // query the bookmarked verses
            // TODO dont query them multiple times in parallel
            Vector<Verse> bookmarkedVerses = BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                    mContext, BibleProviderUtils.QueryBookmarksFilterType.ALL, BibleProviderUtils.QueryBookmarksPathType.DEFAULT, null);
            // set the bookmarks
            if (chapter != null) {
                chapter.setBookMarkedVerses(bookmarkedVerses);
                return chapter;
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Chapter result) {
        if (mListener != null) {
            mListener.onQueryChapterTaskFinished(result);
        } else {
//			throw new IllegalStateException("Listener is not set for chapter " + mChapterNumber);
        }
        boolean removed = sTaskInstances.remove(this);
        if (!removed) {
            throw new IllegalStateException("Reference to the QueryChapterTask instance for chapter " +
                    +mChapterNumber + " could not be released while executing onPostExecute()");
        }
        super.onPostExecute(result);
    }

    @Override


    protected void onCancelled() {
        boolean removed = sTaskInstances.remove(this);
        if (!removed) {
            throw new IllegalStateException("Reference to the QueryChapterTask instance for chapter " +
                    +mChapterNumber + " could not be released while executing onCancelled()");
        }
        super.onCancelled();
    }

    @Override
    protected void onCancelled(Chapter result) {
        boolean removed = sTaskInstances.remove(this);
        if (!removed) {
            throw new IllegalStateException("Reference to the QueryChapterTask instance for chapter " +
                    +mChapterNumber + " could not be released while executing onCancelled(Chapter)");
        }
        super.onCancelled(result);
    }

}