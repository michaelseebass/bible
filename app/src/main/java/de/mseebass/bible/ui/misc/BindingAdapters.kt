package de.mseebass.bible.ui.misc

import android.view.View
import androidx.databinding.BindingAdapter

// TODO not used?
@BindingAdapter("app:visible")
fun visible(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}