package de.mseebass.bible.ui.appupdate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.mseebass.bible.R
import de.mseebass.bible.databinding.AppUpdateFragmentBinding
import de.mseebass.bible.tasks.AppUpdateTask
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment

class AppUpdateFragment : BaseFragment(), AppUpdateTask.Listener {

    private lateinit var binding: AppUpdateFragmentBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.app_update_fragment, container, false)
        return binding.root
    }

    override fun onResume() {
        if (activity == null) return
        if (AppUpdateTask.isRunning()) {
            AppUpdateTask.getInstance(activity).setListener(this)
        }
        super.onResume()
    }

    override fun onAppUpdateTaskStarted() {
        // nothing
    }

    override fun onAppUpdateTaskProgress(progressType: Int, percent: Int) {
        // progress
        if (percent in 0..100) {
            binding.appUpdateFragmentProgressbar.isIndeterminate = false
            binding.appUpdateFragmentProgressbar.progress = percent
            binding.appUpdateFragmentTvStateProgress.text = "$percent%"
        } else {
            binding.appUpdateFragmentProgressbar.isIndeterminate = true
            binding.appUpdateFragmentTvStateProgress.text = ""
        }
        binding.appUpdateFragmentTvStateMsg.apply {
            when (progressType) {
                AppUpdateTask.PROGRESS_TYPE_QUERYING_BOOKMARKS -> setText(R.string.app_update_fragment_state_message_querying_bookmarks)
                AppUpdateTask.PROGRESS_TYPE_COPYING_DATABASE -> setText(R.string.app_update_fragment_state_message_copying_databas)
                AppUpdateTask.PROGRESS_TYPE_UPDATING_BOOKMARKS -> setText(R.string.app_update_fragment_state_message_updating_bookmarks)
                AppUpdateTask.PROGRESS_TYPE_DONE -> text = ""
                else -> {}
            }
        }
    }

    override fun onAppUpdateTaskFinished(isUpdate: Boolean) {
        if (isUpdate) {
            exit()
        } else {
            binding.appUpdateFragmentBtnDone.apply {
                isEnabled = true
                setOnClickListener {
                    exit()
                }
            }
        }
    }

    private fun exit() {
        val activity = activity as MainActivity?
        activity?.onCopyDatabaseTaskFinished()
    }

    companion object {
        // Log-TAG
        const val TAG = "CopyDatabaseFragment"
    }
}