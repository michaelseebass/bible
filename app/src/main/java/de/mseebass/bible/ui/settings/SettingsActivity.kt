package de.mseebass.bible.ui.settings

import android.content.res.Resources.Theme
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import de.mseebass.bible.R
import de.mseebass.bible.databinding.SettingsActivityBinding
import de.mseebass.bible.ui.misc.BaseActivity
import de.mseebass.bible.utils.ResourceUtils.getThemedResourceId
import de.mseebass.bible.utils.Settings

class SettingsActivity : BaseActivity() {

    private lateinit var binding: SettingsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.settings_activity)

        //set status bar color
        val colorResId = getThemedResourceId(R.attr.colorPrimaryDark, this)
        window.statusBarColor = resources.getColor(colorResId)

        setSupportActionBar(binding.settingsActivityToolbar)
        supportFragmentManager.beginTransaction().replace(R.id.settings_activity_framelayout, SettingsFragment()).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onApplyThemeResource(theme: Theme, resid: Int, first: Boolean) {
        theme.applyStyle(Settings.getThemeId(this), true)
    }
}