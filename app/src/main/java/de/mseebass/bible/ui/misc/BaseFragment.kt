package de.mseebass.bible.ui.misc

import android.os.Bundle
import androidx.fragment.app.Fragment
import de.mseebass.bible.application.BibleApplication

abstract class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        savedInstanceState?.classLoader = javaClass.classLoader
        super.onCreate(savedInstanceState)
    }

    fun getBaseActivity() : BaseActivity {
        return activity as BaseActivity
    }

    fun getAppContext() : BibleApplication {
        return activity?.applicationContext as BibleApplication
    }

    fun showToast(text: String?) {
        val baseActivity = getBaseActivity()
        baseActivity.showToast(text)
    }

    fun showToast(strRes: Int) {
        val baseActivity = getBaseActivity()
        baseActivity.showToast(strRes)
    }

    fun showToastLong(text: String?) {
        val baseActivity = getBaseActivity()
        baseActivity.showToastLong(text)
    }

    fun showToastLong(strRes: Int) {
        val baseActivity = getBaseActivity()
        baseActivity.showToastLong(strRes)
    }

}