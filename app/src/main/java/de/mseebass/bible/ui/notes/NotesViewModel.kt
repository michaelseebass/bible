package de.mseebass.bible.ui.notes

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import de.mseebass.bible.models.Verse
import de.mseebass.bible.providers.BibleProviderUtils
import de.mseebass.bible.repository.*
import de.mseebass.bible.utils.BibleLog
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class NotesViewModel(appContext : Application) : AndroidViewModel(appContext) {

    val result by lazy { MutableLiveData<RepoResult<List<Verse>>>() }
    val isLoading by lazy { MutableLiveData<Boolean>() }


    fun fetchData(forceUpdate: Boolean) {
        try {
            val existingData = result.value as? Success<List<Verse>>
            if (existingData == null || forceUpdate) {
                fetchRepo()
            }
        } catch (e: Exception) {
            BibleLog.e("NotesViewModel", "Error fetching data:", e)
        }
    }

    private fun fetchRepo() = viewModelScope.launch {
        isLoading.value = true
        try {
//            delay(10000)
            val verses = BibleProviderUtils.queryBookmarkedAndHighlightedVerses(
                    getApplication(),
                    BibleProviderUtils.QueryBookmarksFilterType.ONLY_NOTES,
                    BibleProviderUtils.QueryBookmarksPathType.DEFAULT,
                    null
            )
            result.value = Success(verses)
        } catch (e: java.lang.Exception) {
            result.value = Error(e)
        }
        isLoading.value = false
    }

}