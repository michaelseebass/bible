package de.mseebass.bible.ui.backup

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import de.mseebass.bible.R

class BackupPagerAdapter(
        fm: FragmentManager,
        private val mContext: Context
) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(pos: Int): Fragment {
        return when (pos) {
            0 -> BackupExportFragment()
            1 -> BackupImportFragment()
            else -> BackupImportFragment()
        }
    }

    override fun getItemPosition(`object`: Any)=  PagerAdapter.POSITION_NONE

    override fun getCount() = 2

    override fun getPageTitle(pos: Int): CharSequence? {
        return when (pos) {
            0 -> mContext.getString(R.string.backup_activity_tab_title_export)
            1 -> mContext.getString(R.string.backup_activity_tab_title_import)
            else -> null
        }
    }

    companion object {
        val TAG = BackupPagerAdapter::class.java.simpleName
    }

}