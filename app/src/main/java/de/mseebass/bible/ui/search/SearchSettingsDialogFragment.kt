package de.mseebass.bible.ui.search

import android.app.Dialog
import android.os.Bundle
import android.os.Parcelable
import android.util.SparseArray
import android.view.View
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import de.mseebass.bible.R
import de.mseebass.bible.models.Book
import de.mseebass.bible.utils.ResourceUtils.getPixel
import java.util.*

class SearchSettingsDialogFragment : DialogFragment(), RadioGroup.OnCheckedChangeListener {

    private var mListener: OnBooksSelectedListener? = null
    private var mBooks = mutableListOf<Book?>()
    private var mSelectedBookNumbers = ArrayList<Int>()
    private val mCheckBoxSparseArray = SparseArray<CheckBox>()
    private var mSelectedFilterMode = 0
    private var bookContainer: LinearLayout? = null


    fun setListener(listener: OnBooksSelectedListener?) {
        mListener = listener
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_SELECTED_BOOKS_COUNT, mSelectedBookNumbers.size)
        for (i in mSelectedBookNumbers.indices) {
            outState.putInt(KEY_SELECTED_BOOK_PREFIX + i, mSelectedBookNumbers[i])
        }
        outState.putInt(KEY_BOOKS_COUNT, mBooks.size)
        for (i in mBooks.indices) {
            outState.putParcelable(KEY_BOOK_PREFIX + i, mBooks[i])
        }
        outState.putInt(KEY_FILTER_MODE, mSelectedFilterMode)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        savedInstanceState?.classLoader = javaClass.classLoader
        super.onCreate(savedInstanceState)

        savedInstanceState?.let {
            if (it.containsKey(KEY_SELECTED_BOOKS_COUNT)) {
                val selectedBooksCount = it.getInt(KEY_SELECTED_BOOKS_COUNT)
                mSelectedBookNumbers = ArrayList()
                for (i in 0 until selectedBooksCount) {
                    mSelectedBookNumbers.add(it.getInt(KEY_SELECTED_BOOK_PREFIX + i))
                }
            }
            if (it.containsKey(KEY_BOOKS_COUNT)) {
                val booksCount = it.getInt(KEY_BOOKS_COUNT)
                mBooks
                for (i in 0 until booksCount) {
                    mBooks.add(it.getParcelable<Parcelable>(KEY_BOOK_PREFIX + i) as Book?)
                }
            }
            mSelectedFilterMode = it.getInt(KEY_FILTER_MODE)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(R.string.search_settings_dialog_fragment_title)
        builder.setIcon(R.drawable.ic_launcher_bible_small)
        val rootView = View.inflate(activity, R.layout.search_settings_dialog_fragment, null)
        val rg = rootView.findViewById<View>(R.id.search_settings_dialog_fragment_radio_group) as RadioGroup
        rg.setOnCheckedChangeListener(this)

        // CheckBoxes
        bookContainer = rootView.findViewById(R.id.search_settings_dialog_fragment_distinct_books_container) as LinearLayout
        for (book in mBooks) {
            val cb = AppCompatCheckBox(activity)
            cb.text = book!!.name
            cb.height = getPixel(48, activity!!)
            // needs to be Integer (not int) for the remove()-Method in onCheckedChanged()
            val bookNumber = book.number
            //cb.setChecked(mSelectedBookNumbers.contains(bookNumber));
            cb.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    if (!mSelectedBookNumbers.contains(bookNumber)) {
                        mSelectedBookNumbers.add(bookNumber)
                    }
                } else {
                    mSelectedBookNumbers.remove(bookNumber)
                }
            }
            bookContainer?.addView(cb)
            mCheckBoxSparseArray.put(bookNumber, cb)
        }
        when {
            isAllSelected() -> rg.check(R.id.search_settings_dialog_fragment_radio_button_all)
            isOldTestamentSelected() -> rg.check(R.id.search_settings_dialog_fragment_radio_button_old_testament)
            isNewTestamentSelected() -> rg.check(R.id.search_settings_dialog_fragment_radio_button_new_testament)
            else -> rg.check(R.id.search_settings_dialog_fragment_radio_button_distinct)
        }

        // set layout as Alert View
        builder.setView(rootView)
        // Buttons
        builder.setPositiveButton(R.string.dialog_ok) { _, _ ->
            mListener?.onBooksSelected(mSelectedBookNumbers)
            if (mSelectedBookNumbers.isEmpty()) { //TODO
                Toast.makeText(activity, R.string.search_settings_dialog_fragment_no_selection_message, Toast.LENGTH_LONG).show()
            }
        }
        builder.setNegativeButton(R.string.dialog_cancel, null)
        return builder.create()
    }

    private fun isAllSelected(): Boolean {
        // if the list of selected books doesn't contain one of the books, return false
        for (i in 1..LAST_BOOK_NR_NEW_TESTAMENT) {
            if (!mSelectedBookNumbers.contains(i)) {
                return false
            }
        }
        return true
        // TODO just check size ?
    }

    private fun isOldTestamentSelected(): Boolean {
        // if the list of selected books doesn't contain any of the old testaments books, return false
        for (i in 1..LAST_BOOK_NR_OLD_TESTAMENT) {
            if (!mSelectedBookNumbers.contains(i)) {
                return false
            }
        }
        // if the list of selected books does contain any of the new testaments books, return false
        for (i in LAST_BOOK_NR_OLD_TESTAMENT + 1..LAST_BOOK_NR_NEW_TESTAMENT) {
            if (mSelectedBookNumbers.contains(i)) {
                return false
            }
        }
        return true
    }

    private fun isNewTestamentSelected(): Boolean {
        // if the list of selected books does contain any of the old testaments books, return false
        for (i in 1..LAST_BOOK_NR_OLD_TESTAMENT) {
            if (mSelectedBookNumbers.contains(i)) {
                return false
            }
        }
        // if the list of selected books doesn't contain any of the new testaments books, return false
        for (i in LAST_BOOK_NR_OLD_TESTAMENT + 1..LAST_BOOK_NR_NEW_TESTAMENT) {
            if (!mSelectedBookNumbers.contains(i)) {
                return false
            }
        }
        return true
    }

    private fun isDistinctSelected(): Boolean {
        // check if all books of the old testament are selected
        var oldTestamentSelected = true
        for (i in 1..LAST_BOOK_NR_OLD_TESTAMENT) {
            if (!mSelectedBookNumbers.contains(i)) {
                oldTestamentSelected = false
                break
            }
        }
        if (oldTestamentSelected) {
            return false
        }
        // check if all books of the new testament are selected
        var newTestamentSelected = true
        for (i in LAST_BOOK_NR_OLD_TESTAMENT + 1..LAST_BOOK_NR_NEW_TESTAMENT) {
            if (!mSelectedBookNumbers.contains(i)) {
                newTestamentSelected = false
                break
            }
        }
        return !newTestamentSelected
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        val newFilterMode: Int = when (checkedId) {
            R.id.search_settings_dialog_fragment_radio_button_all -> FILTER_ALL
            R.id.search_settings_dialog_fragment_radio_button_old_testament -> FILTER_OLD_TESTAMENT
            R.id.search_settings_dialog_fragment_radio_button_new_testament -> FILTER_NEW_TESTAMENT
            R.id.search_settings_dialog_fragment_radio_button_distinct -> FILTER_DISTINCT
            else -> return
        }
        onFilterModeSelected(newFilterMode)
    }

    private fun onFilterModeSelected(newFilterMode: Int) {
        if (newFilterMode == mSelectedFilterMode) {
            // catch double calls to onCheckedChanged()
            return
        }
        when (newFilterMode) {
            FILTER_ALL -> onAllBooksSelected()
            FILTER_OLD_TESTAMENT -> onOldTestamentSelected()
            FILTER_NEW_TESTAMENT -> onNewTestamentSelected()
            FILTER_DISTINCT -> onDistinctSelected()
            else -> throw IllegalArgumentException("Unknown filter mode supplied: $newFilterMode")
        }
    }

    private fun onAllBooksSelected() {
        setAllCheckBoxesEnabled(false)
        mSelectedBookNumbers.clear()
        for (currentBook in mBooks) {
            mSelectedBookNumbers.add(currentBook!!.number)
        }
    }

    private fun onOldTestamentSelected() {
        setAllCheckBoxesEnabled(false)
        mSelectedBookNumbers.clear()
        for (i in 1..LAST_BOOK_NR_OLD_TESTAMENT) {
            mSelectedBookNumbers.add(i)
        }
    }

    private fun onNewTestamentSelected() {
        setAllCheckBoxesEnabled(false)
        mSelectedBookNumbers.clear()
        for (i in LAST_BOOK_NR_OLD_TESTAMENT + 1..LAST_BOOK_NR_NEW_TESTAMENT) {
            mSelectedBookNumbers.add(i)
        }
    }

    private fun onDistinctSelected() {
        if (isDistinctSelected()) {
            for (i in 0 until mCheckBoxSparseArray.size()) {
                val bookNr = mCheckBoxSparseArray.keyAt(i)
                // get the object by the key.
                val cb = mCheckBoxSparseArray[bookNr]
                val bookSelected = mSelectedBookNumbers.contains(bookNr)
                cb.isChecked = bookSelected
            }
        } else {
            mSelectedBookNumbers.clear()
        }
        setAllCheckBoxesEnabled(true)
    }

    private fun setAllCheckBoxesEnabled(enabled: Boolean) {
        for (i in 0 until bookContainer!!.childCount) {
            val cb = bookContainer!!.getChildAt(i) as CheckBox
            cb.isEnabled = enabled
        }
    }

    companion object {
        // TAG
        const val TAG = "SearchSettingsDialogFragment"

        //private static SearchSettingsDialogFragment sDialog;
        const val LAST_BOOK_NR_OLD_TESTAMENT = 39
        const val LAST_BOOK_NR_NEW_TESTAMENT = 66
        const val FILTER_ALL = 1000
        const val FILTER_OLD_TESTAMENT = 1001
        const val FILTER_NEW_TESTAMENT = 1002
        const val FILTER_DISTINCT = 1003
        private const val KEY_SELECTED_BOOKS_COUNT = "SELECTED_BOOKS_COUNT"
        private const val KEY_SELECTED_BOOK_PREFIX = "SELECTED_BOOK_"
        private const val KEY_BOOKS_COUNT = "BOOKS_COUNT"
        private const val KEY_BOOK_PREFIX = "BOOK_"
        private const val KEY_FILTER_MODE = "FILTER_MODE"

        @JvmStatic
        fun showDialog(books: MutableList<Book?>, selectedBookNumbers: ArrayList<Int>, listener: OnBooksSelectedListener?, fm: FragmentManager) {
            val dialog = SearchSettingsDialogFragment()
            dialog.mBooks = books
            dialog.mSelectedBookNumbers = selectedBookNumbers
            dialog.mListener = listener
            dialog.show(fm, TAG)
        }
    }
}