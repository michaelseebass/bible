package de.mseebass.bible.ui.search

import android.content.Context
import android.text.Spannable
import android.text.style.BackgroundColorSpan
import androidx.core.content.ContextCompat
import de.mseebass.bible.R
import de.mseebass.bible.models.Verse
import java.util.*

internal fun getQuerySpannable(context: Context, verse: Verse, queryWords: List<String>): Spannable {
    val text = verse.text
    val regions = text.getRegionsForQueryWords(queryWords)
    val spannable = highlightRegions(context, verse, regions)
    return spannable
}


internal fun highlightRegions(context: Context, verse: Verse, regions: List<QueryRegion>): Spannable {
    // SpannableString
    val src = verse.getSpannable()
    val locatorOffset = verse.getLocator().length + 1
    for (region in regions) {
                src.setSpan(
                        BackgroundColorSpan(ContextCompat.getColor(context, R.color.search_highlight_transparent)),
                        region.start + locatorOffset,
                        region.end + locatorOffset,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    return src
}

internal fun String.getRegionsForQueryWords(queryWords: List<String>): List<QueryRegion> {
    val lowerCaseText = toLowerCase(Locale.getDefault())

    val regionList = mutableListOf<QueryRegion>()

    for (word in queryWords) {
        val lowerCaseQuery = word.toLowerCase(Locale.getDefault())

        var start = lowerCaseText.indexOf(lowerCaseQuery)
        while (start >= 0) {
            val end = start + word.length

            val region = QueryRegion(start, end)

            var found = false

            for (existingRegion in regionList) {
                if (region.intersectsOrTouches(existingRegion)) {
                    existingRegion.add(region)
                    found = true
                    break
                }
            }

            if (!found) {
                regionList.add(region)
            }

            // naechstes Vorkommen suchen
            start = lowerCaseText.indexOf(lowerCaseQuery, start + word.length)
        }
    }
    return regionList
}

internal data class QueryRegion(val start: Int, val end: Int) {
    fun intersectsOrTouches(other: QueryRegion) : Boolean {
        if (start > (other.end + 1)) {
            return false
        }
        if (end < (other.start - 1)) {
            return false
        }
        return true
    }

    fun add(other: QueryRegion): QueryRegion {
        val newStart = if (start < other.start) start else other.start
        val newEnd = if (end > other.end) end else other.end
        return QueryRegion(newStart, newEnd)
    }
}
