package de.mseebass.bible.ui.highlights

import android.app.Dialog
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import de.mseebass.bible.R
import de.mseebass.bible.interfaces.OnColorSelectedListener
import de.mseebass.bible.providers.BibleProvider.Bookmarks
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils.getDisplayHighlightColor
import de.mseebass.bible.utils.ResourceUtils.getSolidHighlightColorsArray
import de.mseebass.bible.utils.ResourceUtils.getThemedResourceId

class ColorPickerDialogFragment : DialogFragment() {
    // Callback
    private var mOnColorSelectedListener: OnColorSelectedListener? = null
    private var mBaseView: View? = null

    fun setOnColorSelectedListener(onColorSelectedListener: OnColorSelectedListener?) {
        mOnColorSelectedListener = onColorSelectedListener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val style = getThemedResourceId(R.attr.bibleAlertDialogStyle, activity!!)
        val alertDialogBuilder = AlertDialog.Builder(activity!!, style)
        alertDialogBuilder.setTitle(R.string.color_picker_dialog_title)
        alertDialogBuilder.setIcon(R.drawable.ic_launcher_bible_small)

        mBaseView = View.inflate(activity, R.layout.color_picker_dialog, null)

        // color items
        val solidColors = getSolidHighlightColorsArray(activity!!)
        setUpImageView(R.id.color_picker_dialog_red, solidColors[0])
        setUpImageView(R.id.color_picker_dialog_deep_purple, solidColors[1])
        setUpImageView(R.id.color_picker_dialog_blue, solidColors[2])
        setUpImageView(R.id.color_picker_dialog_teal, solidColors[3])
        setUpImageView(R.id.color_picker_dialog_light_green, solidColors[4])
        setUpImageView(R.id.color_picker_dialog_yellow, solidColors[5])
        setUpImageView(R.id.color_picker_dialog_orange, solidColors[6])
        setUpImageView(R.id.color_picker_dialog_transparent, Bookmarks.COLOR_NONE)
        alertDialogBuilder.setView(mBaseView)
        alertDialogBuilder.setNegativeButton(R.string.dialog_cancel) { _, _ ->
            // nothing
        }
        return alertDialogBuilder.create()
    }

    private fun setUpImageView(imageViewResId: Int, solidColor: Int) {
        val imageView = mBaseView!!.findViewById<ImageView>(imageViewResId)
        val gradientDrawable = imageView.background as GradientDrawable
        if (solidColor != Bookmarks.COLOR_NONE) {
            val displayColor = getDisplayHighlightColor(solidColor)
            gradientDrawable.setColor(displayColor)
        }
        imageView.setOnClickListener {
            mOnColorSelectedListener?.onColorSelected(solidColor)
            dialog?.dismiss()
        }
    }

    companion object {
        // TAG
        @JvmField
        var TAG = "ColorPickerDialogFragment"
    }
}