package de.mseebass.bible.ui.history

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.mseebass.bible.R
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.utils.getDescription
import kotlinx.android.synthetic.main.history_list_item.view.*

class HistoryRecyclerViewAdapter (private val mainActivity : MainActivity,
                                  var entries: List<HistoryEntry>)
        : RecyclerView.Adapter<HistoryRecyclerViewAdapter.HistoryViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : HistoryViewHolder {
        val v: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.history_list_item, null,false)
        return HistoryViewHolder(v)
    }


    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        try {
            val entry = entries[position]
//        if (entry != null && entry.verse != null)
            val verse = entry.verse

            holder.locatorTv.text = verse.getLocator()
            holder.bibleTv.text = " • " + verse.bibleShortName
            holder.timeTv.text = entry.timeStamp.getDescription(mainActivity)
            holder.textTv.text = verse.text

            holder.container.setOnClickListener {
                mainActivity.startReaderWithIntent(entry.verse)
            }
        } catch (e: Exception) {
            try {
                holder.locatorTv.text = "-"
                holder.bibleTv.text = "-"
                @SuppressLint("SetTextI18n")
                holder.textTv.text = "Error: " + e.javaClass.simpleName + "* " + e.message
            } catch (e2: java.lang.Exception) {}
        }
    }

    override fun getItemCount(): Int {
        return entries.size
    }

//    override fun getItemId(position: Int): Long {
//        return super.getItemId(position)
//    }
//
//    override fun getItemViewType(position: Int): Int {
//        return super.getItemViewType(position)
//    }

    class HistoryViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val container = itemView
        val locatorTv = itemView.history_list_item_locator_tv
        val bibleTv = itemView.history_list_item_bible_tv
        val textTv = itemView.history_list_item_text_tv
        val timeTv = itemView.history_list_item_time_tv
    }
}