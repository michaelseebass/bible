package de.mseebass.bible.ui.notes

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import de.mseebass.bible.R
import de.mseebass.bible.databinding.NotesDialogBinding
import de.mseebass.bible.interfaces.OnNotesChangedListener
import de.mseebass.bible.models.Verse
import de.mseebass.bible.providers.BibleProviderUtils
import de.mseebass.bible.utils.ResourceUtils.getThemedResourceId

class NotesDialogFragment: DialogFragment() {

    internal var listener: OnNotesChangedListener? = null

    private var verse: Verse? = null
    private lateinit var binding: NotesDialogBinding

    companion object {
        const val TAG = "NotesDialogFragment"

        fun instance(verse: Verse, listener: OnNotesChangedListener) = NotesDialogFragment().apply {
            this.verse = verse
            this.listener = listener
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            verse = it.getParcelable("verse")
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putParcelable("verse", verse)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val style = getThemedResourceId(R.attr.bibleAlertDialogStyle, activity!!)
        val alertDialogBuilder = AlertDialog.Builder(activity!!, style)
        alertDialogBuilder.setTitle(R.string.notes_dialog_title)
        alertDialogBuilder.setIcon(R.drawable.ic_launcher_bible_small)

        binding = DataBindingUtil.inflate(activity!!.layoutInflater, R.layout.notes_dialog, null, false)
//        binding.notesVerseLocatorTv.text = verse?.getLocator()
        binding.notesVerseTv.text = verse?.getSpannable()
        binding.notesEt.setText(verse?.notes)
        alertDialogBuilder.setView(binding.root)
        alertDialogBuilder.setPositiveButton(R.string.dialog_ok) { _, _ ->
            ok()
        }
        alertDialogBuilder.setNegativeButton(R.string.dialog_cancel) { _, _ ->
            // nothing
        }
        return alertDialogBuilder.create()
    }

    private fun ok() {
        verse?.notes = binding.notesEt.text.toString().trim()
        // Database entry - this sets the bookmark id for the verse object
        activity?.let {BibleProviderUtils.setBookmarks(verse, it) }

        listener?.onNotesChanged()
    }

}