package de.mseebass.bible.ui.highlights

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import de.mseebass.bible.R
import de.mseebass.bible.databinding.HighlightedVersesFragmentBinding
import de.mseebass.bible.models.Verse
import de.mseebass.bible.tasks.QueryHighlightedVersesTask
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.ui.reader.ReaderFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.Settings
import java.util.*

class HighlightedVersesFragment : BaseFragment(), QueryHighlightedVersesTask.Listener, OnHighlightedVerseDeletedListener {
    // Content-Layout
    private lateinit var binding: HighlightedVersesFragmentBinding

    // Labels
    private var mVerses: Vector<Verse>? = null
    private var mQueryHighlightedVersesTaskRunning = false
    private var mLastDbQueryTime: Long = -1

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_QUERY_HIGHLIGHTED_VERSES_TASK_RUNNING, mQueryHighlightedVersesTaskRunning)
        if (mVerses != null) {
            // Bibel-Vector
            outState.putInt(KEY_VERSE_COUNT, mVerses!!.size)
            for (i in mVerses!!.indices) {
                outState.putParcelable(KEY_VERSE_PREFIX + i, mVerses!![i])
            }
            outState.putLong(KEY_LAST_DB_QUERY_TIME, mLastDbQueryTime)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        // load data
        savedInstanceState?.let {
            mQueryHighlightedVersesTaskRunning = it.getBoolean(KEY_QUERY_HIGHLIGHTED_VERSES_TASK_RUNNING)
            if (it.containsKey(KEY_VERSE_COUNT)) {
                mVerses = Vector()
                // Vector
                val labelCount = it.getInt(KEY_VERSE_COUNT)
                for (i in 0 until labelCount) {
                    mVerses!!.add(it.getParcelable<Parcelable>(KEY_VERSE_PREFIX + i) as Verse?)
                }
            }
            mLastDbQueryTime = it.getLong(KEY_LAST_DB_QUERY_TIME)
        }
        fragmentManager?.findFragmentByTag(ReaderFragment.FRAGMENT_TRANSACTION_TAG)?.let{
            setTargetFragment(it, 0)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Layout
        binding = DataBindingUtil.inflate(inflater, R.layout.highlighted_verses_fragment, container, false)
        // check if the database has been updated (after restoring)
        if (!Settings.areQueriedBookmarksValid(activity, mLastDbQueryTime)) {
            // Database edited - invalidate previously queried verses
            mVerses = null
        }
        // Task
        if (mVerses == null) {
            // Task
            val task = QueryHighlightedVersesTask.getInstance(activity)
            task.setListener(this)
            if (!QueryHighlightedVersesTask.isRunning()) {
                task.execute()
            }
            mQueryHighlightedVersesTaskRunning = true
        }
        // UI
        updateUi()
        // Rückgabewert
        return binding.root
    }

    private fun updateUi() {
        if (mQueryHighlightedVersesTaskRunning) {
            displayProgressBar()
        } else if (mVerses == null) {
            displayErrorMsg()
        } else if (mVerses!!.size == 0) {
            displayEmptyStateMsg()
        } else {
            setUpDropDownNavigation()
        }
    }

    override fun onQueryHighlightedVersesTaskStarted() {
        mQueryHighlightedVersesTaskRunning = true
        displayProgressBar()
    }

    override fun onQueryHighlightedVersesTaskFinished(results: Vector<Verse>?) {
        mQueryHighlightedVersesTaskRunning = false
        mVerses = results
        mLastDbQueryTime = System.currentTimeMillis()
        updateUi()
    }

    override fun onResume() {
        // notify the navigation drawer
        val activity = activity as MainActivity?
        activity!!.onNavigationDrawerItemSelected(MainActivity.POSITION_HIGHLIGHTED_VERSES)
        super.onResume()
    }

    override fun onDestroy() {
        // unregister this fragment as task listener (in order to prevent memory leaks)
        QueryHighlightedVersesTask.getExistingInstance(activity)?.setListener(null)
        super.onDestroy()
    }

    private fun displayListView(verses: Vector<Verse>?) {
        binding.emptyMessageTv.isVisible = false
        binding.emptyHintTv.isVisible = false
        binding.errorMessageTv.isVisible = false
        binding.progressbar.isVisible = false
        // ListView
        binding.listview.apply {
            isVisible = true
            adapter = HighlightedVersesListAdapter(activity as MainActivity, verses, this@HighlightedVersesFragment)
        }
    }

    private fun displayEmptyStateMsg() {
        val mainActivity = activity as MainActivity?
        if (mainActivity == null) {
            BibleLog.e(TAG, "getActivity() == null")
            return
        }
        // ActionBar
        mainActivity.resetToolbar()

        binding.emptyMessageTv.isVisible = true
        binding.emptyHintTv.isVisible = true
        binding.errorMessageTv.isVisible = false
        binding.progressbar.isVisible = false
        binding.listview.isVisible = false
    }

    private fun displayProgressBar() {
        binding.emptyMessageTv.isVisible = false
        binding.emptyHintTv.isVisible = false
        binding.errorMessageTv.isVisible = false
        binding.progressbar.isVisible = true
        binding.listview.isVisible = false
    }

    private fun displayErrorMsg() {
        binding.emptyMessageTv.isVisible = false
        binding.emptyHintTv.isVisible = false
        binding.errorMessageTv.isVisible = true
        binding.progressbar.isVisible = false
        binding.listview.isVisible = false
    }

    private fun setUpDropDownNavigation() {
        val mainActivity = activity as MainActivity?
        if (mainActivity == null) {
            BibleLog.e(TAG, "getActivity() == null")
            return
        }
        // Actionbar
        val actionBar = mainActivity.supportActionBar ?: return
        actionBar.setDisplayShowTitleEnabled(false)
        // Spinner
        val spinner = mainActivity.findViewById<View>(R.id.main_activity_toolbar_spinner) as Spinner
        // Adapter
        val spinnerAdapter = ActionBarColorDropDownNavigationAdapter(actionBar.themedContext)
        // Layout fuer Dropdown-Listen-Auswahl
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = spinnerAdapter
        spinner.visibility = View.VISIBLE
        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    displayListView(mVerses)
                } else {
                    val solidFilterColor = spinnerAdapter.getItem(position)!!
                    displayListView(getFilteredVerses(solidFilterColor))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        // Default selection
        spinner.setSelection(0)
    }

    private fun getFilteredVerses(filterColor: Int): Vector<Verse> {
        val filteredVerses = Vector<Verse>()
        for (currentVerse in mVerses!!) {
            if (currentVerse.highLightColor == filterColor) {
                filteredVerses.add(currentVerse)
            }
        }
        return filteredVerses
    }

    override fun onItemDeleted() {
        // notify the reader of the change
        val readerFragment = targetFragment
        readerFragment?.onActivityResult(targetRequestCode, RESULT_ITEMS_DELETED, null)
    }

    companion object {
        const val TAG = "HighlightedVersesFragment"
        const val RESULT_ITEMS_DELETED = 2001
        private const val KEY_QUERY_HIGHLIGHTED_VERSES_TASK_RUNNING = "KEY_QUERY_HIGHLIGHTED_VERSES_TASK_RUNNING"
        private const val KEY_VERSE_COUNT = "KEY_VERSE_COUNT"
        private const val KEY_VERSE_PREFIX = "KEY_VERSE_"
        private const val KEY_LAST_DB_QUERY_TIME = "KEY_LAST_DB_QUERY_TIME"
    }
}