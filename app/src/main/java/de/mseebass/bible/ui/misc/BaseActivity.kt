package de.mseebass.bible.ui.misc

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.mseebass.bible.application.BibleApplication
import de.mseebass.bible.utils.Settings

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        // set the user-defined theme
        setTheme(Settings.getThemeId(this))
        savedInstanceState?.classLoader = javaClass.classLoader
        super.onCreate(savedInstanceState)
    }

//    fun getAppContext() : BibleApplication {
//        return applicationContext as BibleApplication
//    }

    fun showToast(text: String?) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    fun showToast(textRes: Int) {
        Toast.makeText(this, textRes, Toast.LENGTH_SHORT).show()
    }

    fun showToastLong(text: String?) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    fun showToastLong(textRes: Int) {
        Toast.makeText(this, textRes, Toast.LENGTH_LONG).show()
    }

}