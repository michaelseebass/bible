package de.mseebass.bible.ui.bookmarks

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import de.mseebass.bible.R
import de.mseebass.bible.interfaces.OnLabelSelectedListener
import de.mseebass.bible.models.Label
import de.mseebass.bible.providers.BibleProvider.Bookmarks
import de.mseebass.bible.ui.bookmarks.BookMarksNewGroupDialogFragment.Companion.showDialog
import java.util.*

class BookMarkGroupDialogFragment : DialogFragment() {

    private var mOnLabelSelectedListener: OnLabelSelectedListener? = null
    private var mLabels: Vector<Label>? = null
    private var mFm: FragmentManager? = null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alert = AlertDialog.Builder(activity!!).apply {
            setTitle(R.string.bookmarks_dialog_title)
            setIcon(R.drawable.ic_launcher_bible_small)
            var containsDefault = false
            for (i in mLabels!!.indices) {
                if (mLabels!![i].name == Bookmarks.LABEL_GENERAL) {
                    containsDefault = true
                }
            }
            if (!containsDefault) {
                val generalLabel = Label(Bookmarks.LABEL_GENERAL)
                mLabels!!.add(generalLabel)
            }
            val items = arrayOfNulls<CharSequence>(mLabels!!.size + 1)
            for (i in mLabels!!.indices) {
                items[i] = mLabels!![i].getDisplayName(activity!!)
            }
            items[mLabels!!.size] = getString(R.string.bookmarks_new_label)
            setItems(items) { dialog, index ->
                if (index == mLabels!!.size) {
                    showDialog(mOnLabelSelectedListener, mFm)
                } else {
                    val newLabel = mLabels!![index].name
                    // ReaderFragment aktualisieren
                    mOnLabelSelectedListener?.onLabelSelected(newLabel)
                }
            }
        }

        return alert.create()
    }

    companion object {
        private const val BOOKMARK_GROUP_DIALOG_FRAGMENT_TAG = "bookmark_group_dialog_fragment"
        private var sDialog // TODO avoid memory leak
                : BookMarkGroupDialogFragment? = null

        fun showDialog(onLabelSelectedListener: OnLabelSelectedListener?, existingLabels: Vector<Label>?, fm: FragmentManager?) {
            sDialog = BookMarkGroupDialogFragment().apply {
                mOnLabelSelectedListener = onLabelSelectedListener
                mLabels = existingLabels
                mFm = fm
            }
            sDialog?.show(fm!!, BOOKMARK_GROUP_DIALOG_FRAGMENT_TAG)
        }

        fun dismissDialog() {
            sDialog?.dismiss()
        }
    }
}