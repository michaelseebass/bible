package de.mseebass.bible.ui.bookmarks

/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.graphics.Rect
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewConfiguration
import android.widget.AbsListView
import android.widget.Button
import android.widget.ExpandableListView
import android.widget.ListView
import de.mseebass.bible.utils.BibleLog
import java.util.*

 /**
 * A [View.OnTouchListener] that makes the list items in a [ListView]
 * dismissable. [ListView] is given special treatment because by default it handles touches
 * for its list items... i.e. it's in charge of drawing the pressed state (the list selector),
 * handling list item clicks, etc.
 *
 * After creating the listener, the caller should also call
 * [ListView.setOnScrollListener], passing
 * in the scroll listener returned by [.makeScrollListener]. If a scroll listener is
 * already assigned, the caller should still pass scroll changes through to this listener. This will
 * ensure that this [BookmarkSwipeDismissListViewTouchListener] is paused during list view
 * scrolling.
 *
 * Example usage:
 *
 * <pre>
 * SwipeDismissListViewTouchListener touchListener =
 * new SwipeDismissListViewTouchListener(
 * listView,
 * new SwipeDismissListViewTouchListener.OnDismissCallback() {
 * public void onDismiss(ListView listView, int[] reverseSortedPositions) {
 * for (int position : reverseSortedPositions) {
 * adapter.remove(adapter.getItem(position));
 * }
 * adapter.notifyDataSetChanged();
 * }
 * });
 * listView.setOnTouchListener(touchListener);
 * listView.setOnScrollListener(touchListener.makeScrollListener());
 * </pre>
 *
 * For a generalized [View.OnTouchListener] that makes any view dismissable,
 * see [SwipeDismissTouchListener].
 *
 * @see SwipeDismissTouchListener
 */
class BookmarkSwipeDismissListViewTouchListener(
         private val listView: ExpandableListView,
         private val callbacks: DismissCallbacks,
         private val foreGroundViewResId: Int,
         private val backGroundViewResId: Int,
         private val unDoButtonResId: Int,
         private val mAnimationTime: Long
) : OnTouchListener {

    // Cached ViewConfiguration and system-wide constant values
    private val mSlop: Int
    private val mMinFlingVelocity: Int
    private val mMaxFlingVelocity: Int


    // Fixed properties
    private var mViewWidth = 1 // 1 and not 0 to prevent dividing by zero

    // Transient properties
    private var mDownX = 0f
    private var mSwiping = false
    private var mVelocityTracker: VelocityTracker? = null
    private var mDownPosition = 0
    private var mParentView: View? = null
    private var mForeGroundView: View? = null
    private var mPaused = false


    init {
        // ViewConfiguration
        ViewConfiguration.get(listView.context).let { vc ->
            mSlop = vc.scaledTouchSlop
            mMinFlingVelocity = vc.scaledMinimumFlingVelocity * 16
            mMaxFlingVelocity = vc.scaledMaximumFlingVelocity
        }
    }

    /**
     * Enables or disables (pauses or resumes) watching for swipe-to-dismiss gestures.
     */
    private fun setEnabled(enabled: Boolean) {
        mPaused = !enabled
    }

    /**
     * Returns an [AbsListView.OnScrollListener] to be added to the [ ] using [ListView.setOnScrollListener].
     * If a scroll listener is already assigned, the caller should still pass scroll changes through
     * to this listener. This will ensure that this [BookmarkSwipeDismissListViewTouchListener] is
     * paused during list view scrolling.
     *
     * @see BookmarkSwipeDismissListViewTouchListener
     */
    fun makeScrollListener(): AbsListView.OnScrollListener {
        return object : AbsListView.OnScrollListener {
            override fun onScrollStateChanged(absListView: AbsListView, scrollState: Int) {
                setEnabled(scrollState != AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
            }

            override fun onScroll(absListView: AbsListView, i: Int, i1: Int, i2: Int) {}
        }
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        if (mViewWidth < 2) {
            mViewWidth = listView.width
        }
        when (motionEvent.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                if (mPaused) {
                    return false
                }
                // TODO: ensure this is a finger, and set a flag
                // Find the child view that was touched (perform a hit test)
                mParentView = getListViewChild(motionEvent)
                mParentView?.let {
                    mDownX = motionEvent.rawX
                    mDownPosition = listView.getPositionForView(it)
                    if (callbacks.canDismiss(mDownPosition) && !callbacks.isDismissed(listView, mDownPosition)) {
                        mForeGroundView = it.findViewById(foreGroundViewResId)
                        mVelocityTracker = VelocityTracker.obtain()
                        mVelocityTracker?.addMovement(motionEvent)
                    } else {
                        mParentView = null
                        mForeGroundView = null
                    }
                }
                return false
            }
            MotionEvent.ACTION_UP -> {
                mVelocityTracker?.let {
                    val deltaX = motionEvent.rawX - mDownX
                    it.addMovement(motionEvent)
                    it.computeCurrentVelocity(1000)
                    val velocityX = it.xVelocity
                    val absVelocityX = Math.abs(velocityX)
                    val absVelocityY = Math.abs(it.yVelocity)
                    var dismiss = false
                    var dismissRight = false
                    if (Math.abs(deltaX) > mViewWidth / 2) {
                        dismiss = true
                        dismissRight = deltaX > 0
                    } else if (mMinFlingVelocity <= absVelocityX && absVelocityX <= mMaxFlingVelocity && absVelocityY < absVelocityX) {

                        // dismiss only if flinging in the same direction as dragging
                        dismiss = velocityX < 0 == deltaX < 0
                        dismissRight = velocityX > 0
                    }
                    if (dismiss) {
                        // dismiss
//                    final View parentView = mParentView; // mParentView gets null'd before animation ends
//                    final int downPosition = mDownPosition;
                        animateDismiss(mParentView, dismissRight, mDownPosition)
                    } else if (!callbacks.isDismissed(listView, mDownPosition)) {
                        val backGroundView = mParentView?.findViewById<View>(backGroundViewResId)
                        mForeGroundView?.animateUndo(backGroundView, mAnimationTime)
                    }
                    it.recycle()
                    mVelocityTracker = null
                    mDownX = 0f
                    mParentView = null
                    mForeGroundView = null
                    //                mBackGroundView = null;
                    mDownPosition = ListView.INVALID_POSITION
                    mSwiping = false
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if (mVelocityTracker == null || mPaused) {
                    val dismissCount = callbacks.getDismissPositions(listView).size
                    if (dismissCount > 0) {
                        BibleLog.d(TAG, "MotionEvent.ACTION_MOVE triggers collapseAllItems(); dismiss position count: $dismissCount")
                        // collapse and delete items in dismissed state
                        collapseAllItems()
                    }
                } else {
                    mVelocityTracker!!.addMovement(motionEvent)
                    val deltaX = motionEvent.rawX - mDownX
                    if (Math.abs(deltaX) > mSlop) {
                        mSwiping = true
                        // prevent the ListView from intercepting touch events
                        listView.requestDisallowInterceptTouchEvent(true)

                        // Cancel ListView's touch (un-highlighting the item)
                        val cancelEvent = MotionEvent.obtain(motionEvent)
                        cancelEvent.action = MotionEvent.ACTION_CANCEL or (motionEvent.actionIndex shl MotionEvent.ACTION_POINTER_INDEX_SHIFT)
                        listView.onTouchEvent(cancelEvent)
                        cancelEvent.recycle()
                    }
                    if (mSwiping) {
                        mForeGroundView?.apply {
                            translationX = deltaX
                            alpha = Math.max(0.5f, Math.min(1f, 1f - Math.abs(deltaX) / mViewWidth))
                        }
                        return true
                    }
                }
            }
        }
        return false
    }

    /**
     * Finds the touched child in the ListView
     *
     * @param motionEvent the MotionEvent
     * @return the touched view, or null
     */
    private fun getListViewChild(motionEvent: MotionEvent): View? {
        val rect = Rect()
        val listViewCoords = IntArray(2)
        listView.getLocationOnScreen(listViewCoords)
        val x = motionEvent.rawX.toInt() - listViewCoords[0]
        val y = motionEvent.rawY.toInt() - listViewCoords[1]
        var child: View?
        for (i in 0 until listView.childCount) {
            child = listView.getChildAt(i)
            child.getHitRect(rect)
            if (rect.contains(x, y)) {
                return child
            }
        }
        return null
    }

    /**
     * Performs the dismiss animation after an initial swipe
     *
     * @param parentView   the parent View of the dismissed list item
     * @param dismissRight a boolean indicating whether the item is dismissed to the right side or not
     * @param downPosition an integer representing the position of the dismissed item in the list
     */
    private fun animateDismiss(parentView: View?, dismissRight: Boolean, downPosition: Int) {
        BibleLog.d(TAG, "animateDismiss() - downPosition: $downPosition")
        // find the views
        val foreGroundView = parentView!!.findViewById<View>(foreGroundViewResId)
        val backGroundView = parentView.findViewById<View>(backGroundViewResId)
        val undoButton = backGroundView!!.findViewById<View>(unDoButtonResId) as Button
        // create a listener
        val listener: AnimatorListenerAdapter = object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                callbacks.onDismiss(listView, downPosition)
                // display the undo layout
                backGroundView.visibility = View.VISIBLE
                // set the OnClickListener for the undo button
                undoButton.setOnClickListener {
                    val removed = callbacks.onUndo(listView, downPosition)
                    if (removed) {
                        foreGroundView.animateUndo(backGroundView, mAnimationTime)
                    } else {
                        collapseAllItems()
                    }
                }
            }
        }
        // start the animation
        val translationX = if (dismissRight) mViewWidth else -mViewWidth
        foreGroundView.animate()
                .translationX(translationX.toFloat())
                .alpha(0.5f)
                .setDuration(mAnimationTime)
                .setListener(listener)
    }

    /**
     * Animate the dismissed list item to zero-height and fire the dismiss callback when
     * all dismissed list item animations have completed.
     *
     * @param dismissView the View that will be collapsed
     */
    private fun collapseAllItems() {
        BibleLog.d(TAG, "collapseAllItems()")
        // This triggers layout on each animation frame
        // In the future we may want to do something smarter and more performant.
        val dismissPositions = callbacks.getDismissPositions(listView)
        // Sort by descending position
        Collections.sort(dismissPositions)
        for (i in dismissPositions.indices) {
            val currentDismissPosition = dismissPositions[i]
            val parentView = listView.getChildAt(currentDismissPosition)
            if (parentView == null) {
                BibleLog.d(TAG, "ListView contains " + listView.childCount + " childs")
                throw IllegalStateException("ListView child for position $currentDismissPosition doesn't exist")
            }
            val backGroundView = parentView.findViewById<View>(backGroundViewResId)
            if (backGroundView != null) {
                backGroundView.visibility = View.GONE
            }
            val lp = parentView.layoutParams
            val originalHeight = parentView.height
            val animator = ValueAnimator.ofInt(originalHeight, 1)
            animator.duration = mAnimationTime
            val isLastDismissPosition = i == dismissPositions.size - 1
            animator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    // Reset view presentation of the parent view
                    val lp1 = parentView.layoutParams
                    lp1.height = originalHeight
                    parentView.layoutParams = lp1
                    // Reset view presentation of the foreground view
                    // backGroundView already set to GONE (before start of animation)
                    val foreGroundView = parentView.findViewById<View>(foreGroundViewResId)
                    if (foreGroundView != null) {
                        foreGroundView.alpha = 1f
                        foreGroundView.translationX = 0f
                    }
                    if (isLastDismissPosition) {
                        // the onDismiss()-callback should be triggered only once
                        callbacks.onItemsCollapsed()
                    }
                }
            })
            animator.addUpdateListener { valueAnimator ->
                lp.height = (valueAnimator.animatedValue as Int)
                parentView.layoutParams = lp
            }
            animator.start()
        }
    }

    /**
     * The callback interface used by [BookmarkSwipeDismissListViewTouchListener] to inform its client
     * about a successful dismissal of one or more list item positions.
     */
    interface DismissCallbacks {
        /**
         * Called to determine whether the given position can be dismissed.
         *
         * @param flatPosition The flat list position
         * @return boolean true if the given position can be dismissed, else false
         */
        fun canDismiss(flatPosition: Int): Boolean

        /**
         * Called when the user has indicated that he/she would like to dismiss a list item position.
         *
         * @param expandableListView The originating [ExpandableListView].
         * @param position           The flat list position to dismiss.
         */
        fun onDismiss(expandableListView: ExpandableListView?, flatPosition: Int)

        /**
         * Called when the user has clicked the undo button for a dismissed item
         *
         * @param position the flat list position of the item to be restored
         * @return true if the item was successfully restored, else false
         */
        fun onUndo(expandableListView: ExpandableListView?, position: Int): Boolean

        /**
         * Called when the dismissed items are collapsed and should be removed from the list
         */
        fun onItemsCollapsed()

        /**
         * Called to retrieve the flat list positions of the items in dismissed state
         * (Collapsed groups should be excluded)
         *
         * @param expandableListView The originating [ExpandableListView]
         * @return A Vector containing Integer objects that indicate the flat dismissed list positions
         */
        fun getDismissPositions(expandableListView: ExpandableListView?): Vector<Int>

        /**
         * Called to determine if an item in the list is in dismissed state
         *
         * @param expandableListView The originating [ExpandableListView]
         * @param absolutePosition   The flat list position
         * @return boolean true if the item is in dismissed state, else false
         */
        fun isDismissed(expandableListView: ExpandableListView?, absolutePosition: Int): Boolean
    }

    companion object {
        const val TAG = "BookmarkSwipeDismissListViewTouchListener"
    }

}

/**
 * Animates the given view back to its original horizontal position and alpha
 * onUndo should be called!!!
 */
fun View.animateUndo(backGroundView: View?, animationTime: Long) {
    BibleLog.d(BookmarkSwipeDismissListViewTouchListener.TAG, "animateUndo() called")
    backGroundView?.visibility = View.GONE
    // cancel
    this.animate()
            .translationX(0f)
            .alpha(1f)
            .setDuration(animationTime)
            .setListener(null)
}