package de.mseebass.bible.ui.backup

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import de.mseebass.bible.R
import de.mseebass.bible.databinding.BackupFragmentBinding
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.StoragePermissionManager

/**
 * @author Michael Seebaß on 28.10.2016.
 */
abstract class BackupBaseFragment : BaseFragment() {


    private lateinit var permissionManager: StoragePermissionManager

    private lateinit var binding: BackupFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.backup_fragment, container, false)
        permissionManager = StoragePermissionManager.get(activity)
        onCreateViewInternal(binding, permissionManager)
        return binding.root
    }

    fun setImage(resId: Int) {
        binding.backupFragmentIv.setImageResource(resId)
    }

    fun setMsg(resId: Int) {
        binding.backupFragmentMsgTv.setText(resId)
    }

    fun setStatus(resId: Int) {
        binding.backupFragmentStatusTv.setText(resId)
    }

    fun setCardText(resId: Int) {
        binding.backupFragmentPermissionCardTv.setText(resId)
    }

    abstract fun onCreateViewInternal(binding: BackupFragmentBinding, permissionManager: StoragePermissionManager)

    /*
     * Scoped Directory Access TODO
     * Android N only
https://www.youtube.comwatch?v=XBfQC4owUJg&list=PLWz5rJ2EKKc-lJo_RGGXL2Psr8vVCTWjM&index=7
https://developer.android.com/preview/features/scoped-folder-access.html?utm_campaign=android_series_scopedfolderdoc_062116&utm_source=anddev&utm_medium=yt-desc
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionManager!!.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SYSTEM_SETTINGS_REQUEST_CODE -> if (permissionManager!!.checkStoragePermissionGranted()) {
                BibleLog.d(TAG, "--> Permission already granted")
                displayBtn()
            } else {
                BibleLog.d(TAG, "--> Permission not granted. Displaying explanation")
                displayStatusMsg(null, R.color.color_error)
                displayPermissionExplanation()
            }
            else -> {
            }
        }
    }

    fun displayPermissionExplanation() {
        binding.backupFragmentProgressbar.isVisible = false
        binding.backupFragmentBtn.isVisible = false
        binding.backupFragmentPermissionCard.isVisible = true

        binding.backupFragmentPermissionCardBtn.setOnClickListener {
            if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), StoragePermissionManager.PERMISSION_REQUEST_CODE)
            } else {
                val intent = Intent().apply {
                    action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", activity!!.packageName, null)
                    data = uri
                }
                startActivityForResult(intent, SYSTEM_SETTINGS_REQUEST_CODE)
            }
        }
    }

    fun displayBtn() {
        binding.backupFragmentProgressbar.isVisible = false
        binding.backupFragmentBtn.isVisible = true
        binding.backupFragmentPermissionCard.isVisible = false
    }

    fun displayProgressBar(percent: Int) {
        binding.backupFragmentBtn.isVisible = false
        binding.backupFragmentPermissionCard.isVisible = false
        binding.backupFragmentStatusTv.isVisible = false

        binding.backupFragmentProgressbar.apply {
            isVisible = true
            // progress
            if (percent in 0..100) {
                isIndeterminate = false
                progress = percent
            } else {
                isIndeterminate = true
            }
        }
    }

    /**
     * Displays a status message
     * @param msg The message to be shown. `null` to hide the message TextView
     * @param colorResId Ressource Id of the color to display the text with
     */
    fun displayStatusMsg(msg: String?, colorResId: Int) {
        binding.backupFragmentStatusTv.apply {
            text = msg
            setTextColor(activity!!.resources.getColor(colorResId))
            visibility = if (!TextUtils.isEmpty(msg)) View.VISIBLE else View.GONE
        }
    }

    abstract fun doAction()

    companion object {
        /** Log-TAG  */
        val TAG = BackupExportFragment::class.java.simpleName
        private const val SYSTEM_SETTINGS_REQUEST_CODE = 24
    }
}