package de.mseebass.bible.ui.notes

import android.view.View

interface NotesClickListener {
    fun onNoteHeaderClicked(v: View)
    fun onNoteIconClicked(v: View)
    fun onNoteTextClicked(v: View)
}