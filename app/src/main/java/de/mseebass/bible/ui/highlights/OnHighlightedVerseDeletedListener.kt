package de.mseebass.bible.ui.highlights

/**
 * Callback interface for the to get informed of deleted items.
 *
 * @author Michael Seebaß B.Eng., 2014
 */
interface OnHighlightedVerseDeletedListener {

    /**
     * Called when an item is deleted
     */
    fun onItemDeleted()
}