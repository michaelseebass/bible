package de.mseebass.bible.ui

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.view.ActionMode
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout.SimpleDrawerListener
import androidx.fragment.app.Fragment
import de.mseebass.bible.R
import de.mseebass.bible.databinding.MainActivityBinding
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.models.Verse
import de.mseebass.bible.repository.DelayedRepoHandler
import de.mseebass.bible.repository.ManagedHistoryRepository
import de.mseebass.bible.tasks.AppUpdateTask
import de.mseebass.bible.ui.about.AboutActivity
import de.mseebass.bible.ui.appupdate.AppUpdateFragment
import de.mseebass.bible.ui.backup.BackupActivity
import de.mseebass.bible.ui.backup.BackupImportFragment
import de.mseebass.bible.ui.bookmarks.BookmarksFragment
import de.mseebass.bible.ui.highlights.HighlightedVersesFragment
import de.mseebass.bible.ui.history.HistoryFragment
import de.mseebass.bible.ui.misc.BaseActivity
import de.mseebass.bible.ui.notes.NotesDialogFragment
import de.mseebass.bible.ui.notes.NotesFragment
import de.mseebass.bible.ui.reader.ReaderFragment
import de.mseebass.bible.ui.search.SearchFragment
import de.mseebass.bible.ui.settings.SettingsActivity
import de.mseebass.bible.ui.settings.SettingsFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils.getThemedResourceId
import de.mseebass.bible.utils.Settings
import de.mseebass.bible.utils.findFragment

class MainActivity : BaseActivity() {
    // Layouts and Views
    private lateinit var binding: MainActivityBinding

    private var mSelectedPosition = POSITION_READER
    private var mReaderInvalidated = false

    // Title
    private var mTitle: CharSequence? = null

    // Contextual Action mode
    private var mActionMode: ActionMode? = null
    private var mDelayedHandler: DelayedRepoHandler? = null


    override fun onSaveInstanceState(outState: Bundle) {
        try {
            outState.putInt(KEY_SELECTED_POSITION, mSelectedPosition)
            outState.putBoolean(KEY_READER_INVALIDATED, mReaderInvalidated)
            super.onSaveInstanceState(outState)
        } catch (e: Exception) {
            BibleLog.e(TAG, "Error in onSaveInstanceState: " + e.javaClass.simpleName + ": " + e.message, e)
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)

        setSupportActionBar(binding.toolbar)
        mTitle = title
        mDelayedHandler = DelayedRepoHandler(ManagedHistoryRepository.createImpl(this))
        if (AppUpdateTask.isRunning()) {
            if (savedInstanceState == null) {
                addCopyDatabaseFragment()
            }
            return
        }

        // Intent verarbeiten
        var intentHandled = false
        if (savedInstanceState == null) {
            intentHandled = handleIntent(intent)
        }

        // init the navigation drawer
        setUpNavigationDrawer()
        if (intentHandled) {
            return
        } else if (savedInstanceState == null) {
            BibleLog.v(TAG, "restoring from SharedPreferences")
            selectReaderUsingSharedPreferences()
        } else {
            mReaderInvalidated = savedInstanceState.getBoolean(KEY_READER_INVALIDATED)
            if (mReaderInvalidated) {
                selectReaderUsingSharedPreferences()
                mReaderInvalidated = false
            }
            // restore selected drawer item
            mSelectedPosition = savedInstanceState.getInt(KEY_SELECTED_POSITION)
            //    		mAdapter.setSelectedPosition(selectedPosition);
            title = getFragmentTitle(mSelectedPosition)
        }
        openNavigationDrawerOnFirstUse()
        savedInstanceState?.let {
            dismissDialogs()
        }
    }

    // TODO should continue dialogs!!
    // TODO ListAdapters should not display dialogs! This should be done in UI
    private fun dismissDialogs() {
        findFragment<NotesDialogFragment>(NotesDialogFragment.TAG)?.dismiss()
    }

    public override fun onNewIntent(intent: Intent) {
        BibleLog.v(TAG, "onNewIntent()")
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent): Boolean {
        BibleLog.v(TAG, "handleIntent()")
        if (Intent.ACTION_SEARCH == intent.action) {
            var searchFragment = findSearchFragment()
            if (searchFragment == null) {
                searchFragment = SearchFragment()
                searchFragment.arguments = intent.extras
                selectItem(POSITION_SEARCH, searchFragment, SearchFragment.TAG, true)
            } else {
                // Get the search query
                val query = intent.getStringExtra(SearchManager.QUERY)
                searchFragment.setQuery(query)
                searchFragment.executeSearch()
            }
            return true
        } else if (intent.containsPassageDetails()) { // && !intent.getExtras().isEmpty()
            selectReader(intent.extras, true)
        }
        return false
    }

    private fun Intent.containsPassageDetails(): Boolean {
        if (extras == null || extras!!.isEmpty) {
            return false
        } else if (extras!!.containsKey(ReaderFragment.KEY_BIBLE_ID)
                && extras!!.containsKey(ReaderFragment.KEY_BOOK_INDEX)
                && extras!!.containsKey(ReaderFragment.KEY_CHAPTER_INDEX)
                && extras!!.containsKey(ReaderFragment.KEY_VERSE_INDEX)) {
            return true
        }
        return false
    }

    fun startReaderWithIntent(verse: Verse) {
        startReaderWithIntent(verse.bibleId, verse.bookNumber, verse.chapterNumber, verse.number)
    }

    private fun startReaderWithIntent(bibleId: Long, bookNumber: Int, chapterNumber: Int, verseNumber: Int) {
        val readerIntent = Intent(this, MainActivity::class.java).apply {
            putExtra(ReaderFragment.KEY_BIBLE_ID, bibleId)
            putExtra(ReaderFragment.KEY_BOOK_INDEX, bookNumber - 1)
            putExtra(ReaderFragment.KEY_CHAPTER_INDEX, chapterNumber - 1)
            putExtra(ReaderFragment.KEY_VERSE_INDEX, verseNumber - 1)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        startActivity(readerIntent)
    }

    private fun setUpNavigationDrawer() {
        try {
            binding.mainActivityNavigationview.setNavigationItemSelectedListener { menuItem ->
                val fragment: Fragment
                val tag: String
                val position: Int
                when (menuItem.itemId) {
                    R.id.nav_drawer_menu_item_reader -> {
                        fragment = ReaderFragment()
                        tag = ReaderFragment.FRAGMENT_TRANSACTION_TAG
                        position = POSITION_READER
                        if (mSelectedPosition != position) {
                            selectItem(position, fragment, tag, true)
                        }
                        menuItem.isChecked = true
                    }
                    R.id.nav_drawer_menu_item_highlighted_verses -> {
                        fragment = HighlightedVersesFragment()
                        tag = HighlightedVersesFragment.TAG
                        position = POSITION_HIGHLIGHTED_VERSES
                        if (mSelectedPosition != position) {
                            selectItem(position, fragment, tag, true)
                        }
                        menuItem.isChecked = true
                    }
                    R.id.nav_drawer_menu_item_labels -> {
                        fragment = BookmarksFragment()
                        tag = BookmarksFragment.TAG
                        position = POSITION_BOOKMARKS
                        if (mSelectedPosition != position) {
                            selectItem(position, fragment, tag, true)
                        }
                        menuItem.isChecked = true
                    }
                    R.id.nav_drawer_menu_item_notes -> {
                        fragment = NotesFragment()
                        tag = NotesFragment.TAG
                        position = POSITION_NOTES
                        if (mSelectedPosition != position) {
                            selectItem(position, fragment, tag, true)
                        }
                        menuItem.isChecked = true
                    }
                    R.id.nav_drawer_menu_item_search -> {
                        fragment = SearchFragment()
                        tag = SearchFragment.TAG
                        position = POSITION_SEARCH
                        if (mSelectedPosition != position) {
                            selectItem(position, fragment, tag, true)
                        }
                        menuItem.isChecked = true
                    }
                    R.id.nav_drawer_menu_item_history -> {
                        fragment = HistoryFragment()
                        tag = HistoryFragment.TAG
                        position = POSITION_HISTORY
                        if (mSelectedPosition != position) {
                            selectItem(position, fragment, tag, true)
                        }
                        menuItem.isChecked = true
                    }
                    R.id.nav_drawer_menu_item_settings -> {
                        val settingsIntent = Intent(this@MainActivity, SettingsActivity::class.java)
                        startActivityForResult(settingsIntent, 0)
                    }
                    R.id.nav_drawer_menu_item_backup -> {
                        val backupIntent = Intent(this@MainActivity, BackupActivity::class.java)
                        startActivityForResult(backupIntent, 0)
                    }
                    R.id.nav_drawer_menu_item_about -> {
                        val aboutIntent = Intent(this@MainActivity, AboutActivity::class.java)
                        startActivity(aboutIntent)
                    }
                    else -> throw IllegalArgumentException("Invalid MenuItem ID: " + menuItem.itemId)
                }

                binding.mainActivityDrawerLayout.closeDrawers()

                true
            }
            binding.mainActivityDrawerLayout.setDrawerListener(object : SimpleDrawerListener() {
                override fun onDrawerOpened(drawerView: View) {
                    super.onDrawerOpened(drawerView)
                    // finish Action mode
                    finishActionMode()
                }
            })
            supportActionBar?.apply {
                try {
                    setHomeAsUpIndicator(R.drawable.ic_menu_white)
                    setDisplayHomeAsUpEnabled(true)
                } catch (e: Exception) {
                    BibleLog.e(TAG, "Failed to set Menu Icon on Toolbar", e)
                }
            }
                // obtain the resource id from the theme attribute
                val drawerShadowResId = getThemedResourceId(R.attr.navigationDrawerShadow, this)
                // add shadow to the right edge of the drawer
//            mDrawerLayout.setDrawerShadow(drawerShadowResId, GravityCompat.START); //TODO elevation

        } catch (e: Exception) {
            BibleLog.e(TAG, "Failed to set up NAV Drawer", e)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                binding.mainActivityDrawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Swaps fragments in the main content view
     */
    private fun selectItem(position: Int, fragment: Fragment, tag: String, addToBackStack: Boolean) {
        try {
            BibleLog.v(TAG, "selectItem() called for  position $position")
            if (findViewById<View?>(R.id.main_activity_content_frame_layout) == null) {
                return
            }
            resetToolbar()
            // Insert the fragment by replacing any existing fragment
            val fragmentManager = supportFragmentManager
            val ft = fragmentManager.beginTransaction()
            ft.replace(R.id.main_activity_content_frame_layout, fragment, tag)
            if (addToBackStack) {
                ft.addToBackStack(null) // TODO tag or null?
            } else {
                fragmentManager.popBackStack()
            }
            ft.commit()
            // Update the title and close the drawer
//        setTitle(getFragmentTitle(position));
            binding.mainActivityDrawerLayout.closeDrawers()
            finishActionMode()
        } catch (e: Exception) {
            BibleLog.e(TAG, "Error selecting item - pos =  " + position + "fragment = " + fragment +
                    " tag = " + tag + " addToBackStack = " + addToBackStack, e)
        }
    }

    /**
     * Selects the Reader view from the NavigationDrawer and performs the respective fragment transaction.
     * @param args Bundle containing the location information for the verse to be displayed. May be `null`.
     * @param addToBackStack `true`, to add the fragment transaction to the BackStack
     */
    private fun selectReader(args: Bundle?, addToBackStack: Boolean) {
        val readerFragment = ReaderFragment()
        readerFragment.arguments = args
        selectItem(POSITION_READER, readerFragment, ReaderFragment.FRAGMENT_TRANSACTION_TAG, addToBackStack)
    }

    /**
     * Selects the Reader view using the verse location information
     * previously stored in SharedPreferences (e.g. the last known location)
     */
    private fun selectReaderUsingSharedPreferences() {
        // gesicherte Einstellungen einlesen, falls nicht vorhanden Defaults wiederherstellen
        val args = Bundle()
        args.putLong(ReaderFragment.KEY_BIBLE_ID, Settings.getBibleId(this))
        args.putInt(ReaderFragment.KEY_BOOK_INDEX, Settings.getBookIndex(this))
        args.putInt(ReaderFragment.KEY_CHAPTER_INDEX, Settings.getChapterIndex(this))
        args.putInt(ReaderFragment.KEY_VERSE_INDEX, Settings.getVerseIndex(this))
        selectReader(args, false)
    }

    private fun getFragmentTitle(position: Int): String {
        return when (position) {
            POSITION_READER -> getString(R.string.app_name)
            POSITION_HIGHLIGHTED_VERSES -> getString(R.string.highlighted_verses_actionbar_title)
            POSITION_BOOKMARKS -> getString(R.string.bookmarks_actionbar_title)
            POSITION_NOTES -> getString(R.string.notes_title)
            POSITION_SEARCH -> getString(R.string.search_actionbar_title)
            POSITION_HISTORY -> getString(R.string.history_title)
            else -> throw IllegalArgumentException("Invalid positon supplied to getTitle(): $position")
        }
    }

    override fun setTitle(title: CharSequence) {
        mTitle = title
        super.setTitle(mTitle)
        supportActionBar?.title = mTitle
    }

    private fun addCopyDatabaseFragment() {
        if (findViewById<View?>(R.id.main_activity_content_frame_layout) == null) return
        // Fragment erstellen
        val copyDatabaseFragment = AppUpdateFragment()
        // Fragment zu Container hinzuf�gen
        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.main_activity_content_frame_layout, copyDatabaseFragment, AppUpdateFragment.TAG)
        ft.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            SettingsFragment.RESULT_THEME_CHANGED -> {
                finish()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            BackupImportFragment.RESULT_IMPORTED, SettingsFragment.RESULT_FONT_SIZE_CHANGED -> mReaderInvalidated = true
            else -> { }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPostResume() {
        if (mReaderInvalidated) {
            selectReaderUsingSharedPreferences()
            mReaderInvalidated = false
        }
        super.onPostResume()
    }

    override fun startSupportActionMode(callback: ActionMode.Callback): ActionMode? {
        // if an ActionMode is already active (e.g. from another fragment)
        if (mActionMode != null) {
            mActionMode!!.finish()
        }
        mActionMode = super.startSupportActionMode(callback)
        return mActionMode
    }

    fun clearActionMode() {
        mActionMode = null
    }

    fun finishActionMode() {
        mActionMode?.finish()
        mActionMode = null
    }

    private fun findSearchFragment(): SearchFragment? {
        val fm = supportFragmentManager
        return fm.findFragmentByTag(SearchFragment.TAG) as SearchFragment?
    }

    private fun findNotesDialogFragment(): NotesDialogFragment? {
        val fm = supportFragmentManager
        return fm.findFragmentByTag(NotesDialogFragment.TAG) as NotesDialogFragment?
    }

    /**
     * Sets the NavigationDrawer Adapters selected position.
     * Called during onResume() of the individual Fragments.
     *
     * @param position the position of the selected item
     */
    fun onNavigationDrawerItemSelected(position: Int) {
        mSelectedPosition = position
        when (position) {
            POSITION_READER -> binding.mainActivityNavigationview.setCheckedItem(R.id.nav_drawer_menu_item_reader)
            POSITION_HIGHLIGHTED_VERSES -> binding.mainActivityNavigationview.setCheckedItem(R.id.nav_drawer_menu_item_highlighted_verses)
            POSITION_BOOKMARKS -> binding.mainActivityNavigationview.setCheckedItem(R.id.nav_drawer_menu_item_labels)
            POSITION_NOTES -> binding.mainActivityNavigationview.setCheckedItem(R.id.nav_drawer_menu_item_notes)
            POSITION_SEARCH -> binding.mainActivityNavigationview.setCheckedItem(R.id.nav_drawer_menu_item_search)
            POSITION_HISTORY -> binding.mainActivityNavigationview.setCheckedItem(R.id.nav_drawer_menu_item_history)
            else -> {
            }
        }
        title = getFragmentTitle(position)
    }

    /**
     * Should be called after selectItem()
     */
    private fun openNavigationDrawerOnFirstUse() {
        if (!Settings.isNavigationDrawerAcknowledged(this)) {
            binding.mainActivityDrawerLayout.openDrawer(GravityCompat.START)
            Settings.saveNavigationDrawerAcknowledged(this)
        }
    }

    fun onCopyDatabaseTaskFinished() {
        setUpNavigationDrawer()
        selectReaderUsingSharedPreferences()
        openNavigationDrawerOnFirstUse()
    }

    fun resetToolbar() {
        try {
            // Toolbar
            supportActionBar?.setDisplayShowTitleEnabled(true)
            setTitle(R.string.app_name)
            hideToolbarSpinner()
        } catch (e: Exception) {
            BibleLog.e(TAG, "Error resetting toolbar: " + e.javaClass.simpleName + ": " + e.message, e)
        }
    }

    fun hideToolbarSpinner() {
        // Spinner
        binding.mainActivityToolbarSpinner.apply {
            adapter = null
            onItemSelectedListener = null
            isVisible = false
        }
    }

    fun saveHistory(bibleId: Long, verse: Verse) {
        // create history entry
        try {
            val entry = HistoryEntry(bibleId, verse.id, System.currentTimeMillis(), 0)
            entry.verse = verse
            mDelayedHandler?.saveDelayed(entry)
        } catch (e: Exception) {
            BibleLog.e(TAG, "Failed to create history entry!", e)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mDelayedHandler = null
    }

    override fun onBackPressed() {
        if (binding.mainActivityDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.mainActivityDrawerLayout.closeDrawers()
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        const val TAG = "MainActivity"
        const val POSITION_READER = 0
        const val POSITION_HIGHLIGHTED_VERSES = 1
        const val POSITION_BOOKMARKS = 2
        const val POSITION_NOTES = 3
        const val POSITION_SEARCH = 4
        const val POSITION_HISTORY = 5
        private const val KEY_SELECTED_POSITION = "KEY_SELECTED_POSITION"
        private const val KEY_READER_INVALIDATED = "KEY_READER_INVALIDATED"
    }
}