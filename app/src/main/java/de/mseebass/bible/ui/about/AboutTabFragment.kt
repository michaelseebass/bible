package de.mseebass.bible.ui.about

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.mseebass.bible.R
import de.mseebass.bible.ui.misc.BaseFragment

class AboutTabFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val args = arguments
        return when (args!!.getInt(KEY_TYPE)) {
            TYPE_LEGAL -> {
                val contentView = inflater.inflate(R.layout.about_page_fragment_legal, null)
                // Email
                val emailTv = contentView.findViewById(R.id.about_page_fragment_legal_contact_email_tv) as TextView
                emailTv.setOnClickListener {
                    // Feedback
                    feedbackIntent()
                }
                contentView
            }
            TYPE_BIBLES -> inflater.inflate(R.layout.about_page_fragment_bibles, null)
            TYPE_LICENSES -> inflater.inflate(R.layout.about_page_fragment_licenses, null)
            else -> null
        }
    }

    private fun feedbackIntent() {
        val feedback = Intent(Intent.ACTION_SEND).apply {
            type = "plain/text"
            putExtra(Intent.EXTRA_EMAIL, arrayOf(getString(R.string.about_page_fragment_legal_feedback_mail_address)))
            // Betreff
            putExtra(Intent.EXTRA_SUBJECT, getString(R.string.about_page_fragment_legal_feedback_subject))
        }
        startActivity(Intent.createChooser(feedback, getString(R.string.about_page_fragment_legal_feedback_app_chooser_title)))
    }

    companion object {
        const val KEY_TYPE = "KEY_TYPE"
        const val TYPE_LEGAL = 101
        const val TYPE_BIBLES = 102
        const val TYPE_LICENSES = 103
    }
}