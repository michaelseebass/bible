package de.mseebass.bible.ui.about

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import de.mseebass.bible.R
import de.mseebass.bible.databinding.AboutActivityBinding
import de.mseebass.bible.ui.misc.BaseActivity
import de.mseebass.bible.utils.ResourceUtils.setStatusBarColorSolid
import de.mseebass.bible.utils.Settings

class AboutActivity : BaseActivity() {

    private lateinit var binding: AboutActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.about_activity)

        setSupportActionBar(binding.toolbar)
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        try {
            val packageInfo = packageManager.getPackageInfo("de.mseebass.bible", 0)
            val title = getString(R.string.app_name) + " " + packageInfo.versionName
            actionBar?.subtitle = title
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        val adapter = AboutPagerAdapter(supportFragmentManager, this)
        binding.viewpager.let {
            it.adapter = adapter
            binding.tabLayout.setupWithViewPager(it)
        }

        //set status bar color
        setStatusBarColorSolid(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //			NavUtils.navigateUpFromSameTask(this);
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}