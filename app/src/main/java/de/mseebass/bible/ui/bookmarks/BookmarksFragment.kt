package de.mseebass.bible.ui.bookmarks

import android.os.Bundle
import android.os.Parcelable
import android.view.*
import android.widget.ExpandableListView
import android.widget.ExpandableListView.OnGroupCollapseListener
import android.widget.ExpandableListView.OnGroupExpandListener
import android.widget.RelativeLayout
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import de.mseebass.bible.R
import de.mseebass.bible.databinding.BookmarksFragmentBinding
import de.mseebass.bible.interfaces.OnLabelCountChangedListener
import de.mseebass.bible.models.Label
import de.mseebass.bible.tasks.QueryBookmarksTask
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.ui.reader.ReaderFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils.getPixel
import de.mseebass.bible.utils.Settings
import java.util.*

class BookmarksFragment : BaseFragment(),
        OnGroupCollapseListener,
        OnGroupExpandListener,
        OnLabelCountChangedListener,
        QueryBookmarksTask.Listener
{
    // Content-Layout
    private lateinit var binding: BookmarksFragmentBinding

    // Labels
    private var mLabels: Vector<Label>? = null
    private var mLastDbQueryTime: Long = -1

    // Adapter
    private var mAdapter: BookmarksExpandableListAdapter? = null

    // Menu items
    private var mCollapseItem: MenuItem? = null
    private var mExpandItem: MenuItem? = null


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (mLabels != null) {
            // Bibel-Vector
            outState.putInt(KEY_LABEL_COUNT, mLabels!!.size)
            for (i in mLabels!!.indices) {
                outState.putParcelable(KEY_LABEL_PREFIX + i, mLabels!![i])
            }
            outState.putLong(KEY_LAST_DB_QUERY_TIME, mLastDbQueryTime)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        // load data
        savedInstanceState?.let {
            if (it.containsKey(KEY_LABEL_COUNT)) {
                mLabels = Vector()
                // Vector
                val labelCount = it.getInt(KEY_LABEL_COUNT)
                for (i in 0 until labelCount) {
                    mLabels!!.add(it.getParcelable<Parcelable>(KEY_LABEL_PREFIX + i) as Label?)
                }
                mLastDbQueryTime = it.getLong(KEY_LAST_DB_QUERY_TIME)
            }
        }
        val readerFragment = parentFragmentManager.findFragmentByTag(ReaderFragment.FRAGMENT_TRANSACTION_TAG)
        readerFragment?.let {
            setTargetFragment(it, 0) //TODO
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.bookmarks_fragment, container, false)
        if (!Settings.areQueriedBookmarksValid(activity, mLastDbQueryTime)) {
            // invalidate previously queried labels
            mLabels = null
            mAdapter = null
        }
        // if labels have not been queried (or invalidated)
        if (mLabels == null) {
            // Task
            QueryBookmarksTask.getInstance(activity).apply {
                setListener(this@BookmarksFragment)
                if (!QueryBookmarksTask.isRunning()) {
                    execute()
                }
            }
        }
        // UI
        updateUi(false)
        return binding.root
    }

    override fun onResume() {
        BibleLog.d(TAG, "onResume()")
        // notify the navigation drawer
        val mainActivity = activity as MainActivity?
        mainActivity!!.onNavigationDrawerItemSelected(MainActivity.POSITION_BOOKMARKS)
        // ActionBar
        val actionBar = (activity as AppCompatActivity?)!!.supportActionBar
        actionBar!!.setDisplayShowTitleEnabled(true)
        mainActivity.hideToolbarSpinner()
        super.onResume()
    }

    override fun onDestroy() {
        // unregister this fragment as task listener (in order to prevent memory leaks)
        QueryBookmarksTask.getExistingInstance(activity)?.setListener(null)
        super.onDestroy()
    }

    override fun onQueryBookmarksTaskStarted() {
        displayProgressBar()
    }

    override fun onQueryBookmarksTaskFinished(results: Vector<Label>) {
        mLabels = results
        mLastDbQueryTime = System.currentTimeMillis()
        updateUi(true)
    }

    /**
     * Updates the UI to display the appropriate elements.
     */
    private fun updateUi(forceTaskFisnished: Boolean) {
        if (!forceTaskFisnished && QueryBookmarksTask.isRunning()) {
            displayProgressBar()
        } else if (mLabels == null) {
            displayErrorMsg()
        } else if (mLabels!!.size == 0) {
            displayEmptyStateMsg()
        } else {
            displayListView()
        }
    }

    /**
     * Sets up the ListView with OnClickListeners and Swipe to dismiss
     */
    private fun displayListView() {
        // ProgressBar
        binding.progressbar.isVisible = false
        // Error message
        binding.errorMessageTv.isVisible = false
        // Message and hint
        binding.emptyMessageTv.isVisible = false
        binding.emptyHintTv.isVisible = false
        // safety check
        val mainActivity = activity as MainActivity? ?: return
        // ListView
        binding.expandableListview.apply {
            isVisible = true
            // save expanded/collapsed state on configuration change
            isSaveEnabled = true
            //the system default short animation time
            val animationTime = mainActivity.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
            // Adapter
            mAdapter = BookmarksExpandableListAdapter(mainActivity, mLabels!!, animationTime, this@BookmarksFragment)
            setAdapter(mAdapter)
            // the adapter handles the click events
            setOnChildClickListener(mAdapter)
            setOnGroupClickListener(mAdapter)
            onItemLongClickListener = mAdapter
            // swipe to dismiss
            val foregroundViewId = R.id.bookmark_undo_list_item_foreground
            val backgroundViewId = R.id.includable_undo_list_item_background_container
            val buttonId = R.id.includable_undo_list_item_background_button
            val touchListener = BookmarkSwipeDismissListViewTouchListener(this, mAdapter!!, foregroundViewId, backgroundViewId, buttonId, animationTime)
            setOnTouchListener(touchListener)
            setOnScrollListener(touchListener.makeScrollListener())
            // Gruppe ausklappen, falls nur eine existiert
            if (mLabels!!.size == 1) {
                post { expandGroup(0) }
            }
            setOnGroupCollapseListener(this@BookmarksFragment)
            setOnGroupExpandListener(this@BookmarksFragment)
            val pixelLeft = getPixel(0, mainActivity)
            val pixelRight = getPixel(56, mainActivity)
            setIndicatorBoundsRelative(pixelLeft, pixelRight)
        }
        // invalidate the expand/collapse menu items
        mainActivity.invalidateOptionsMenu()
    }

    private fun displayEmptyStateMsg() {
        binding.expandableListview.isVisible = false
        binding.progressbar.isVisible = false
        binding.errorMessageTv.isVisible = false
        binding.emptyMessageTv.isVisible = true
        binding.emptyHintTv.isVisible = true
    }

    private fun displayProgressBar() {
        binding.expandableListview.isVisible = false
        binding.progressbar.isVisible = true
        binding.errorMessageTv.isVisible = false
        binding.emptyMessageTv.isVisible = false
        binding.emptyHintTv.isVisible = false
    }

    private fun displayErrorMsg() {
        binding.expandableListview.isVisible = false
        binding.progressbar.isVisible = false
        binding.errorMessageTv.isVisible = true
        binding.emptyMessageTv.isVisible = false
        binding.emptyHintTv.isVisible = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_bookmars, menu)
        mCollapseItem = menu.findItem(R.id.bookmarks_menu_item_collapse)
        mExpandItem = menu.findItem(R.id.bookmarks_menu_item_expand)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        mExpandItem?.isVisible = !areAllGroupsExpanded()
        mCollapseItem?.isVisible = !areAllGroupsCollapsed()
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.bookmarks_menu_item_collapse -> {
                var i = 0
                while (i < mAdapter!!.groupCount) {
                    binding.expandableListview.collapseGroup(i)
                    i++
                }
                return true
            }
            R.id.bookmarks_menu_item_expand -> {
                var i = 0
                while (i < mAdapter!!.groupCount) {
                    binding.expandableListview.expandGroup(i, true)
                    i++
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun areAllGroupsExpanded(): Boolean {
        mAdapter?.let {
            for (i in 0 until it.groupCount) {
                if (!binding.expandableListview.isGroupExpanded(i)) {
                    // this item is collapsed
                    return false
                }
            }
        }
        return true
    }

    private fun areAllGroupsCollapsed(): Boolean {
        mAdapter?.let {
            for (i in 0 until it.groupCount) {
                if (binding.expandableListview.isGroupExpanded(i)) {
                    // this item is expanded
                    return false
                }
            }
        }
        return true
    }

    override fun onGroupExpand(groupPosition: Int) {
        activity?.invalidateOptionsMenu()
    }

    override fun onGroupCollapse(groupPosition: Int) {
        activity?.invalidateOptionsMenu()
    }

    override fun onLabelCountChanged(groupCount: Int) {
        activity?.invalidateOptionsMenu()
    }

    override fun onItemDeleted() {
        // notify the reader of the change
        val readerFragment = targetFragment
        readerFragment?.onActivityResult(targetRequestCode, RESULT_ITEMS_DELETED, null)
    }

    companion object {
        const val TAG = "BookmarksFragment"
        const val RESULT_ITEMS_DELETED = 2000
        private const val KEY_LABEL_COUNT = "KEY_LABEL_COUNT"
        private const val KEY_LABEL_PREFIX = "KEY_LABEL_"
        private const val KEY_LAST_DB_QUERY_TIME = "KEY_LAST_DB_QUERY_TIME"
    }
}