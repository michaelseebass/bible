package de.mseebass.bible.ui.about

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import de.mseebass.bible.R

class AboutPagerAdapter(
        fm: FragmentManager,
        private val mContext: Context
) : FragmentPagerAdapter(fm) {

    override fun getItem(pos: Int): Fragment {
        val fragment = AboutTabFragment()
        val args = Bundle()
        when (pos) {
            0 -> args.putInt(AboutTabFragment.KEY_TYPE, AboutTabFragment.TYPE_LEGAL)
            1 -> args.putInt(AboutTabFragment.KEY_TYPE, AboutTabFragment.TYPE_BIBLES)
            2 -> args.putInt(AboutTabFragment.KEY_TYPE, AboutTabFragment.TYPE_LICENSES)
        }
        fragment.arguments = args
        return fragment
    }

    override fun getItemPosition(`object`: Any) = PagerAdapter.POSITION_NONE


    override fun getCount() = 3

    override fun getPageTitle(pos: Int): CharSequence? {
        return when (pos) {
            0 -> mContext.getString(R.string.about_activity_tab_title_legal)
            1 -> mContext.getString(R.string.about_activity_tab_title_bibles)
            2 -> mContext.getString(R.string.about_activity_tab_title_licenses)
            else -> null
        }
    }

    companion object {
        const val TAG = "AboutPagerAdapter"
    }

}