package de.mseebass.bible.ui.backup

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import de.mseebass.bible.R
import de.mseebass.bible.ui.misc.BaseActivity
import de.mseebass.bible.utils.ResourceUtils
import de.mseebass.bible.utils.Settings

class BackupActivity : BaseActivity() {

    companion object {

        private const val TAG = "BackupActivity"

        // Unique request code.
        // Those are not in the individual Fragment classes to make sure the numbers always differ.
        const val SHARE_REQUEST_CODE = 17
        const val CHOOSE_REQUEST_CODE = 18
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.backup_activity)

        val toolbar = findViewById<Toolbar>(R.id.backup_activity_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        ResourceUtils.setStatusBarColorSolid(this)

        val viewPager = findViewById<ViewPager>(R.id.backup_activity_viewpager)
        val adapter = BackupPagerAdapter(supportFragmentManager, this)
        viewPager.adapter = adapter
        val tabLayout = findViewById<TabLayout>(R.id.backup_activity_tablayout)
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //			NavUtils.navigateUpFromSameTask(this);
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}