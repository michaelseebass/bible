package de.mseebass.bible.ui.reader

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import de.mseebass.bible.R
import de.mseebass.bible.databinding.ReaderPageFragmentBinding
import de.mseebass.bible.models.Chapter
import de.mseebass.bible.tasks.QueryChapterTask
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils.getPixel
import de.mseebass.bible.utils.Settings

class ReaderPageFragment : BaseFragment(), QueryChapterTask.Listener {
    // Variablen
    private var mBibleId: Long = 0
    private var mBibleShortName: String? = null
    private var mBookNumber = 0
    private var mChapterNumber = 0
    private var mSelectedVerseIndex = -1
    var chapter: Chapter? = null
        private set

    //private Vector<Verse> mBookmarkedVerses;
    private var mLastDbQueryTime: Long = -1

    // UI
    private lateinit var dataBinding: ReaderPageFragmentBinding
    private var mTaskRunning = false
//    private var mListView: ListView? = null

    /**
     * initialisiert das Fragment
     *
     * @param bibleId   ID der Bibel�bersetzung
     * @param bookNr    Nummer des Buches f�r dieses Kapitel
     * @param chapterNr Nummer das Kapitels
     */
    fun init(bibleId: Long, bibleShortName: String?, bookNr: Int, chapterNr: Int) {
        mBibleId = bibleId
        mBookNumber = bookNr
        mChapterNumber = chapterNr
        mBibleShortName = bibleShortName
    }

    /**
     * Setzt den vom Nutzer gew�hlten Verse. Das Fragment wird zu diesem Verse scrollen.
     *
     * @param verseIndex Index des Verses im Kapitel
     */
    fun setSelectedVerse(verseIndex: Int) {
        BibleLog.e(TAG, "setting index for chapter-number $mChapterNumber : $verseIndex")
        mSelectedVerseIndex = verseIndex
    }

    /*
     * called only when restoring fragments after swiping back and forth
     * not called on orientation change, since setRetainInstance() is set to true
     */
    override fun onSaveInstanceState(outState: Bundle) {
        BibleLog.d(TAG, "onSaveInstanceState()")
        /*
		 * When a task is running, the result will not be displayed anyway
		 */
//		outState.putBoolean("THREAD_RUNNING", mThreadRunning);
        outState.putLong(ReaderFragment.KEY_BIBLE_ID, mBibleId)
        outState.putInt("BOOK_NUMBER", mBookNumber)
        outState.putInt("CHAPTER_NUMBER", mChapterNumber)
        /*
		 * only needed when first displaying the chapter
		 */
//		outState.putInt("SELECTED_VERSE", mSelectedVerseIndex);
        outState.putParcelable("CHAPTER", chapter)
        outState.putString("BIBLE_SHORT_NAME", mBibleShortName)
        outState.putLong(KEY_LAST_DB_QUERY_TIME, mLastDbQueryTime)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // restore data
        savedInstanceState?.let {
            BibleLog.d(TAG, "restoring ReaderPageFragment")
            /*
			 * if a task was running, the result could most likely not be displayed
			 */
//			this.mThreadRunning = savedInstanceState.getBoolean("THREAD_RUNNING");
            mBibleId = it.getLong(ReaderFragment.KEY_BIBLE_ID)
            mBookNumber = it.getInt("BOOK_NUMBER")
            mChapterNumber = it.getInt("CHAPTER_NUMBER")
            /*
			 * only needed when first displaying the chapter
			 */
//			this.mSelectedVerseIndex = savedInstanceState.getInt("SELECTED_VERSE");
            chapter = it.getParcelable("CHAPTER")
            mBibleShortName = it.getString("BIBLE_SHORT_NAME")
            mLastDbQueryTime = it.getLong(KEY_LAST_DB_QUERY_TIME)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        BibleLog.d(TAG, "onCreateView() - Chapter $mChapterNumber $mBibleShortName")

        dataBinding = DataBindingUtil.inflate(inflater, R.layout.reader_page_fragment, container, false)

        // check if the database has been updated (after restoring)
        if (!Settings.areQueriedBookmarksValid(activity, mLastDbQueryTime)) {
            // Database edited - invalidate previously queried labels/highlights
            chapter = null
        }
        if (chapter == null) {
            QueryChapterTask.getInstance(activity, mBibleId, mBookNumber, mChapterNumber).apply {
                setListener(this@ReaderPageFragment)
                if (!isRunning) {
                    execute()
                }
                mTaskRunning = true
            }
        }
        updateUi()
        // Rückgabewert
        return dataBinding.root
    }

    override fun onDestroy() {
        BibleLog.d(TAG, "onDestroy() called on chapter-number $mChapterNumber $mBibleShortName")
        // unregister this fragment as task listener (in order to prevent memory leaks)
        QueryChapterTask.getExistingInstance(mBibleId, mBookNumber, mChapterNumber)?.setListener(null)
        super.onDestroy()
    }

    override fun onQueryChapterTaskStarted() {
        // nothing
    }

    override fun onQueryChapterTaskFinished(chapter: Chapter?) {
        BibleLog.d(TAG, "onQueryChapterTaskFinished() $mChapterNumber $mBibleShortName")
        mTaskRunning = false
        this.chapter = chapter
        mLastDbQueryTime = System.currentTimeMillis()
        updateUi()
    }

    private fun updateUi() {
        if (mTaskRunning) {
            displayProgressBar()
        } else if (chapter == null || chapter?.size() == 0) {
            displayErrorMessage(R.string.reader_page_fragment_error_loading_chapter)
        } else {
            displayChapter()
        }
    }

    private fun displayChapter() {
        BibleLog.d(TAG, "displayChapter() called on chapter-number $mChapterNumber $mBibleShortName")
        if (activity == null) {
            BibleLog.w(TAG, "getActivity() == null on Book $mBookNumber Chapter $mChapterNumber")
            // leave this method
            return
        }
        dataBinding.readerPageProgressbar.visibility = View.GONE
        dataBinding.readerPageErrorMessageTv.isVisible = false
        dataBinding.readerPageFragmentListview.isVisible = true
        val adapter = ReaderListAdapter(activity as MainActivity, chapter!!.verses, mBibleShortName!!)
        dataBinding.readerPageFragmentListview.adapter = adapter

        notifyParent()
        if (mSelectedVerseIndex != -1) {
            dataBinding.root.post {
                // scroll to the selected verse
                dataBinding.readerPageFragmentListview.setSelection(mSelectedVerseIndex)
            }
        }
    }

    fun notifyParent() {
        if (chapter == null) return
        try {
            val readerFragment = parentFragment as ReaderFragment?
            readerFragment!!.notifyChapterLoaded(chapter!!)
        } catch (e: Exception) {
            BibleLog.e(TAG, "Error notifying parent", e)
        }
    }

    private fun displayProgressBar() {
        dataBinding.readerPageFragmentListview.isVisible = false
        dataBinding.readerPageErrorMessageTv.isVisible = false
        dataBinding.readerPageProgressbar.isVisible = true
    }

    private fun displayErrorMessage(stringResId: Int) {
        dataBinding.readerPageFragmentListview.isVisible = false
        dataBinding.readerPageProgressbar.isVisible = false
        dataBinding.readerPageErrorMessageTv.let {
            it.isVisible = true
            it.setText(stringResId)
        }
    }

    val firstVisibleVerseIndex: Int
        get() {
            return dataBinding.readerPageFragmentListview.let {
                val view = it.getChildAt(0)
                if (view != null) {
                    val viewTop = view.top
                    val viewHeight = view.height
                    val verticalTvPadding = activity!!.resources.getDimension(R.dimen.verse_tv_vertical_padding)
                    val allowableSpacing = getPixel(16, activity!!)
                    if (viewHeight + viewTop - 2 * verticalTvPadding <= allowableSpacing) {
                        return it.firstVisiblePosition + 1
                    }
                }
                it.firstVisiblePosition
            }
        }

    companion object {
        // Log-Tag
        const val TAG = "-> ReaderPageFragment"

        private const val KEY_LAST_DB_QUERY_TIME = "KEY_LAST_DB_QUERY_TIME"
    }
}