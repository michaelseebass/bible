package de.mseebass.bible.ui.bookmarks

import android.content.Context
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemLongClickListener
import android.widget.ExpandableListView.OnChildClickListener
import android.widget.ExpandableListView.OnGroupClickListener
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.ShareActionProvider
import androidx.core.view.MenuItemCompat
import de.mseebass.bible.R
import de.mseebass.bible.interfaces.OnLabelCountChangedListener
import de.mseebass.bible.models.Label
import de.mseebass.bible.models.Verse
import de.mseebass.bible.models.Verse.DuplicateLabelException
import de.mseebass.bible.models.Verse.IllegalLabelFormatException
import de.mseebass.bible.providers.BibleProviderUtils
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.bookmarks.BookmarkSwipeDismissListViewTouchListener
import de.mseebass.bible.ui.bookmarks.BookmarkSwipeDismissListViewTouchListener.DismissCallbacks
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils.setStatusBarColorActionMode
import de.mseebass.bible.utils.ResourceUtils.setStatusBarColorDefault
import de.mseebass.bible.utils.Settings
import java.util.*

class BookmarksExpandableListAdapter internal constructor(// Context
        private val mMainActivity: MainActivity,
        private val mLabels: Vector<Label>,
        private val mAnimationTime: Long,
        private val mDeleteBookmarksCallback: OnLabelCountChangedListener?
) : BaseExpandableListAdapter(),
        ActionMode.Callback,
        OnChildClickListener,
        OnGroupClickListener,
        OnItemLongClickListener,
        DismissCallbacks
{

    // ActionMode
    private var mActionMode: ActionMode? = null
    private var mShareActionProvider: ShareActionProvider? = null

    // ActionMode TextView
    private var mActionModeView: View? = null

    // ActionMode Verse
    private var mActionModeVerseIndex = -1

    // ActionMode Label
    private var mActionModeLabelIndex = -1



    override fun getChild(groupPosition: Int, childPosition: Int): Verse {
        return mLabels[groupPosition].bookMarks[childPosition]
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        // Layout
        var retView = convertView
        if (retView == null) {
            val inflater = mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            retView = inflater.inflate(R.layout.bookmark_child_undo_list_item, null)
        }
        retView?.let {
            // Foreground Layout
            val foreGroundContainer = it.findViewById<View>(R.id.bookmark_undo_list_item_foreground)
            // TextViews
            val locatorTv = it.findViewById<TextView>(R.id.includable_bookmark_item_locator_tv)
            val bibleTv = it.findViewById<TextView>(R.id.includable_bookmark_item_bible_tv)
            val textTv = it.findViewById<TextView>(R.id.includable_bookmark_item_text_tv)
            // Verse
            val verse = getChild(groupPosition, childPosition)
            // Text setzen
            locatorTv.text = verse.getLocator()
            bibleTv.text = verse.bibleShortName
            textTv.text = verse.text

            // background
            val backGroundView = it.findViewById<View>(R.id.includable_undo_list_item_background_container)
            // determine if this item has been dismissed
            if (verse.containsLabel(getGroup(groupPosition))) {
                foreGroundContainer.x = 0f
                foreGroundContainer.alpha = 1f
                backGroundView.visibility = View.GONE
            } else {
                foreGroundContainer.x = parent!!.width.toFloat()
                foreGroundContainer.alpha = 0.5f
                backGroundView.visibility = View.VISIBLE
                // set the OnClickListener for the undo button
                val undoButton = it.findViewById<View>(R.id.includable_undo_list_item_background_button) as Button
                undoButton.setOnClickListener {
                    val removed = onUndoChild(groupPosition, childPosition)
                    if (removed) {
                        foreGroundContainer?.animateUndo(backGroundView, mAnimationTime)
                    } else {
                        removeDismissedItemsAndNotify()
                    }
                }
            }
        }

        return retView!!
    }

    override fun onChildClick(parent: ExpandableListView, v: View?, groupPosition: Int, childPosition: Int, id: Long): Boolean {
        if (isChildDismissed(groupPosition, childPosition)) {
            // if in action mode, don't do any collapse/expand action (returns true if the click was handled)
            return false
        }
        if (mActionMode != null) {
            if (mActionModeLabelIndex == groupPosition && mActionModeVerseIndex == childPosition) {
                // short click on selected action mode child finishes action mode
                mActionMode!!.finish()
            }
            // if in action mode, don't do any collapse/expand action
            return false
        }
        val verse = getChild(groupPosition, childPosition)
        // open the selected verse in the reader
        mMainActivity.startReaderWithIntent(verse)
        return true
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        val label = mLabels[groupPosition]
        return if (label.isMarkedAsDeleted) {
            0
        } else label.size()
    }

    override fun getGroup(groupPosition: Int): Label {
        return mLabels[groupPosition]
    }

    override fun getGroupCount(): Int {
        return mLabels.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var retView = convertView
        if (retView == null) {
            val inflater = mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            retView = inflater.inflate(R.layout.bookmark_group_undo_list_item, null)
        }
        retView?.let {
            // group
            val label = mLabels[groupPosition]
            // TextView
            val foreGroundTv = it.findViewById<View>(R.id.bookmark_undo_list_item_foreground) as TextView
            foreGroundTv.text = label.getDisplayName(mMainActivity)
            // background
            val backGroundView = it.findViewById<View>(R.id.includable_undo_list_item_background_container)
            // determine if this item has been dismissed
            if (!label.isMarkedAsDeleted) {
                foreGroundTv.x = 0f
                foreGroundTv.alpha = 1f
                backGroundView.visibility = View.GONE
            } else {
                foreGroundTv.x = parent!!.width.toFloat()
                foreGroundTv.alpha = 0.5f
                backGroundView.visibility = View.VISIBLE
                // set the OnClickListener for the undo button
                val undoButton = it.findViewById<View>(R.id.includable_undo_list_item_background_button) as Button
                undoButton.setOnClickListener {
                    val removed = onUndoGroup(groupPosition)
                    if (removed) {
                        foreGroundTv.animateUndo(backGroundView, mAnimationTime)
                    } else {
                        removeDismissedItemsAndNotify()
                    }
                }
            }
        }
        // Rückgabewert
        return retView!!
    }

    override fun onGroupClick(parent: ExpandableListView?, v: View?, groupPosition: Int, id: Long): Boolean {
        if (isGroupDismissed(groupPosition)) {
            // group item is dismissed, don't do any collapse/expand action (returns true if the click was handled)
            return true
        }
        mActionMode?.let {
            if (mActionModeLabelIndex == groupPosition) {
                // short click on selected action mode group finishes action mode
                mActionMode!!.finish()
            }
            // if in action mode, don't do any collapse/expand action
            return@onGroupClick true
        }
        val label = getGroup(groupPosition)
        return label.isMarkedAsDeleted
    }

    override fun onItemLongClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long): Boolean {
        if (mActionMode != null) {
            return false
        }
        mActionModeView = view?.findViewById(R.id.bookmark_undo_list_item_foreground)
        val groupPosition = ExpandableListView.getPackedPositionGroup(id)
        mActionModeLabelIndex = groupPosition
        val itemType = ExpandableListView.getPackedPositionType(id)
        mActionModeVerseIndex = when (itemType) {
            ExpandableListView.PACKED_POSITION_TYPE_CHILD -> {
                val childPosition = ExpandableListView.getPackedPositionChild(id)
                childPosition
            }
            ExpandableListView.PACKED_POSITION_TYPE_GROUP -> -1
            else ->                 // null item; we don't consume the click
                return false
        }
        mActionMode = mMainActivity.startSupportActionMode(this)
        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            val actionModeVerse = getGroup(mActionModeLabelIndex).getBookMark(mActionModeVerseIndex)
            mShareActionProvider?.setShareIntent(actionModeVerse.getShareIntent())
        }
        return true
    }

    override fun hasStableIds() = false

    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = true

    // ### ACTION MODE ###
    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        if (mActionMode != null || hasDismissedItems()) {
            return false
        }
        // status bar color
        setStatusBarColorActionMode(mMainActivity)
        // inflate the menu
        val inflater = mode.menuInflater
        inflater.inflate(R.menu.menu_bookmars_contextual, menu)
        // Share-Item
        val shareMenuItem = menu.findItem(R.id.bookmarks_menu_item_share)
        if (mActionModeVerseIndex != -1) {
            // work arround:
            mShareActionProvider = ShareActionProvider(mMainActivity)
            MenuItemCompat.setActionProvider(shareMenuItem, mShareActionProvider)
        } else {
            // Label gewählt
            menu.removeItem(shareMenuItem.itemId)
        }
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
        mActionModeView!!.isSelected = true
        // set the title
        mode.title = "1" + mMainActivity.getString(R.string.actionmode_selected) //TODO
        return false
    }

    override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.bookmarks_menu_item_delete -> {
                // is this list item a verse or a label?
                if (mActionModeVerseIndex != -1) {
                    deleteVerse(mActionModeLabelIndex, mActionModeVerseIndex)
                    removeChild(mActionModeLabelIndex, mActionModeVerseIndex)
                } else {
                    deleteLabel(mActionModeLabelIndex)
                    removeGroup(mActionModeLabelIndex)
                }
                // notify the reader
                mDeleteBookmarksCallback?.onItemDeleted()
                notifyDataSetChanged()
                // ActionMode beenden
                mActionMode!!.finish()
                true
            }
            else -> false
        }
    }

    override fun onDestroyActionMode(mode: ActionMode) {
        val themeId = Settings.getThemeId(mMainActivity)
        val array = mMainActivity.theme.obtainStyledAttributes(
                themeId, intArrayOf(R.attr.bookmarkChildListItemForeground, R.attr.bookmarkGroupListItemForeground))
        if (mActionModeVerseIndex != -1) {
            val childResId = array.getResourceId(0, 0)
            mActionModeView!!.setBackgroundResource(childResId)
        } else {
            val groupResId = array.getResourceId(1, 0)
            mActionModeView!!.setBackgroundResource(groupResId)
        }
        array.recycle()
        // status bar color
        setStatusBarColorDefault(mMainActivity)
        // clean up
        mActionModeView!!.isSelected = false
        mActionMode = null
        mActionModeLabelIndex = -1
        mActionModeVerseIndex = -1
        mActionModeView = null
        mMainActivity.clearActionMode()
    }

    // ### SWIPE TO DISMISS ###
    override fun canDismiss(flatPosition: Int): Boolean {
        // Every list item can be dismissed
        return true
    }

    override fun onDismiss(expandableListView: ExpandableListView?, flatPosition: Int) {
        // get the packed group (and child) positions for the flat list position
        val packedPosition = expandableListView!!.getExpandableListPosition(flatPosition)
        // retrieve the items type
        val itemType = ExpandableListView.getPackedPositionType(packedPosition)
        // check
        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            // this item is a child
            val childPosition = ExpandableListView.getPackedPositionChild(packedPosition)
            val groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition)
            if (mActionMode != null && groupPosition == mActionModeLabelIndex && childPosition == mActionModeVerseIndex) {
                // the swiped item is selected as the action item
                // so finish the action mode
                mActionMode!!.finish()
            } else if (groupPosition < mActionModeLabelIndex && childPosition < mActionModeVerseIndex) {
                mActionMode!!.finish()
            }
            // delete the verse
            deleteVerse(groupPosition, childPosition)
        } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
            // this item is a group
            val groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition)
            if (mActionMode != null && groupPosition == mActionModeLabelIndex) {
                // the swiped group is selected as the action item
                // or contains the action item
                // so finish the action mode
                mActionMode!!.finish()
            }
            // delete the label
            deleteLabel(groupPosition)
            // collapse this group
            expandableListView.collapseGroup(groupPosition)
        }
        // notify the reader
        mDeleteBookmarksCallback?.onItemDeleted()
        // TODO add a 'DONE' action item
    }

    override fun onUndo(expandableListView: ExpandableListView?, position: Int): Boolean {
        // get the packed group (and child) positions for the flat list position
        val packedPosition = expandableListView!!.getExpandableListPosition(position)
        // retrieve the items type
        val itemType = ExpandableListView.getPackedPositionType(packedPosition)
        // check
        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            // this item is a child
            val childPosition = ExpandableListView.getPackedPositionChild(packedPosition)
            val groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition)
            return onUndoChild(groupPosition, childPosition)
        } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
            // this item is a group
            val groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition)
            return onUndoGroup(groupPosition)
        }
        return false
    }

    private fun onUndoChild(groupPosition: Int, childPosition: Int): Boolean {
        BibleLog.d(TAG, "onUndoChild()")
        val undoVerse = getChild(groupPosition, childPosition)
        val undoLabelName = getGroup(groupPosition).name
        // re-add the label to the verse
        try {
            undoVerse.addLabel(undoLabelName)
            // Provider aktualisieren
            // neue id wird ggf f�r verse-objekt �bernommen
            BibleProviderUtils.setBookmarks(undoVerse, mMainActivity) // TODO thread
            // report success
            return true
        } catch (e: DuplicateLabelException) {
            e.printStackTrace()
        } catch (e: IllegalLabelFormatException) {
            e.printStackTrace()
        }
        return true
    }

    private fun onUndoGroup(groupPosition: Int): Boolean {
        BibleLog.d(TAG, "onUndoGroup($groupPosition)")
        val undoLabel = getGroup(groupPosition)
        if (!undoLabel.isMarkedAsDeleted) {
            BibleLog.w(TAG, "group with groupPosition $groupPosition was not deleted beforehand")
            // item was not deleted beforehand, no action required
            return false
        }
        // re-add this label to the verses in database
        // TODO this should be a batch insert
        for (verse in undoLabel.bookMarks) {
            try {
                verse.addLabel(undoLabel.name)
            } catch (e: DuplicateLabelException) {
                e.printStackTrace()
            } catch (e: IllegalLabelFormatException) {
                e.printStackTrace()
            }
            BibleLog.i(TAG, verse.getLocator() + " contains labels : " + verse.getCommaSeparatedLabels())
            BibleProviderUtils.setBookmarks(verse, mMainActivity) // TODO should be executed from background thread
        }
        // mark this label as not deleted
        undoLabel.isMarkedAsDeleted = false
        // successfully restored
        return true
    }

    override fun onItemsCollapsed() {
        removeDismissedItemsAndNotify()
    }

    private fun removeDismissedItemsAndNotify() {
        val labelIterator = mLabels.iterator()
        while (labelIterator.hasNext()) {
            val label = labelIterator.next()
            if (label.isMarkedAsDeleted || label.size() == 0) {
                labelIterator.remove()
            } else {
                val verseIterator = label.bookMarks.iterator()
                while (verseIterator.hasNext()) {
                    val verse = verseIterator.next()
                    if (!verse.containsLabel(label)) {
                        verseIterator.remove()
                    }
                }
            }
        }

        // redraw
        notifyDataSetChanged()
        mDeleteBookmarksCallback?.onLabelCountChanged(mLabels.size)
    }

    override fun getDismissPositions(expandableListView: ExpandableListView?): Vector<Int> {
        BibleLog.d(TAG, "getDismissPositions()")
        val dismissedPositions = Vector<Int>()
        var position = -1
        // iterate over the list of labels
        for (groupPos in mLabels.indices) {
            val label = mLabels[groupPos]
            position += 1
            if (label.isMarkedAsDeleted) {
                BibleLog.d(TAG, "Adding Label '" + label.name + "' to list of dismissed positions")
                dismissedPositions.add(position)
            }
            if (expandableListView!!.isGroupExpanded(groupPos)) {
                // iterate over the verses contained in the current label
                for (verse in label.bookMarks) {
                    position += 1
                    if (!verse.containsLabel(label)) {
                        BibleLog.d(TAG, "Adding Verse '" + verse.getShortLocator() + "' to list of dismissed positions")
                        dismissedPositions.add(position)
                    }
                }
            }
        }
        return dismissedPositions
    }

    /**
     * Determines if an item in the adapter is in dismissed state. This will also return true, if items in an collapsed group are dismissed.
     * In comparison DismissCallbacks.getDismissPositions() returns only positions for childs, if they are in an expanded group.
     *
     * @return boolean
     */
    private fun hasDismissedItems(): Boolean {
        // iterate over the list of labels
        for (groupPos in mLabels.indices) {
            val label = mLabels[groupPos]
            if (label.isMarkedAsDeleted) {
                return true
            }
            // iterate over the verses contained in the current label
            for (verse in label.bookMarks) {
                if (!verse.containsLabel(label)) {
                    return true
                }
            }
        }
        return false
    }

    override fun isDismissed(expandableListView: ExpandableListView?, absolutePosition: Int): Boolean {
        // get the packed group (and child) positions for the flat list position
        val packedPosition = expandableListView!!.getExpandableListPosition(absolutePosition)
        // retrieve the items type
        val itemType = ExpandableListView.getPackedPositionType(packedPosition)
        // check
        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            // this item is a child
            val childPosition = ExpandableListView.getPackedPositionChild(packedPosition)
            val groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition)
            return isChildDismissed(groupPosition, childPosition)
        } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
            // this item is a group
            val groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition)
            return isGroupDismissed(groupPosition)
        }
        return false
    }

    private fun isGroupDismissed(groupPosition: Int): Boolean {
        val label = getGroup(groupPosition)
        return label.isMarkedAsDeleted
    }

    private fun isChildDismissed(groupPosition: Int, childPosition: Int): Boolean {
        val label = getGroup(groupPosition)
        val verse = getChild(groupPosition, childPosition)
        return !verse.containsLabel(label)
    }

    /**
     * Deletes a verse from the provider database
     */
    private fun deleteVerse(groupPosition: Int, childPosition: Int) {
        // Vers gew�hlt --> Label entfernen
        val removeVerse = getChild(groupPosition, childPosition)
        val (removeLabelName) = getGroup(groupPosition)
        // remove the desired label from this verses labels
        removeVerse.removeLabel(removeLabelName)
        // Provider aktualisieren
        // neue id wird ggf f�r verse-objekt �bernommen
        BibleProviderUtils.setBookmarks(removeVerse, mMainActivity) // TODO thread
    }

    /**
     * removes a child from a label in the adapter
     */
    private fun removeChild(groupPosition: Int, childPosition: Int) {
        val (_, bookMarks) = getGroup(groupPosition)
        // remove this verse from the corresponding label group
        bookMarks.removeAt(childPosition)
        // remove the label group if it is now empty
        if (getGroup(groupPosition).size() == 0) {
            mLabels.removeAt(groupPosition)
            mDeleteBookmarksCallback?.onLabelCountChanged(mLabels.size)
        }
    }

    /**
     * Deletes a label from the database
     *
     * @param groupPosition The Position of the label group that should be deleted
     */
    private fun deleteLabel(groupPosition: Int) {
        val removeLabel = getGroup(groupPosition)
        for (verse in removeLabel.bookMarks) {
            // remove the label from the verse object
            verse.removeLabel(removeLabel.name)
        }
        // delete this label from the database
        BibleProviderUtils.deleteLabel(removeLabel, mMainActivity) // TODO thread
        // mark the label object as deleted
        removeLabel.isMarkedAsDeleted = true
    }

    /**
     * removes a label from the adapter
     *
     * @param groupPosition the groupPosition of the label
     */
    private fun removeGroup(groupPosition: Int) {
        // remove the label group from the local data set
        mLabels.removeAt(groupPosition)
        mDeleteBookmarksCallback?.onLabelCountChanged(mLabels.size)
    }

    companion object {
        // Log-Tag
        const val TAG = "BookmarksExpandableListAdapter"
    }

}