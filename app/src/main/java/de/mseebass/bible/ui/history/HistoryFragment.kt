package de.mseebass.bible.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import de.mseebass.bible.R
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.repository.Error
import de.mseebass.bible.repository.RepoResult
import de.mseebass.bible.repository.Success
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils
import kotlinx.android.synthetic.main.history_fragment.*

class HistoryFragment : BaseFragment() {

    companion object {
        // Log-TAG
        const val TAG = "HistoryFragment"
    }

    private lateinit var viewModel : HistoryViewModel
    private var adapter: HistoryRecyclerViewAdapter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.history_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // in onViewCreated because snythetic views are only accessible after onCreateView
        try {
            adapter = HistoryRecyclerViewAdapter((activity as MainActivity?)!!, ArrayList())
            history_recyclerView.adapter = adapter
            history_recyclerView.layoutManager = LinearLayoutManager(activity)

            viewModel = ViewModelProvider(this).get(HistoryViewModel::class.java)

            viewModel.result.observe(viewLifecycleOwner,
                    Observer { result ->
                        onResultChanged(result)
                    })

            viewModel.isLoading.observe(viewLifecycleOwner,
                    Observer { loading ->
                        setProgressBarVisible(loading)
                        history_swipeRefreshLayout.isRefreshing = false
                    })

            viewModel.fetchData()

            history_swipeRefreshLayout.setOnRefreshListener {
                setProgressBarVisible(true)
                history_recyclerView?.isVisible = false
                history_empty_message_tv?.isVisible = false
                viewModel.fetchData()
            }
            history_swipeRefreshLayout.setColorSchemeResources(
                    ResourceUtils.getThemedResourceId(R.attr.colorAccent, activity!!),
                    ResourceUtils.getThemedResourceId(R.attr.colorPrimary, activity!!)
            )

        } catch (thr : Throwable) {
            BibleLog.e(TAG, "Error creating HistoryFragment", thr);
        }
    }

    override fun onResume() {
        BibleLog.d(TAG, "onResume()")
        // notify the navigation drawer
        val activity = activity as MainActivity?
        activity?.onNavigationDrawerItemSelected(MainActivity.POSITION_HISTORY)
        activity?.hideToolbarSpinner()
        // ActionBar
        val actionBar = (activity as AppCompatActivity?)!!.supportActionBar
        actionBar!!.setDisplayShowTitleEnabled(true)
        super.onResume()
    }

    private fun onResultChanged(result: RepoResult<List<HistoryEntry>>) {
        when (result) {
            is Success<List<HistoryEntry>> -> { updateRecyclerView(result.data) }
            is Error -> {
                onError(result.exception)
            }
        }
    }

    private fun updateRecyclerView(entries : List<HistoryEntry>) {
        adapter?.entries = entries
        adapter?.notifyDataSetChanged()
        if (entries.isNotEmpty()) {
            onDataAvailable(entries)
        } else {
            onListEmpty()
        }
    }

    private fun onDataAvailable(entries : List<HistoryEntry>) {
        history_recyclerView?.isVisible = true
        history_empty_message_tv?.isVisible = false
//        history_fragment_progressbar?.visibility = View.GONE
    }

    private fun onListEmpty() {
        history_recyclerView?.isVisible = false
        history_empty_message_tv?.isVisible = true
        history_empty_message_tv?.text = getString(R.string.history_empty_state_msg)
//        history_fragment_progressbar?.visibility = View.GONE
    }

    private fun onError(thr : Throwable ) {
        history_empty_message_tv?.isVisible = true
        history_empty_message_tv?.text = getString(R.string.history_error_state_msg)
//        history_fragment_progressbar?.visibility = View.GONE
        try {
            Toast.makeText(activity, getString(R.string.history_error_state_msg) + " : " + thr.javaClass.simpleName + ": " + thr.message, Toast.LENGTH_LONG).show()
        } catch (e : Exception) {
            BibleLog.w(TAG, "Error:", e)
        }
    }

    private fun setProgressBarVisible(visible: Boolean) {
        history_fragment_progressbar?.isVisible = visible
    }

}