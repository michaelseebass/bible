package de.mseebass.bible.ui.highlights

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.text.TextUtils
import android.util.SparseBooleanArray
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.ShareActionProvider
import androidx.core.view.MenuItemCompat
import de.mseebass.bible.R
import de.mseebass.bible.interfaces.OnColorSelectedListener
import de.mseebass.bible.interfaces.OnLabelSelectedListener
import de.mseebass.bible.models.Verse
import de.mseebass.bible.models.Verse.DuplicateLabelException
import de.mseebass.bible.models.Verse.IllegalLabelFormatException
import de.mseebass.bible.providers.BibleProvider.Bookmarks
import de.mseebass.bible.providers.BibleProviderUtils
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.bookmarks.BookMarkGroupDialogFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils.getDisplayHighlightColor
import de.mseebass.bible.utils.ResourceUtils.setStatusBarColorActionMode
import de.mseebass.bible.utils.ResourceUtils.setStatusBarColorDefault
import java.util.*

class HighlightedVersesListAdapter(
        private val mainActivity: MainActivity,
        verses: Vector<Verse>?,
        private val callBack: OnHighlightedVerseDeletedListener
) : ArrayAdapter<Verse>(mainActivity, R.layout.highlighted_verses_list_item, verses!!),
        ActionMode.Callback,
        OnLabelSelectedListener,
        OnColorSelectedListener
{

    // Contextual
    private var mActionMode: ActionMode? = null
    private var mShareActionProvider: ShareActionProvider? = null
    private var mSelectedItemPositions = SparseBooleanArray()


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Verse-Layout
        var retView = convertView
        if (retView == null) {
            retView = View.inflate(context, R.layout.highlighted_verses_list_item, null)
        }

        // TextViews
        val locatorTv = retView!!.findViewById<View>(R.id.highlighted_verses_list_item_locator_tv) as TextView
        val bibleTv = retView.findViewById<View>(R.id.highlighted_verses_list_item_bible_tv) as TextView
        val textTv = retView.findViewById<View>(R.id.highlighted_verses_list_item_text_tv) as TextView
        // Verse
        val verse = getItem(position)
        // Text setzen
        locatorTv.text = verse!!.getLocator()
        bibleTv.text = verse.bibleShortName
        textTv.text = verse.text
        // color button
        val colorImageButton = retView.findViewById<View>(R.id.highlighted_verses_list_item_color_image_button) as ImageView
        val bgShape = colorImageButton.drawable as GradientDrawable
        val color = getDisplayHighlightColor(verse.highLightColor)
        bgShape.setColor(color)
        // get a final reference to the list item view for the listener
        val listItemView = retView
        colorImageButton.setOnClickListener {
            onListItemSelect(position, listItemView)
        }
        colorImageButton.setOnLongClickListener {
            onListItemSelect(position, listItemView)
            true
        }
        // OnClickListener setzen
        retView.setOnClickListener { view ->
            if (mActionMode != null) {
                // add or remove selection for current list item
                onListItemSelect(position, view)
            } else {
                mainActivity.startReaderWithIntent(verse)
            }
        }
        // Listener for ActionMode
        retView.setOnLongClickListener { view ->
            onListItemSelect(position, view)
            true
        }
        // view state
        retView.isActivated = mSelectedItemPositions[position]
        // return value
        return retView
    }

    /**
     * Called when a list item is selected or unselected for the contextual mode
     *
     * @param position the list position of the selected item
     */
    private fun onListItemSelect(position: Int, view: View?) {
        toggleSelection(position, view)
        val hasCheckedItems = selectedCount > 0
        if (hasCheckedItems && mActionMode == null) {
            // there are some selected items, start the actionMode
            mActionMode = mainActivity.startSupportActionMode(this)
        } else if (!hasCheckedItems && mActionMode != null) {
            // there no selected items, finish the actionMode
            mActionMode!!.finish()
        }
        if (mActionMode != null) {
            mActionMode!!.title = selectedCount.toString() + mainActivity.getString(R.string.actionmode_selected)
        }
        setShareIntent()
    }

    private fun toggleSelection(position: Int, view: View?) {
        if (!mSelectedItemPositions[position]) {
            mSelectedItemPositions.put(position, true)
            view?.isActivated = true
        } else {
            mSelectedItemPositions.delete(position)
            view?.isActivated = false
        }
    }

    private fun removeSelection() {
        mSelectedItemPositions = SparseBooleanArray()
        notifyDataSetChanged()
    }

    private val selectedCount: Int
        private get() = mSelectedItemPositions.size()

    private fun setShareIntent() {
        if (mShareActionProvider == null) {
            BibleLog.w(TAG, "setShareIntent(): exiting method - mShareActionProvider == null")
            return
        }
        val selectedVerses = Vector<Verse?>()
        for (i in 0 until selectedCount) {
            if (mSelectedItemPositions.valueAt(i)) {
                selectedVerses.add(getItem(mSelectedItemPositions.keyAt(i)))
            }
        }
        var subject: String? = null
        var text: String? = null
        if (selectedVerses.size == 1) {
            subject = selectedVerses[0]!!.getLocator()
            text = selectedVerses[0]!!.getLocator() + ": " + selectedVerses[0]!!.text
        } else if (selectedVerses.size > 1) {
            text = selectedVerses[0]!!.getLocator() + ": " + selectedVerses[0]!!.text
            for (i in 1 until selectedVerses.size) {
                text += "\n"
                text += selectedVerses[i]!!.getLocator() + ": " + selectedVerses[i]!!.text
            }
        }
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        if (subject != null) {
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (text != null) {
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        }
        mShareActionProvider!!.setShareIntent(shareIntent)
    }

    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        // status bar color
        setStatusBarColorActionMode(mainActivity)
        // inflate the menu resource
        val inflater = mode.menuInflater
        inflater.inflate(R.menu.menu_highlighted_verses_contextual, menu)
        // Share
        val shareMenuItem = menu.findItem(R.id.highlighted_verses_menu_item_contextual_share)
        mShareActionProvider = ShareActionProvider(mainActivity)
        MenuItemCompat.setActionProvider(shareMenuItem, mShareActionProvider)
        // Rückgabewert
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode, menu: Menu)=  false

    override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.highlighted_verses_menu_item_contextual_colors -> {
                showColorPicker()
                true
            }
            R.id.highlighted_verses_menu_item_contextual_labels -> {
                val labels = BibleProviderUtils.getLabelGroups(context)
                BookMarkGroupDialogFragment.showDialog(this@HighlightedVersesListAdapter, labels, mainActivity.supportFragmentManager)
                true
            }
            else -> false
        }
    }

    override fun onDestroyActionMode(mode: ActionMode) {
        removeSelection()
        mActionMode = null
        mainActivity.clearActionMode()

        // remove deleted verses
        val deletedVerses = Vector<Verse?>()
        for (i in 0 until count) {
            val verse = getItem(i)
            if (verse!!.highLightColor == Bookmarks.COLOR_NONE) {
                // remove this verse from the adapter, if it is not highlighted any more
                deletedVerses.add(verse)
                // notify the reader
                callBack.onItemDeleted()
                // TODO UNDO
            }
        }
        // remove items from the adapter
        for (deletedVerse in deletedVerses) {
            this.remove(deletedVerse)
        }
        //set status bar color
        setStatusBarColorDefault(mainActivity)
    }

    /**
     * Displays the ColorPickerDialogFragment
     * see onColorSelected(int color)
     */
    private fun showColorPicker() {
        val fragment = ColorPickerDialogFragment()
        fragment.setOnColorSelectedListener(this)
        fragment.show(mainActivity.supportFragmentManager, ColorPickerDialogFragment.TAG)
    }

    override fun onLabelSelected(newLabel: String) {
        if (TextUtils.isEmpty(newLabel)) {
            Toast.makeText(mainActivity, R.string.bookmarks_group_name_empty_error, Toast.LENGTH_SHORT).show()
        } else {
            if (selectedCount == 0) return
            // index of first selected Verse
            val verseIndex = mSelectedItemPositions.keyAt(0)
            try {
                val verse = getItem(verseIndex)
                // add Label
                verse!!.addLabel(newLabel)
                //				verse.setBibleShortName(mBibleShortName); bible short name should already exist at this point
                // Database entry - this sets the bookmark id for the verse object
                BibleProviderUtils.setBookmarks(verse, mainActivity) //TODO Thread?
                // notify the user
                Toast.makeText(
                        mainActivity,
                        verse.getLocator() + mainActivity.getText(R.string.actionmode_bookmark_added),
                        Toast.LENGTH_SHORT
                ).show()
                // finish the contextual action mode
                mActionMode?.finish()
            } catch (e: DuplicateLabelException) {
                Toast.makeText(mainActivity, R.string.bookmarks_group_name_duplicate_error, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            } catch (e: IllegalLabelFormatException) {
                Toast.makeText(mainActivity, R.string.bookmarks_group_name_contains_comma_error, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }
        }
    }

    override fun onColorSelected(solidColor: Int) {
        if (selectedCount == 0) return
        for (i in 0 until selectedCount) {
            if (mSelectedItemPositions.valueAt(i)) {
                // selected Verse
                val currentVerseIndex = mSelectedItemPositions.keyAt(i)
                val verse = getItem(currentVerseIndex)
                // set the new color
                verse!!.highLightColor = solidColor
                // database entry
                BibleProviderUtils.setBookmarks(verse, mainActivity) //TODO Thread?, Batch insert
            }
        }
        // finish the contextual action mode
        mActionMode?.finish()
    }

    companion object {
        const val TAG = "HighlightedVersesListAdapter"
    }

}