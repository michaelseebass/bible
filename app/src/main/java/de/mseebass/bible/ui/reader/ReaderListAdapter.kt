package de.mseebass.bible.ui.reader

import android.content.Intent
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.BackgroundColorSpan
import android.text.style.StyleSpan
import android.util.SparseBooleanArray
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.ShareActionProvider
import androidx.core.view.MenuItemCompat
import de.mseebass.bible.R
import de.mseebass.bible.interfaces.OnColorSelectedListener
import de.mseebass.bible.interfaces.OnLabelSelectedListener
import de.mseebass.bible.interfaces.OnNotesChangedListener
import de.mseebass.bible.models.Verse
import de.mseebass.bible.models.Verse.DuplicateLabelException
import de.mseebass.bible.models.Verse.IllegalLabelFormatException
import de.mseebass.bible.providers.BibleProvider.Bookmarks
import de.mseebass.bible.providers.BibleProviderUtils
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.bookmarks.BookMarkGroupDialogFragment
import de.mseebass.bible.ui.highlights.ColorPickerDialogFragment
import de.mseebass.bible.ui.notes.NotesDialogFragment
import de.mseebass.bible.ui.search.getQuerySpannable
import de.mseebass.bible.utils.ResourceUtils.getTextBackgroundHighlightColor
import de.mseebass.bible.utils.ResourceUtils.setStatusBarColorActionMode
import de.mseebass.bible.utils.ResourceUtils.setStatusBarColorDefault
import de.mseebass.bible.utils.Settings
import java.util.*
import java.util.regex.Pattern

class ReaderListAdapter(
        private val mainActivity: MainActivity,
        private val verses: Vector<Verse>,
        private val bibleShortName: String
) : ArrayAdapter<Verse?>(mainActivity, R.layout.verse_text_view, verses),
        ActionMode.Callback,
        OnLabelSelectedListener,
        OnColorSelectedListener,
        OnNotesChangedListener
{
    // Contextual
    private var mActionMode: ActionMode? = null
    private var mShareActionProvider: ShareActionProvider? = null
    private var mSelectedItemPositions = SparseBooleanArray()
    // Search
    private var mQuery: List<String>? = null

    fun setSearchQuery(query: List<String>?) {
        mQuery = query
    }

    init {
        val fm = mainActivity.supportFragmentManager
        val fragment = fm.findFragmentByTag(ColorPickerDialogFragment.TAG) as ColorPickerDialogFragment?
        fragment?.dismiss()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Verse-Layout
        var retView = convertView
        if (retView == null) {
            retView = View.inflate(context, R.layout.verse_text_view, null)
        }
        val verseTv = retView as TextView

        // Text-Size
        val textScale = Settings.getFontScale(context)
        if (textScale == 0) {
            verseTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        } else if (textScale == 1) {
            verseTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
        } else if (textScale == 2) {
            verseTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        }
        val verse = verses[position]
        if (mQuery == null) {
            verseTv.text = getFormatedSpannable(verse)
        } else {
            verseTv.text = getQuerySpannable(context, verse, mQuery!!)
        }

        // OnClickListener setzen
        verseTv.setOnClickListener { view ->
            if (mActionMode != null) {
                // add or remove selection for current list item
                onListItemSelect(position, view)
            } else if (mQuery != null) {
                mainActivity.startReaderWithIntent(verse)
            }
        }
        // Listener f�r ActionMode
        verseTv.setOnLongClickListener { view ->
            onListItemSelect(position, view)
            true
        }
        // view state
        verseTv.isActivated = mSelectedItemPositions[position]
        // return value
        return verseTv
    }

    private fun getFormatedSpannable(verse: Verse): Spannable {
        val numberStr = verse.number.toString()
        val cleanedText = getCleanedVerseText(verse)
        val src: Spannable = SpannableString("$numberStr $cleanedText")
        src.setSpan(StyleSpan(Typeface.BOLD), 0, numberStr.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        if (verse.highLightColor != Bookmarks.COLOR_NONE) {
            val alphaColor = getTextBackgroundHighlightColor(verse.highLightColor)
            src.setSpan(BackgroundColorSpan(alphaColor), 0, src.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        return src
    }

    private fun onListItemSelect(position: Int, view: View) {
        toggleSelection(position, view)
        val hasCheckedItems = selectedCount > 0
        if (hasCheckedItems && mActionMode == null) {
            // there are some selected items, start the actionMode
            mActionMode = mainActivity.startSupportActionMode(this)
        } else if (!hasCheckedItems && mActionMode != null) {
            // there no selected items, finish the actionMode
            mActionMode?.finish()
        }
        mActionMode?.title = selectedCount.toString() + mainActivity.getString(R.string.actionmode_selected)
        mActionMode?.invalidate()
        setShareIntent()
    }

    private fun toggleSelection(position: Int, view: View) {
        if (!mSelectedItemPositions[position]) {
            mSelectedItemPositions.put(position, true)
            view.isActivated = true
        } else {
            mSelectedItemPositions.delete(position)
            view.isActivated = false
        }
    }

    private fun removeSelection() {
        mSelectedItemPositions = SparseBooleanArray()
    }

    private val selectedCount: Int
        get() = mSelectedItemPositions.size()

    private fun setShareIntent() {
        if (mShareActionProvider == null) {
            return
        }
        val selectedVerses = Vector<Verse?>()
        for (i in 0 until selectedCount) {
            if (mSelectedItemPositions.valueAt(i)) {
                selectedVerses.add(getItem(mSelectedItemPositions.keyAt(i)))
            }
        }
        var subject: String? = null
        var text: String? = null
        if (selectedVerses.size == 1) {
            subject = selectedVerses[0]!!.getLocator()
            text = selectedVerses[0]!!.getLocator() + ": " + selectedVerses[0]!!.text
        } else if (selectedVerses.size > 1) {
            text = selectedVerses[0]!!.getLocator() + ": " + selectedVerses[0]!!.text
            for (i in 1 until selectedVerses.size) {
                text += "\n"
                text += selectedVerses[i]!!.getLocator() + ": " + selectedVerses[i]!!.text
            }
        }
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        if (subject != null) {
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (text != null) {
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        }
        mShareActionProvider!!.setShareIntent(shareIntent)
    }

    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        // Status bar color
        setStatusBarColorActionMode(mainActivity)
        // inflate the menu
        val inflater = mode.menuInflater
        inflater.inflate(R.menu.menu_reader_contextual, menu)
        // Share
        val shareMenuItem = menu.findItem(R.id.reader_menu_item_contextual_share)
        mShareActionProvider = ShareActionProvider(mainActivity)
        MenuItemCompat.setActionProvider(shareMenuItem, mShareActionProvider)
        // Rückgabewert
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
        menu.findItem(R.id.reader_menu_item_contextual_notes).setVisible(selectedCount == 1)
        return false
    }

    override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.reader_menu_item_contextual_colors -> {
                val fragment = ColorPickerDialogFragment()
                fragment.setOnColorSelectedListener(this)
                fragment.show(mainActivity.supportFragmentManager, ColorPickerDialogFragment.TAG)
                true
            }
            R.id.reader_menu_item_contextual_labels -> {
                val labels = BibleProviderUtils.getLabelGroups(context)
                BookMarkGroupDialogFragment.showDialog(this@ReaderListAdapter, labels, mainActivity.supportFragmentManager)
                true
            }
            R.id.reader_menu_item_contextual_notes -> {
                val verseIndex = mSelectedItemPositions.keyAt(0)
                val verse = verses[verseIndex]
                verse.bibleShortName = bibleShortName
                val fragment = NotesDialogFragment.instance(verse,this)
                fragment.show(mainActivity.supportFragmentManager, NotesDialogFragment.TAG)
                true
            }
            else -> false
        }
    }

    override fun onDestroyActionMode(mode: ActionMode) {
        removeSelection()
        mActionMode = null
        mainActivity.clearActionMode()
        //set status bar color
        setStatusBarColorDefault(mainActivity)
        notifyDataSetChanged()
    }

    override fun onLabelSelected(newLabel: String) {
        if (TextUtils.isEmpty(newLabel)) {
            Toast.makeText(mainActivity, R.string.bookmarks_group_name_empty_error, Toast.LENGTH_SHORT).show()
        } else {
            if (selectedCount == 0) return
            // index of first selected Verse
            val verseIndex = mSelectedItemPositions.keyAt(0)
            try {
                val verse = verses[verseIndex]
                // add Label
                verse.addLabel(newLabel)
                verse.bibleShortName = bibleShortName
                // Database entry - this sets the bookmark id for the verse object
                BibleProviderUtils.setBookmarks(verses[verseIndex], mainActivity) //TODO Thread?
                // notify the user
                Toast.makeText(
                        mainActivity,
                        verses[verseIndex].getLocator() + mainActivity.getText(R.string.actionmode_bookmark_added),
                        Toast.LENGTH_SHORT
                ).show()
                // finish the contextual action mode
                if (mActionMode != null) {
                    mActionMode!!.finish()
                }
            } catch (e: DuplicateLabelException) {
                Toast.makeText(mainActivity, R.string.bookmarks_group_name_duplicate_error, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            } catch (e: IllegalLabelFormatException) {
                Toast.makeText(mainActivity, R.string.bookmarks_group_name_contains_comma_error, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }
        }
    }

    override fun onColorSelected(solidColor: Int) {
        if (selectedCount == 0) return

        for (i in 0 until selectedCount) {
            if (mSelectedItemPositions.valueAt(i)) {
                // selected Verse
                val currentVerseIndex = mSelectedItemPositions.keyAt(i)
                val verse = verses[currentVerseIndex]
                // set the new color
                verse.highLightColor = solidColor
                verse.bibleShortName = bibleShortName
                // database entry
                BibleProviderUtils.setBookmarks(verses[currentVerseIndex], mainActivity) //TODO Thread?
            }
        }
        // finish the contextual action mode
        mActionMode?.finish()
    }

    override fun onNotesChanged() {
    }

    companion object {
        const val TAG = "ReaderListAdapter"
        private fun getCleanedVerseText(verse: Verse): String? {
            val pattern = Pattern.compile("\\{.*?\\}")
            val matcher = pattern.matcher(verse.text)
            return matcher.replaceAll("") //TODO doppelte whitespaces entfernen ??
        }
    }
}