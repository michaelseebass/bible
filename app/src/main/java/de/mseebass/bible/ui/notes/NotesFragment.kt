package de.mseebass.bible.ui.notes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import de.mseebass.bible.R
import de.mseebass.bible.databinding.NotesFragmentBinding
import de.mseebass.bible.interfaces.OnNotesChangedListener
import de.mseebass.bible.models.Verse
import de.mseebass.bible.repository.Error
import de.mseebass.bible.repository.RepoResult
import de.mseebass.bible.repository.Success
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.utils.BibleLog

class NotesFragment : BaseFragment(), NotesClickListener {

    companion object {
        // Log-TAG
        const val TAG = "NotesFragment"
    }

    private lateinit var viewModel : NotesViewModel
    private lateinit var binding: NotesFragmentBinding
    private var adapter: NotesRecyclerViewAdapter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.notes_fragment, container, false)

        try {
            adapter = NotesRecyclerViewAdapter(ArrayList(), callback = this)
            binding.notesRecyclerView.adapter = adapter
            binding.notesRecyclerView.layoutManager = LinearLayoutManager(activity)

            viewModel = ViewModelProvider(this).get(NotesViewModel::class.java)

            viewModel.result.observe(viewLifecycleOwner,
                    Observer { result ->
                        onResultChanged(result)
                    })

            viewModel.isLoading.observe(viewLifecycleOwner,
                    Observer { loading ->
                        setProgressBarVisible(loading)
                    })

            viewModel.fetchData(false)

        } catch (thr : Throwable) {
            BibleLog.e(TAG, "Error creating HistoryFragment", thr);
        }

        return binding.root
    }


    override fun onNoteHeaderClicked(v: View) {
        val verse = v.tag as Verse
        val activity = activity as MainActivity?
        activity?.startReaderWithIntent(verse)
    }

    override fun onNoteIconClicked(v: View) {
        val verse = v.tag as Verse
        showNotesDialog(verse)
    }

    override fun onNoteTextClicked(v: View) {
        val verse = v.tag as Verse
        showNotesDialog(verse)
    }

    // TODO should check for changes when coming back from reader

    override fun onResume() {
        BibleLog.d(TAG, "onResume()")
        // notify the navigation drawer
        val activity = activity as MainActivity?
        activity?.onNavigationDrawerItemSelected(MainActivity.POSITION_NOTES)
        activity?.hideToolbarSpinner()
        // ActionBar
        val actionBar = (activity as AppCompatActivity?)!!.supportActionBar
        actionBar!!.setDisplayShowTitleEnabled(true)
        super.onResume()
    }

    private fun onResultChanged(result: RepoResult<List<Verse>>) {
        when (result) {
            is Success<List<Verse>> -> { updateRecyclerView(result.data) }
            is Error -> {
                onError(result.exception)
            }
        }
    }

    private fun updateRecyclerView(entries : List<Verse>) {
        adapter?.verses = entries
        adapter?.notifyDataSetChanged()
        if (entries.isNotEmpty()) {
            onDataAvailable()
        } else {
            onListEmpty()
        }
    }

    private fun onDataAvailable() {
        binding.notesRecyclerView.isVisible = true
        binding.notesMessageTv.isVisible = false
    }

    private fun onListEmpty() {
        binding.notesRecyclerView.isVisible = false
        binding.notesMessageTv.isVisible = true
        binding.notesMessageTv.text = getString(R.string.notes_empty_state_msg)
    }

    private fun onError(thr : Throwable ) {
        binding.notesMessageTv.isVisible = true
        binding.notesMessageTv.text = getString(R.string.notes_error_state_msg)
        try {
            Toast.makeText(activity, getString(R.string.notes_error_state_msg) + " : " + thr.javaClass.simpleName + ": " + thr.message, Toast.LENGTH_LONG).show()
        } catch (e : Exception) {
            BibleLog.w(TAG, "Error:", e)
        }
    }

    private fun setProgressBarVisible(visible: Boolean) {
        if (visible) {
            binding.notesRecyclerView.isVisible = false
            binding.notesMessageTv.isVisible = false
        }
        binding.notesFragmentProgressbar.isVisible = visible
    }

    private fun showNotesDialog(verse: Verse) {
        val fragment = NotesDialogFragment.instance(verse, object : OnNotesChangedListener {
            override fun onNotesChanged() {
                viewModel.fetchData(true)
            }
        })
        fragment.show(parentFragmentManager, NotesDialogFragment.TAG)
    }

}