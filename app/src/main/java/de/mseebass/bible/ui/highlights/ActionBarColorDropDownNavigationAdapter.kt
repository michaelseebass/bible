package de.mseebass.bible.ui.highlights

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckedTextView
import android.widget.ImageView
import android.widget.TextView
import de.mseebass.bible.R
import de.mseebass.bible.utils.ResourceUtils.getDisplayHighlightColor
import de.mseebass.bible.utils.ResourceUtils.getSolidHighlightColorsArray

class ActionBarColorDropDownNavigationAdapter(
        private val mContext: Context
) : ArrayAdapter<Int?>(mContext, android.R.layout.simple_spinner_dropdown_item) {

    init {
        initItemList()
    }

    private fun initItemList() {
        // first item - all colors
        add(mContext.resources.getColor(android.R.color.transparent))
        // color items
        val solidColors = getSolidHighlightColorsArray(mContext)
        for (color in solidColors) {
            add(color)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Return a view which appears in the action bar.
        var retView = convertView
        if (retView == null) {
            retView = View.inflate(context, android.R.layout.simple_spinner_dropdown_item, null)
        }
        val tv = retView as CheckedTextView?
        if (position == 0) {
            tv!!.setText(R.string.highlighted_verses_actionbar_navigation_all_colors)
        } else {
            tv!!.text = getColorName(position - 1)
        }
        return retView!!
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Return a view which appears in the drop-down list
        var retView = convertView
        if (retView == null) {
            retView = View.inflate(context, R.layout.color_drop_down_list_item, null)
        }
        val colorView = retView!!.findViewById<View>(R.id.color_drop_down_list_item_color_imageview) as ImageView
        val tv = retView!!.findViewById<View>(R.id.color_drop_down_list_item_tv) as TextView
        if (position == 0) {
            colorView.visibility = View.GONE
            tv.setText(R.string.highlighted_verses_actionbar_navigation_all_colors)
        } else {
            colorView.visibility = View.VISIBLE
            val alphaColor = getDisplayHighlightColor(getItem(position)!!)

            val bgShape = colorView.drawable as GradientDrawable
            bgShape.setColor(alphaColor)
            tv.text = getColorName(position - 1)
        }
        return retView
    }

    private fun getColorName(colorValueIndex: Int): String {
        val res = mContext.resources
        val colorNames = res.getStringArray(R.array.highlight_color_names)
        return colorNames[colorValueIndex]
    }

}