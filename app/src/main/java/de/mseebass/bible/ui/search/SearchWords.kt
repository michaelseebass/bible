package de.mseebass.bible.ui.search

class SearchWords(val query: String?) {
    fun split(): List<String> {
        val wordList = query?.split(" ")?.toList() ?: listOf()
        return wordList
    }
}