package de.mseebass.bible.ui.settings

import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import de.mseebass.bible.R
import de.mseebass.bible.utils.Settings

class SettingsFragment : PreferenceFragmentCompat(), OnSharedPreferenceChangeListener {

    // Prefrences
    private var fontScaleListPreference: ListPreference? = null
    private var themeListPreference: ListPreference? = null


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey : String?) {
        // This static call will reset default values only on the first ever read
        PreferenceManager.setDefaultValues(activity!!.baseContext, R.xml.preferences, false)
        addPreferencesFromResource(R.xml.preferences)

        // ActionBar
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        fontScaleListPreference = preferenceScreen.findPreference<Preference>(KEY_FONT_SCALE_PREFERENCE) as ListPreference?
        themeListPreference = preferenceScreen.findPreference<Preference>(KEY_THEME_PREFRENCE) as ListPreference?
    }

    override fun onResume() {
        super.onResume()
        themeListPreference?.summary = Settings.getThemeDescription(activity)
        fontScaleListPreference?.summary = Settings.getFontScaleDescription(activity)
        // Listener
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        // Listener entfernen
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        val activity = activity ?: return
        // Beschreibung anpassen
        if (key == KEY_THEME_PREFRENCE) {
            activity.application.setTheme(Settings.getThemeId(activity))
            themeListPreference?.summary = Settings.getThemeDescription(activity)
            activity.setResult(RESULT_THEME_CHANGED)
            activity.finish()
            val intent = Intent(activity, SettingsActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            startActivity(intent)
        } else if (key == KEY_FONT_SCALE_PREFERENCE) {
            fontScaleListPreference?.summary = Settings.getFontScaleDescription(activity)
            activity.setResult(RESULT_FONT_SIZE_CHANGED)
        }
    }

    companion object {
        const val KEY_FONT_SCALE_PREFERENCE = "pref_key_font_scale"
        const val KEY_THEME_PREFRENCE = "pref_key_theme"
        const val RESULT_THEME_CHANGED = 1000
        const val RESULT_FONT_SIZE_CHANGED = 1001
    }
}