package de.mseebass.bible.ui.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.repository.HistoryDatabaseRepository
import de.mseebass.bible.repository.IHistoryRepository
import de.mseebass.bible.repository.RepoResult
import de.mseebass.bible.utils.BibleLog
import kotlinx.coroutines.launch

class HistoryViewModel(appContext : Application) : AndroidViewModel(appContext) {

    private val repo : IHistoryRepository = HistoryDatabaseRepository(appContext)

    val result by lazy { MutableLiveData<RepoResult<List<HistoryEntry>>>() }

    val isLoading by lazy { MutableLiveData<Boolean>() }

    /**
     * Fetch data async from repo, updating the live data
     */
    fun fetchData() {
        try {
            fetchRepo()
        } catch (e: Exception) {
            BibleLog.e("HistoryViewModel", "Error fetching data:", e)
        }
    }


    // https://stackoverflow.com/questions/54077592/kotlin-coroutines-handle-error-and-implementation

    private fun fetchRepo() = viewModelScope.launch {
        isLoading.value = true
        val result = repo.getHistoryEntries()

        this@HistoryViewModel.result.value = result
        isLoading.value = false
    }

}