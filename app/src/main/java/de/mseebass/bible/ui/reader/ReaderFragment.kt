package de.mseebass.bible.ui.reader

import android.os.Bundle
import android.os.Parcelable
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
import de.mseebass.bible.R
import de.mseebass.bible.databinding.ReaderFragmentBinding
import de.mseebass.bible.models.Bible
import de.mseebass.bible.models.Book
import de.mseebass.bible.models.Chapter
import de.mseebass.bible.tasks.QueryBiblesTask
import de.mseebass.bible.tasks.QueryBooksTask
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.ResourceUtils.getThemedResourceId
import de.mseebass.bible.utils.Settings
import java.util.*

class ReaderFragment : BaseFragment(), QueryBiblesTask.Listener, QueryBooksTask.Listener {
    private var mTag = FRAGMENT_TRANSACTION_TAG

    // Variablen
    private var mBibles: MutableList<Bible>? = null
    private var mBibleId: Long = 1
    private var mBookIndex = 0
    private var mChapterIndex = 0
    private var mVerseIndex = -1
    private var mBookMenuIndex = 0

    lateinit var dataBinding: ReaderFragmentBinding

    // UI
    private var mMediator: TabLayoutMediator? = null
    private var mPagerAdapter: ReaderPagerAdapter? = null

    private var mAlertDialog: AlertDialog? = null

    override fun onSaveInstanceState(outState: Bundle) {
        BibleLog.d(mTag, "onSaveInstanceState()")
        outState.putLong(KEY_BIBLE_ID, mBibleId)
        outState.putInt(KEY_BOOK_INDEX, mBookIndex)
        outState.putInt(KEY_CHAPTER_INDEX, mChapterIndex)
        outState.putInt(KEY_VERSE_INDEX, mVerseIndex)
        outState.putInt("BOOK_MENU_INDEX", mBookMenuIndex)
        if (mBibles != null) {
            // Bibel-Vector
            outState.putInt("BIBLES_COUNT", mBibles!!.size)
            for (i in mBibles!!.indices) {
                outState.putParcelable("BIBLE$i", mBibles!![i])
            }
        }
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        mTag = BibleLog.getInstanceSpecificLogTag(FRAGMENT_TRANSACTION_TAG)
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)

        // Daten laden
        if (savedInstanceState != null ) {
            BibleLog.d(mTag, "restoring from savedInstanceState")
            // wiederherstellen
            mBibleId = savedInstanceState.getLong(KEY_BIBLE_ID)
            mBookIndex = savedInstanceState.getInt(KEY_BOOK_INDEX)
            mChapterIndex = savedInstanceState.getInt(KEY_CHAPTER_INDEX)
            if (savedInstanceState.containsKey(KEY_VERSE_INDEX)) {
                mVerseIndex = savedInstanceState.getInt(KEY_VERSE_INDEX)
            }
            mBookMenuIndex = savedInstanceState.getInt("BOOK_MENU_INDEX")
            if (savedInstanceState.containsKey("BIBLES_COUNT")) {
                mBibles = arrayListOf()
                // Vector
                val biblesCount = savedInstanceState.getInt("BIBLES_COUNT")
                for (i in 0 until biblesCount) {
                    mBibles?.add((savedInstanceState.getParcelable<Parcelable>("BIBLE$i") as Bible?)!!)
                }
            }
            BibleLog.w(mTag, "onCreate() - mBibles == null : " + (mBibles == null).toString())
        } else if (arguments != null) {
            BibleLog.d(mTag, "using arguments")
            // Intent auswerten
            val extras = arguments
            mBibleId = extras!!.getLong(KEY_BIBLE_ID)
            mBookIndex = extras.getInt(KEY_BOOK_INDEX)
            mChapterIndex = extras.getInt(KEY_CHAPTER_INDEX)
            mVerseIndex = extras.getInt(KEY_VERSE_INDEX)
            // books menu
            mBookMenuIndex = mBookIndex
        } else {
            BibleLog.d(mTag, "restoring from SharedPreferences")
            mBibleId = Settings.getBibleId(activity)
            mBookIndex = Settings.getBookIndex(activity)
            mChapterIndex = Settings.getChapterIndex(activity)
            mVerseIndex = Settings.getVerseIndex(activity)
            mBookMenuIndex = mBookIndex
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.reader_fragment, container, false)
        return dataBinding.root
    }

    override fun onResume() {
        // notify the navigation drawer
        val activity = activity as MainActivity?
        activity?.onNavigationDrawerItemSelected(MainActivity.POSITION_READER)
        // keep screen on
        val keepScreenOn = Settings.isKeepScreenOnEnabled(activity)
        dataBinding.pager.keepScreenOn = keepScreenOn
        if (mBibles != null) {
            setUpToolbarNavigation()
        } else if (QueryBiblesTask.isRunning()) {
            // register Listener callbacks for the allready running task
            val task = QueryBiblesTask.getInstance(getActivity())
            task.setListener(this)
            displayProgressBar()
        } else if (QueryBooksTask.isInstanceRunning(mBibleId)) {
            // register Listener callbacks for the allready running task
            val task = QueryBooksTask.getInstance(getActivity(), mBibleId)
            task.setListener(this)
            displayProgressBar()
        } else {
            QueryBiblesTask.getInstance(getActivity()).apply {
                setListener(this@ReaderFragment)
                if (!QueryBiblesTask.isRunning()) {
                    execute()
                }
            }
        }
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        dataBinding.pager.keepScreenOn = false
        // SharedPreferences
        Settings.saveBibleId(mBibleId, activity)
        Settings.saveBookIndex(mBookIndex, activity)
        Settings.saveChapterIndex(mChapterIndex, activity)
        Settings.saveVerseIndex(mVerseIndex, activity)

        QueryBiblesTask.getExistingInstance()?.setListener(null)
        QueryBooksTask.getExistingInstance(mBibleId)?.setListener(null)
    }

    override fun onDestroyView() {
        try {
            dataBinding.pager.unregisterOnPageChangeCallback(onPageChangeCallback)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        super.onDestroyView()
    }

    fun notifyChapterLoaded(chapter: Chapter) {
        try {
            if (chapter.number - 1 == mChapterIndex) {
                val selectedVerse = chapter.getVerse(if (mVerseIndex < 0) 0 else mVerseIndex)
                val mainActivity = activity as MainActivity?
                mainActivity!!.saveHistory(mBibleId, selectedVerse)
            }
        } catch (e: Exception) {
            BibleLog.e(mTag, "Failed to save History entry", e)
        }
    }

    override fun onQueryBiblesTaskStarted() {
        displayProgressBar()
    }

    override fun onQueryBiblesTaskFinished(results: MutableList<Bible>) {
        mBibles = results
        if (mBibles == null || mBibles!!.size == 0) {
            displayErrorMessage(R.string.reader_fragment_error_loading_bibles)
        } else {
            setUpToolbarNavigation()
        }
    }

    override fun onQueryBooksTaskStarted() {
        displayProgressBar()
    }

    override fun onQueryBooksTaskFinished(results: Vector<Book>?) {
        if (results == null || results.size == 0) {
            displayErrorMessage(R.string.reader_fragment_error_loading_books)
        } else {
            // get the user selected bible
            val bible = selectedBible
            if (bible == null) {
                // Fehlermeldung
                displayErrorMessage(R.string.reader_fragment_error_loading_books)
            } else {
                // Ergebnis �bernehmen
                bible.books = results
                // anzeigen
                BibleLog.d(mTag, "onQueryBooksTaskFinished() - calling setUpViewPager()")
                setUpViewPager() //onQueryBooksTaskFinished
            }
        }
    }

    private fun setUpToolbarNavigation() {
        // Toolbar
        val appCompatActivity = activity as AppCompatActivity?
        if (appCompatActivity == null) {
            BibleLog.e(mTag, "getActivity() == null")
            return
        }
        val actionBar = appCompatActivity.supportActionBar
                ?: throw IllegalStateException("getSupportActionBar() returned null")
        actionBar.setDisplayShowTitleEnabled(false)
        // Spinner
        val spinner = appCompatActivity.findViewById<View>(R.id.main_activity_toolbar_spinner) as Spinner
        // List
        val items = arrayOfNulls<String>(mBibles!!.size)
        for (i in mBibles!!.indices) {
            items[i] = mBibles!![i].name
        }
        // Adapter
        val spinnerAdapter = ArrayAdapter<CharSequence?>(actionBar.themedContext, android.R.layout.simple_spinner_dropdown_item, items)
        //        adapter.setDropDownViewTheme(getActivity().getTheme());
        // Layout for drop-down list
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = spinnerAdapter
        spinner.visibility = View.VISIBLE
        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                BibleLog.d(mTag, "onNavigationItemSelected() - itemPosition $position")
                if (mBibles == null) {
                    return
                }
                val selectedBible = mBibles!![position]
                if (mBibleId != selectedBible.id) {
                    mBibleId = selectedBible.id
                    Settings.saveBibleId(mBibleId, activity)
                }
                if (selectedBible.books.size == 0) {
                    // laden
                    BibleLog.d(mTag, "onNavigationItemSelected() - starting QueryBooksTask")
                    val task = QueryBooksTask.getInstance(activity, mBibleId)
                    task.setListener(this@ReaderFragment)
                    if (!task.isRunning) {
                        task.execute()
                    }
                } else {
                    setUpViewPager() // spinner
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        spinner.setSelection(selectedBibleIndex)
    }

    // Buch
    fun showBuchMenu() {
        if (mAlertDialog != null) {
            mAlertDialog!!.dismiss()
        }
        val selectedBible = selectedBible
        if (selectedBible == null || selectedBible.books.size == 0 || activity == null) {
            return
        }
        val books = selectedBible.books
        val items = arrayOfNulls<CharSequence>(books.size)
        for (i in books.indices) {
            items[i] = books[i].name.toString()
        }
        val style = getThemedResourceId(R.attr.bibleAlertDialogStyle, activity!!)
        val alert = AlertDialog.Builder(activity!!, style)
        alert.setTitle(R.string.reader_book_dialog_title)
        alert.setIcon(R.drawable.ic_launcher_bible_small)
        alert.setSingleChoiceItems(items, mBookMenuIndex) { dialog, item ->
            mBookMenuIndex = item
            if (mBookIndex != mBookMenuIndex) {
                mBookIndex = mBookMenuIndex
                mChapterIndex = 0
                mVerseIndex = -1
                setUpViewPager() // showBuchMenu
            }
            if (mAlertDialog != null) {
                mAlertDialog!!.dismiss()
            }
            showKapitelMenu(books[mBookIndex].size)
        }
        alert.setNegativeButton(R.string.dialog_cancel, null)
        mAlertDialog = alert.show()
    }

    // Kapitel
    fun showKapitelMenu(chapterSize: Int) {
        if (mAlertDialog != null) {
            mAlertDialog!!.dismiss()
        }
        val items = arrayOfNulls<CharSequence>(chapterSize)
        for (i in 0 until chapterSize) {
            items[i] = (i + 1).toString()
        }
        val style = getThemedResourceId(R.attr.bibleAlertDialogStyle, activity!!)
        val alert = AlertDialog.Builder(activity!!, style)
        alert.setTitle(R.string.reader_chapter_dialog_title)
        alert.setIcon(R.drawable.ic_launcher_bible_small)
        alert.setSingleChoiceItems(items, mChapterIndex) { dialog, item ->
            if (mChapterIndex != item) {
                mChapterIndex = item
                scrollToChapter()
            }
            if (mAlertDialog != null) {
                mAlertDialog!!.dismiss()
            }
            //saveHistory();//TODO
        }
        alert.setNegativeButton(R.string.dialog_cancel) { dialog, whichButton -> }
        mAlertDialog = alert.show()
    }

    private fun setUpViewPager() {
        BibleLog.d(mTag, "setUpViewPager() - mVerseIndex = $mVerseIndex")
        // make the ViewPager visible
        displayViewPager()
        // retrieve the selected bible
        val selectedBible = selectedBible
        // validation
        if (selectedBible == null || selectedBible.books.isEmpty() || activity == null) {
            // leave this method
            return
        }
        val (_, _, name, size) = selectedBible.getBook(mBookIndex) // TODO was ist das???
        // workaround for double-trigger onNavigationItemSelected()
        (dataBinding.pager.adapter as ReaderPagerAdapter?)?.let {
            mPagerAdapter = it
            if (it.bookNumber == mBookIndex + 1 && it.selectedVerseIndexOnCurrentItem == mVerseIndex) {
                if (it.bibleId == mBibleId) {
                    // no need to set a new adapter
                    return
                } else {
                    val currentPageFragment = it.getRegisteredFragment(dataBinding.pager.currentItem) as ReaderPageFragment?
                    currentPageFragment?.let { mVerseIndex = it.firstVisibleVerseIndex }
                }
            }
        }

        // new adapter
        val bibleShortName = selectedBible.shortName
        mPagerAdapter = ReaderPagerAdapter(this, mBibleId, bibleShortName!!, mBookIndex + 1, size)
        dataBinding.pager.adapter = mPagerAdapter
        // scroll to chapter
        dataBinding.pager.setCurrentItem(mChapterIndex, false)
        // scroll to verse
        mPagerAdapter!!.setSelectedVerse(mChapterIndex, mVerseIndex)
        // listen for page scroll events
        dataBinding.pager.registerOnPageChangeCallback(onPageChangeCallback)

//        new TabLayoutMediator(mTabLayout, mPager, (tab, position) ->
//            tab.setText(getSelectedBible().getBook(mBookIndex).getName() + " " + (position + 1));
//        ).attach();

        mMediator?.detach() // make sure we dont add callbacks with each change of bible translation
        mMediator = TabLayoutMediator(dataBinding.tabLayout, dataBinding.pager, TabConfigurationStrategy { tab: TabLayout.Tab, position: Int ->
            var title: String? = null
            if (name != null) title = name + " " + (position + 1)
            tab.text = title
        })
        mMediator!!.attach()
        dataBinding.pager.adapter?.notifyDataSetChanged()
    }

    private val onPageChangeCallback = object: OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            if (position != mChapterIndex) {
                // user scrolled to other chapter, so save this chapters index
                mChapterIndex = position
                mVerseIndex = -1
                // update the adapter
                mPagerAdapter?.setChapterIndex(mChapterIndex)
                (activity as? MainActivity)?.finishActionMode()
            }
        }
    }

    private fun scrollToChapter() {
        dataBinding.pager.setCurrentItem(mChapterIndex, true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        // inflate the menu xml
        inflater.inflate(R.menu.menu_reader, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.reader_menu_item_open -> {
                showBuchMenu()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // nach passender Bibel in Vector suchen
    private val selectedBibleIndex: Int
        private get() {
            // nach passender Bibel in Vector suchen
            for (i in mBibles!!.indices) {
                if (mBibles!![i].id == mBibleId) {
                    return i
                }
            }
            return 0
        }

    // nach passender Bibel in Vector suchen
    private val selectedBible: Bible?
        private get() {
            if (mBibles != null) {
                // nach passender Bibel in Vector suchen
                for (i in mBibles!!.indices) {
                    if (mBibles!![i].id == mBibleId) {
                        return mBibles!![i]
                    }
                }
            }
            return null
        }

    private fun displayViewPager() {
        dataBinding.pager.isVisible = true
        dataBinding.tabLayout.isVisible = true
        dataBinding.errorMessageTv.isVisible = false
        dataBinding.progressbar.isVisible = false
    }

    private fun displayProgressBar() {
        dataBinding.pager.isVisible = false
        dataBinding.tabLayout.isVisible = false
        dataBinding.errorMessageTv.isVisible = false
        dataBinding.progressbar.isVisible = true
    }

    private fun displayErrorMessage(stringId: Int) {
        dataBinding.pager.isVisible = false
        dataBinding.tabLayout.isVisible = false
        dataBinding.progressbar.isVisible = false
        dataBinding.errorMessageTv.apply {
            isVisible = true
            setText(stringId)
        }
    }

    companion object {
        // Log-TAG
        const val FRAGMENT_TRANSACTION_TAG = "ReaderFragment"

        // KEYS
        const val KEY_BIBLE_ID = "KEY_BIBLE_ID"
        const val KEY_BOOK_INDEX = "KEY_BOOK_INDEX"
        const val KEY_CHAPTER_INDEX = "KEY_CHAPTER_INDEX"
        const val KEY_VERSE_INDEX = "KEY_VERSE_INDEX"
    }
}