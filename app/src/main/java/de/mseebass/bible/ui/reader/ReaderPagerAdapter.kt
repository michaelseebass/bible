package de.mseebass.bible.ui.reader

import android.util.SparseArray
import androidx.core.util.containsKey
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import de.mseebass.bible.utils.BibleLog

class ReaderPagerAdapter(
        fragment: Fragment,
        val bibleId: Long,
        private val mBibleShortName: String,
        val bookNumber: Int,
        private val mSize: Int
) : FragmentStateAdapter(fragment) {

    private var chapterIndex = -1
    var selectedVerseIndexOnCurrentItem = -1
        private set
    private val mRegisteredFragments = SparseArray<Fragment>()


    fun setSelectedVerse(chapterIndex: Int, verseIndex: Int) {
        BibleLog.i(TAG, "setting selected verse for chapter-index $chapterIndex verseindex: $verseIndex")
        this.chapterIndex = chapterIndex
        selectedVerseIndexOnCurrentItem = verseIndex
    }

    fun setChapterIndex(chapterIndex: Int) {
        this.chapterIndex = chapterIndex
        val f = getRegisteredFragment(this.chapterIndex) as ReaderPageFragment?
        f?.notifyParent()
    }

    override fun createFragment(position: Int): Fragment {
        val fragment = ReaderPageFragment()
        fragment.init(bibleId, mBibleShortName, bookNumber, position + 1)
        mRegisteredFragments.put(position, fragment)
        val isSelectedChapter = chapterIndex == position
        if (isSelectedChapter && selectedVerseIndexOnCurrentItem > -1) {
            fragment.setSelectedVerse(selectedVerseIndexOnCurrentItem)
        }
        return fragment
    }

    override fun getItemCount(): Int {
        return mSize
    }

    fun getRegisteredFragment(position: Int): Fragment? {
        return if (mRegisteredFragments.containsKey(position)) {
            mRegisteredFragments[position]
        } else null
    }

    companion object {
        const val TAG = "ReaderPagerAdapter"
    }

}