package de.mseebass.bible.ui.search

import java.util.ArrayList

interface OnBooksSelectedListener {

    fun onBooksSelected(selectedBookNumbers: ArrayList<Int>)

}