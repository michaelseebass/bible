package de.mseebass.bible.ui.notes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import de.mseebass.bible.R
import de.mseebass.bible.databinding.NotesListItemBinding
import de.mseebass.bible.models.Verse

class NotesRecyclerViewAdapter(
        var verses: List<Verse>,
        val callback: NotesClickListener
) : RecyclerView.Adapter<NotesRecyclerViewAdapter.NotesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : NotesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<NotesListItemBinding>(inflater, R.layout.notes_list_item, parent, false)
        return NotesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        holder.binding.verse = verses[position]
        holder.binding.listener = callback
    }

    override fun getItemCount()= verses.size

    class NotesViewHolder(var binding: NotesListItemBinding): RecyclerView.ViewHolder(binding.root)

}