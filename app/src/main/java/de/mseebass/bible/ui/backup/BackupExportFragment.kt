package de.mseebass.bible.ui.backup

import android.content.Intent
import androidx.core.content.FileProvider
import de.mseebass.bible.R
import de.mseebass.bible.databinding.BackupFragmentBinding
import de.mseebass.bible.tasks.ExportBookmarksDbFileTask
import de.mseebass.bible.tasks.ExportBookmarksDbFileTask.ExportBookmarksDbFileTaskResultCode
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.BookmarkUtils
import de.mseebass.bible.utils.ResourceUtils.getThemedResourceId
import de.mseebass.bible.utils.StoragePermissionManager
import java.io.File
import java.io.IOException

class BackupExportFragment : BackupBaseFragment() {

    override fun onCreateViewInternal(binding: BackupFragmentBinding, permissionManager: StoragePermissionManager) {
        binding.backupFragmentBtn.apply {
            setText(R.string.backup_activity_btn_export_text)
            setOnClickListener {
                permissionManager.onStoragePermissionRequired(this@BackupExportFragment)
            }
        }
        // Button anzeigen
        super.displayBtn()

        super.setImage(getThemedResourceId(R.attr.iconExportLarge, activity!!))
        super.setMsg(R.string.backup_activity_msg_export)
        super.setCardText(R.string.backup_activity_export_storage_permission_explanation_primary)
    }

    override fun doAction() {
        if (activity == null) {
            BibleLog.w(TAG, "getActivity() == null")
            return
        }
        // we copy the file to external storage so that it can be accessed(for exporting/share)
        ExportBookmarksDbFileTask.getInstance(getAppContext()).apply {
            setListener(mExportBookmarksDbFileTaskListener)
            if (!ExportBookmarksDbFileTask.isRunning()) {
                execute()
            }
        }
    }

    private val mExportBookmarksDbFileTaskListener: ExportBookmarksDbFileTask.Listener = object : ExportBookmarksDbFileTask.Listener {
        override fun onExportBookmarksDbFileTaskStarted() {
            displayProgressBar(-1)
        }

        override fun onExportBookmarksDbFileTaskFinished(result: ExportBookmarksDbFileTask.Result) {
            BibleLog.d(TAG, "onExportBookmarksDbFileTaskFinished() called with result = [$result], result = [$result]")
            var exceptionString = ""
            if (result.exception != null) {
                exceptionString += " (" + result.exception.javaClass.simpleName + ": " + result.exception.message + ")"
            }
            when (result.resultCode) {
                ExportBookmarksDbFileTaskResultCode.EXTERNAL_DIR_NOT_EXISTING -> {
                    val msg = getString(R.string.backup_activity_export_msg_failed_external_dir) + exceptionString
                    displayStatusMsg(msg, R.color.color_error)
                }
                ExportBookmarksDbFileTaskResultCode.RESULT_UNAVAILABLE -> displayStatusMsg(getString(R.string.backup_activity_export_msg_failed) + exceptionString, R.color.color_error)
                ExportBookmarksDbFileTaskResultCode.SUCCESS -> {
                    displayStatusMsg(getString(R.string.backup_activity_export_msg_success), R.color.color_success)
                    // now that the file is copied to external storage, we can share/export it
                    share(result.file)
                }
                else -> throw IllegalArgumentException("Unknown result code: " + result.resultCode.toString())
            }
            displayBtn()
        }
    }
    // http://stackoverflow.com/questions/29376072/share-sqlite-database-from-android-app-without-intermediate-copy
    // http://stackoverflow.com/questions/18548255/android-send-xml-file-doesnt-send-attachment/18548685#18548685
    /**
     * Use a share intent to export our bookmarks database file
     */
    private fun share(file: File) {
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "application/octet-stream"
            val uri = FileProvider.getUriForFile(getAppContext(), getAppContext().packageName + ".fileprovider", file)
            putExtra(Intent.EXTRA_STREAM, uri)
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        }
        startActivityForResult(Intent.createChooser(intent, getString(R.string.backup_export_intent)), BackupActivity.SHARE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        BibleLog.d(TAG, "onActivityResult() called with requestCode = [$requestCode], resultCode = [$resultCode], data = [$data]")
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            BackupActivity.SHARE_REQUEST_CODE -> try {
                BookmarkUtils.getBackupFile()
                // call to getBackupFile() already deleted File
            } catch (e: IOException) {
                e.printStackTrace()
            }
            else -> {}
        }
    }

    companion object {
        /** Log-TAG  */
        val TAG = BackupExportFragment::class.java.simpleName
    }
}