package de.mseebass.bible.ui.bookmarks

import android.app.Dialog
import android.os.Bundle
import android.util.TypedValue
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import de.mseebass.bible.R
import de.mseebass.bible.interfaces.OnLabelSelectedListener
import de.mseebass.bible.providers.BibleProvider.Bookmarks

class BookMarksNewGroupDialogFragment : DialogFragment() {

    private var mListener: OnLabelSelectedListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alert = AlertDialog.Builder(activity!!)
        alert.apply {
            setTitle(R.string.bookmarks_dialog_title)
            setIcon(R.drawable.ic_launcher_bible_small)
            // Content-Layout
            val layout = LinearLayout(activity).apply {
                orientation = LinearLayout.VERTICAL
                // Padding
                val padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
                setPadding(padding, padding, padding, padding)
            }
            // EditText
            val et = EditText(activity).apply {
                setHint(R.string.bookmarks_new_group_dialog_hint)
                setSingleLine()
            }
            layout.addView(et)

            // set layout as Alert View
            setView(layout)
            setPositiveButton(R.string.dialog_ok) { arg0, arg1 ->
                if (et.text.toString() == Bookmarks.LABEL_GENERAL) {
                    Toast.makeText(activity, R.string.bookmarks_group_name_reserved, Toast.LENGTH_LONG).show()
                } else {
                    mListener?.onLabelSelected(et.text.toString())
                }
            }
            setNegativeButton(R.string.dialog_cancel, null)
        }

        return alert.create()
    }

    companion object {
        private const val PROGRESS_DIALOG_FRAGMENT_TAG = "bookmark_new_group_dialog_fragment"

        private var sDialog // TODO avoid memory leak
                : BookMarksNewGroupDialogFragment? = null

        @JvmStatic
        fun showDialog(listener: OnLabelSelectedListener?, fm: FragmentManager?) {
            sDialog = BookMarksNewGroupDialogFragment()
            sDialog?.mListener = listener
            sDialog?.show(fm!!, PROGRESS_DIALOG_FRAGMENT_TAG)
        }

        fun dismissDialog() {
            sDialog?.dismiss()
        }
    }
}