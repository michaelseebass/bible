package de.mseebass.bible.ui.backup

import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.Toast
import de.mseebass.bible.R
import de.mseebass.bible.databinding.BackupFragmentBinding
import de.mseebass.bible.models.Verse
import de.mseebass.bible.tasks.ImportBookmarksDbFileTask
import de.mseebass.bible.ui.backup.BackupImportFragment
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.EncodingUtils.encodeBase64ToString
import de.mseebass.bible.utils.EncodingUtils.getHexEncodedMD5
import de.mseebass.bible.utils.FilesUtils
import de.mseebass.bible.utils.ResourceUtils.getThemedResourceId
import de.mseebass.bible.utils.StoragePermissionManager
import java.io.*

class BackupImportFragment : BackupBaseFragment() {

    private var mTempFile: File? = null

    override fun onCreateViewInternal(binding: BackupFragmentBinding, permissionManager: StoragePermissionManager) {
        binding.backupFragmentBtn.apply {
            setText(R.string.backup_activity_btn_import_text)
            setOnClickListener {
                permissionManager.onStoragePermissionRequired(this@BackupImportFragment)
            }
        }
        // Button anzeigen
        super.displayBtn()

        super.setImage(getThemedResourceId(R.attr.iconImportLarge, activity!!))
        super.setMsg(R.string.backup_activity_msg_import)
        super.setCardText(R.string.backup_activity_import_storage_permission_explanation_primary)
    }

    override fun doAction() {
        // choose a File to import. This will be a previously exported bookmarks database file
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "file/*"
        startActivityForResult(intent, BackupActivity.CHOOSE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            BackupActivity.CHOOSE_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                // User choose, which file he wants to import
                //String filePath = data.getData().getPath();
                val uri = data!!.data
                //File myFile = new File(uri.getPath());
                //String filePath = myFile.getAbsolutePath();
                try {
                    val tempDataDirectory = getAppContext().cacheDir
                    mTempFile = File(tempDataDirectory, "import_db")
                    BibleLog.i(TAG, "Importing backup: $mTempFile")
                    val `is` = getAppContext().contentResolver.openInputStream(uri!!)
                    copyFile(`is`, mTempFile!!)
                    val md5Str = getHexEncodedMD5(mTempFile!!)
                    BibleLog.i(TAG, "After importing backup: MD5: $md5Str")
                    BibleLog.i(TAG, "After importing backup: Size: " + FilesUtils.getFileSizeString(mTempFile, true))

                    // Encode the Path
                    val encodedPath = encodeBase64ToString(mTempFile!!.absolutePath)
                    // Start the Import-Worker-Task
                    // TODO in Task, Progress
                    val task = ImportBookmarksDbFileTask.getInstance(getAppContext(), encodedPath)
                    task.setListener(mTaskListener)
                    if (!ImportBookmarksDbFileTask.isRunning()) {
                        task.execute()
                    }
                } catch (e: Exception) {
                    Toast.makeText(activity, getString(R.string.backup_import_error_general) + " " + e.javaClass + ": " + e.message, Toast.LENGTH_LONG).show()
                }
            }
            else -> { }
        }
    }

    // TODO auslagern
    private fun copyFile(inputStream: InputStream?, destinationFile: File) {
        var bis: BufferedInputStream? = null
        var bos: BufferedOutputStream? = null
        try {
            bis = BufferedInputStream(inputStream)
            bos = BufferedOutputStream(FileOutputStream(destinationFile, false))
            var length = 0
            val buf = ByteArray(1024)
            bis.read(buf)
            do {
                length += 1024
                bos.write(buf)
            } while (bis.read(buf) != -1)
            BibleLog.i(TAG, "Copied: $length")
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                bis?.close()
                bos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    //https://developer.android.com/guide/topics/providers/document-provider.html
    private val mTaskListener: ImportBookmarksDbFileTask.Listener = object : ImportBookmarksDbFileTask.Listener {
        override fun onImportBookmarksDbFileTaskStarted() {
            displayProgressBar(-1)
        }

        override fun onImportBookmarksDbFileTaskProgress(progressType: Int, percent: Int) {
            displayProgressBar(percent)
        }

        override fun onImportBookmarksDbFileTaskFinished(result: ImportBookmarksDbFileTask.Result) {
            var msg: String
            val colorResId: Int
            if (result.exception != null) {
                // something "top-level" went wrong
                msg = (getString(R.string.backup_import_error_general)
                        + " " + result.exception.javaClass.simpleName + ": " + result.exception.message)
                colorResId = R.color.color_error
            } else if (result.errorVerses.size > 0) {
                // error on specific Verses/Bookmarks occured
                msg = result.errorVerses.size.toString() + " " + getString(R.string.backup_import_error_specific)
                val verses: Set<Verse> = result.errorVerses.keys
                val verseArray = verses.toTypedArray()
                val e = result.errorVerses[verseArray[0]]
                msg += " - " + e!!.javaClass.simpleName + ": " + e.message
                if (verseArray.size > 1) {
                    msg += " ( + " + (verseArray.size - 1) + " " + getString(R.string.backup_import_error_specific_suffix) + ") "
                }
                colorResId = R.color.color_error
            } else {
                // No errors, we are good to go
                msg = result.successCount.toString() + "/" + result.totalCount + " " + getString(R.string.backup_import_success)
                colorResId = R.color.color_success
            }
            displayStatusMsg(msg, colorResId)
            displayBtn()
            deleteTempFile()
            activity!!.setResult(RESULT_IMPORTED)
        }
    }

    /**
     * Deletes the temporary imported database file
     */
    private fun deleteTempFile() {
        try {
            if (mTempFile != null && mTempFile!!.exists()) {
                val deleted = mTempFile!!.delete()
                BibleLog.d(TAG, "Deleted temp import file: $deleted")
            }
        } catch (e: Exception) {
            BibleLog.w(TAG, "Failed to delete temp file: ", e)
        }
    }

    companion object {
        /** Log-TAG  */
        val TAG = BackupImportFragment::class.java.simpleName
        const val RESULT_IMPORTED = 707
    }
}