package de.mseebass.bible.ui.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.TextUtils
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import de.mseebass.bible.R
import de.mseebass.bible.databinding.SearchFragmentBinding
import de.mseebass.bible.models.Bible
import de.mseebass.bible.models.Verse
import de.mseebass.bible.providers.BibleProviderUtils
import de.mseebass.bible.tasks.SearchTask
import de.mseebass.bible.ui.MainActivity
import de.mseebass.bible.ui.misc.BaseFragment
import de.mseebass.bible.ui.reader.ReaderListAdapter
import de.mseebass.bible.ui.search.SearchSettingsDialogFragment.Companion.showDialog
import de.mseebass.bible.utils.BibleLog
import de.mseebass.bible.utils.Settings
import java.util.*

class SearchFragment : BaseFragment(), SearchTask.Listener, OnBooksSelectedListener {
    // UI
    private lateinit var binding: SearchFragmentBinding
    private var searchView: SearchView? = null

    // Variables
    private var query: String? = null
    private var results: Vector<Verse>? = null
    private var mBible: Bible? = null
    private var mLastDbQueryTime: Long = -1
    private var selectedBookNumbers = arrayListOf<Int>()


    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(KEY_QUERRY, query)
        if (results != null) {
            outState.putInt(KEY_RESULT_COUNT, results!!.size)
            for (i in results!!.indices) {
                outState.putParcelable(KEY_RESULT_PREFIX + i, results!![i])
            }
        }
        outState.putInt(KEY_SELECTED_BOOKS_COUNT, selectedBookNumbers.size)
        for (i in selectedBookNumbers.indices) {
            outState.putInt(KEY_SELECTED_BOOK_PREFIX + i, selectedBookNumbers[i])
        }
        outState.putParcelable(KEY_BIBLE, mBible)
        outState.putLong(KEY_LAST_DB_QUERY_TIME, mLastDbQueryTime)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        // load data
        if (savedInstanceState != null) {
            query = savedInstanceState.getString(KEY_QUERRY)
            if (savedInstanceState.containsKey(KEY_RESULT_COUNT)) {
                val resultCount = savedInstanceState.getInt(KEY_RESULT_COUNT)
                results = Vector()
                for (i in 0 until resultCount) {
                    results!!.add(savedInstanceState.getParcelable<Parcelable>(KEY_RESULT_PREFIX + i) as Verse?)
                }
            }
            if (savedInstanceState.containsKey(KEY_SELECTED_BOOKS_COUNT)) {
                val selectedBooksCount = savedInstanceState.getInt(KEY_SELECTED_BOOKS_COUNT)
                selectedBookNumbers.clear()
                for (i in 0 until selectedBooksCount) {
                    selectedBookNumbers.add(savedInstanceState.getInt(KEY_SELECTED_BOOK_PREFIX + i))
                }
            }
            mBible = savedInstanceState.getParcelable(KEY_BIBLE)
            mLastDbQueryTime = savedInstanceState.getLong(KEY_LAST_DB_QUERY_TIME)
        } else if (arguments != null) {
            BibleLog.d(TAG, "using Arguments")
            query = arguments!!.getString(SearchManager.QUERY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.search_fragment, container, false)
        try {
            if (mBible == null) {
                val bibleId = Settings.getBibleId(activity)
                mBible = BibleProviderUtils.getBible(bibleId, activity, true) // TODO should be executed from a background thread
                val books = BibleProviderUtils.queryBooks(bibleId, activity)
                mBible?.books = books
            }
            // if no special books have been selected for search filtering, search in all books by default
            if (selectedBookNumbers.isEmpty()) {
                for ((_, number) in mBible!!.books) {
                    selectedBookNumbers.add(number)
                }
            }

            // check if the database has been updated (after restoring)
            if (!Settings.areQueriedBookmarksValid(activity, mLastDbQueryTime)) {
                // Database edited - invalidate previously queried results
                results = null
            }
            if (SearchTask.isRunning()) {
                displayProgressBar()
            } else if (results != null && mBible != null) {
                displayResults()
            } else if (query != null) {
                executeSearch()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            try {
                showToastLong("Error: " + e.javaClass + ": " + e.message) // TODO
            } catch (ignored: Exception) {
            }
        }
        // Rückgabewert
        return binding.root
    }

    override fun onResume() {
        // notify the navigation drawer
        val activity = activity as MainActivity?
        activity?.onNavigationDrawerItemSelected(MainActivity.POSITION_SEARCH)
        // set listener for the filter settings dialog
        val dialog = childFragmentManager.findFragmentByTag(SearchSettingsDialogFragment.TAG) as SearchSettingsDialogFragment?
        dialog?.setListener(this)
        super.onResume()
    }

    fun setQuery(query: String?) {
        this.query = query
    }

    fun executeSearch() {
        if (activity == null) {
            BibleLog.e(TAG, "getActivity() returns null while performing executeSearch()")
            return
        }
        if (mBible == null) {
            displayErrorMessage()
            return
        }
        if (!SearchTask.isRunning()) {
            SearchTask.getInstance(activity, mBible!!.id, query, selectedBookNumbers).apply {
                setListener(this@SearchFragment)
                execute()
            }
        } else {
            BibleLog.w(TAG, "SearchTask already running")
        }
    }

    override fun onSearchTaskStarted() {
        displayProgressBar()
    }

    override fun onSearchTaskFinished(results: Vector<Verse>?) {
        this.results = results
        if (this.results == null && mBible != null) {
            displayErrorMessage()
        } else {
            mLastDbQueryTime = System.currentTimeMillis()
            displayResults()
        }
    }

    private fun displayProgressBar() {
        binding.searchErrorTv.isVisible = false
        binding.searchListview.isVisible = false
        binding.searchResultsTv.isVisible = false
        binding.searchProgressbar.isVisible = true
    }

    private fun displayResults() {
        if (mBible == null) {
            displayErrorMessage() // TODO adjust string
            return
        }
        // Empty-State-TextViews
        binding.searchErrorTv.isVisible = false
        binding.searchProgressbar.isVisible = false
        // number of results
        binding.searchResultsTv.apply {
            isVisible = true
            text = results!!.size.toString() + " " + getText(R.string.search_results)
        }
        // ListView + FastScroll
        binding.searchListview.apply {
            isVisible = true
            isFastScrollEnabled = true
            isFastScrollAlwaysVisible = true
            // Adapter
            val adapter = ReaderListAdapter((activity as MainActivity?)!!, results!!, mBible!!.shortName!!)
            adapter.setSearchQuery(SearchWords(query).split())
            binding.searchListview.adapter = adapter
        }
        // remove focus from the SearchView, so the user doesn't have to press "Back" twice
        searchView?.clearFocus()

    }

    private fun displayErrorMessage() {
        binding.searchErrorTv.isVisible = true
        binding.searchListview.isVisible = false
        binding.searchResultsTv.isVisible = false
        binding.searchProgressbar.isVisible = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_search, menu)
        val searchMenuItem = menu.findItem(R.id.search_menu_item_search)
        searchView = searchMenuItem.actionView as SearchView
        searchView?.apply {
            queryHint = getText(R.string.search_hint)
            val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
            setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
            setQuery(query, false)
            if (results == null || results!!.size == 0) {
                MenuItemCompat.expandActionView(searchMenuItem)
            }
            setOnQueryTextFocusChangeListener { view, queryTextFocused ->
                if (!queryTextFocused) {
                    searchMenuItem.collapseActionView()
                    setQuery(null, false)
                }
            }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search_menu_item_settings -> {
                showDialog(mBible!!.books, selectedBookNumbers, this, childFragmentManager)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBooksSelected(selectedBookNumbers: ArrayList<Int>) {
        this.selectedBookNumbers = selectedBookNumbers
        // if user searched before search again with the new filter settings
        if (!TextUtils.isEmpty(query)) {
            executeSearch()
        }
    }

    companion object {
        // Log-TAG
        const val TAG = "SearchFragment"
        private const val KEY_QUERRY = "QUERRY"
        private const val KEY_RESULT_COUNT = "RESULT_COUNT"
        private const val KEY_RESULT_PREFIX = "RESULT_"
        private const val KEY_SELECTED_BOOKS_COUNT = "SELECTED_BOOKS_COUNT"
        private const val KEY_SELECTED_BOOK_PREFIX = "SELECTED_BOOK_"
        private const val KEY_BIBLE = "BIBLE"
        private const val KEY_LAST_DB_QUERY_TIME = "KEY_LAST_DB_QUERY_TIME"
    }
}