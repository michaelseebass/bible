package de.mseebass.bible.utils;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BookmarkUtilsTest {

    @Test
    public void testGetBackupFileNameUS() {
        // input
        Calendar cal = Calendar.getInstance();
        cal.set(2020, 5, 25, 12, 30, 10);
        Date date = new Date(cal.getTimeInMillis());
        Locale locale = Locale.US;
        // do
        String str = BookmarkUtils.getBackupFileName(date, locale);
        // check
        assertNotNull(str);
        assertEquals("bible_backup_Jun_25_2020_12_30_10_PM", str);
    }

    @Test
    public void testGetBackupFileNameDE() {
        // input
        Calendar cal = Calendar.getInstance();
        cal.set(2020, 5, 25, 12, 30, 10);
        Date date = new Date(cal.getTimeInMillis());
        Locale locale = Locale.GERMAN;
        // do
        String str = BookmarkUtils.getBackupFileName(date, locale);
        // check
        assertNotNull(str);
        assertEquals("bible_backup_25_06_2020_12_30_10", str);
    }

//    @Test
//    public void setBookMarkedVerses() {
//    }
//
//    @Test
//    public void getBackupFile() {
//    }
//
//    @Test
//    public void copyFile() {
//    }
}