package de.mseebass.bible.utils

import com.google.common.truth.Truth
import de.mseebass.bible.ui.search.QueryRegion
import org.junit.Test

class SearchUiUtilsTest {

    @Test
    fun testRegions() {
        testIntersect(5, 7, 9, 15, false)

        testIntersect(5, 7, 8, 15, true)

        testIntersect(5, 7, 7, 15, true)

        testIntersect(8, 18, 4, 6, false)

        testIntersect(8, 18, 4, 7, true)

        testIntersect(8, 18, 4, 8, true)


        testIntersect(5, 10, 8, 15, true)

        testIntersect(8, 18, 4, 10, true)
    }

    private fun testIntersect(start1: Int, end1: Int, start2: Int, end2: Int, expected: Boolean) {
        val region1 = QueryRegion(start1, end1)
        val region2 = QueryRegion(start2, end2)
        val intersects = region1.intersectsOrTouches(region2)
        Truth.assertThat(intersects).isEqualTo(expected)
    }

    @Test
    fun testAddRegion() {
        testAdd(0, 4, 2, 6, 0, 6)
        testAdd(3, 12, 1, 5, 1, 12)
    }

    private fun testAdd(start1: Int, end1: Int, start2: Int, end2: Int, expectedStart: Int, expectedEnd: Int) {
        val region1 = QueryRegion(start1, end1)
        val region2 = QueryRegion(start2, end2)
        val newRegion = region1.add(region2)
        Truth.assertThat(newRegion.start).isEqualTo(expectedStart)
        Truth.assertThat(newRegion.end).isEqualTo(expectedEnd)
    }
}