package de.mseebass.bible

import de.mseebass.bible.logic.HistoryEntryComparatorImpl
import de.mseebass.bible.logic.IHistoryEntryComparator
import de.mseebass.bible.misc.getHistoryEntry
import de.mseebass.bible.misc.mockLogcat
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class HistoryEntryComparatorTest {

    private val comparator : IHistoryEntryComparator = HistoryEntryComparatorImpl()

    @Before
    fun prepare() {
        mockLogcat()
    }

    @Test
    fun testCompareSameChapterNear() {
        val entry1 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 0, verseId = 2, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 2, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(true, comparator.areHistoryEntriesSimilar(entry1, entry2))
    }

    @Test
    fun testCompareSameChapterFar() {
        val entry1 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 0, verseId = 2, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 20, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(false, comparator.areHistoryEntriesSimilar(entry1, entry2))
    }

    @Test
    fun testCompareDifferentChapter() {
        val entry1 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 0, verseId = 2, bookNr = 1, bookName = "1. Mose", chapterNr = 2, verseNr = 2, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(false, comparator.areHistoryEntriesSimilar(entry1, entry2))
    }

    @Test
    fun testCompareDifferentBook() {
        val entry1 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 2, bookName = "1. Mose", chapterNr = 2, verseNr = 2, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(false, comparator.areHistoryEntriesSimilar(entry1, entry2))
    }

    @Test
    fun testIsStartOfSameBook() {
        val entryNew = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 10, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(true, comparator.isHistoryEntryStartOfSameBook(entryNew, entry2))
    }

    @Test
    fun testIsNotStartOfSameBookByBibleId() {
        // different bible ID
        val entryNew = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 1, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(false, comparator.isHistoryEntryStartOfSameBook(entryNew, entry2))
    }

    @Test
    fun testIsNotStartOfSameBookByBookNr() {
        // different bible ID
        val entryNew = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 2, bookName = "2. Mose", chapterNr = 1, verseNr = 1, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(false, comparator.isHistoryEntryStartOfSameBook(entryNew, entry2))
    }

    @Test
    fun testIsNotStartOfSameBookByChapterNr() {
        // different bible ID
        val entryNew = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 10, verseNr = 1, historyEntryId = 1, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 0, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 2, verseNr = 1, historyEntryId = 2, timeStamp = System.currentTimeMillis())

        assertEquals(false, comparator.isHistoryEntryStartOfSameBook(entryNew, entry2))
    }
}