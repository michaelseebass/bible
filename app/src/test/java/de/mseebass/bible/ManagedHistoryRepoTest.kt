package de.mseebass.bible

import de.mseebass.bible.logic.HistoryEntryComparatorImpl
import de.mseebass.bible.logic.IHistoryEntryComparator
import de.mseebass.bible.misc.buildHistoryEntryList
import de.mseebass.bible.misc.getHistoryEntry
import de.mseebass.bible.misc.mockLogcat
import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.repository.IHistoryRepository
import de.mseebass.bible.repository.ManagedHistoryRepository
import de.mseebass.bible.repository.Success
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ManagedHistoryRepoTest {

    private val sourceRepo = mockk<IHistoryRepository>()
    private val mockComparator = mockk<IHistoryEntryComparator>()
    private val realComparator = HistoryEntryComparatorImpl()
    private val MAX_SIZE = 50
    private val initialSize = MAX_SIZE - 2
    private var sourceList = buildHistoryEntryList(size = initialSize, diff = 15)

    private val repo = ManagedHistoryRepository(sourceRepo, mockComparator, MAX_SIZE)


    @Before
    fun prepare() {
        println("[ManagedHistoryRepoTest] prepare()")
        mockLogcat()

        coEvery { sourceRepo.getHistoryEntries() } returns Success(sourceList)
        coEvery { sourceRepo.insert(any()) } answers { sourceList.add(firstArg()) }
        coEvery { sourceRepo.delete(any()) } answers { sourceList.remove(firstArg()) }
    }

    private fun mockForSimilarity() {
        // similarity with real comparison
        val slot1 = slot<HistoryEntry>()
        val slot2 = slot<HistoryEntry>()
        every {
            mockComparator.areHistoryEntriesSimilar(capture(slot1), capture(slot2))
        } answers {
            realComparator.areHistoryEntriesSimilar(slot1.captured, slot2.captured)
        }

        // check for previous always false
        every { mockComparator.isHistoryEntryStartOfSameBook(any(), any()) } returns false
    }

    private fun mockForPrevious() {
        every { mockComparator.areHistoryEntriesSimilar(any(), any()) } returns false

        val slot1 = slot<HistoryEntry>()
        val slot2 = slot<HistoryEntry>()
        every {
            mockComparator.isHistoryEntryStartOfSameBook(capture(slot1), capture(slot2))
        } answers {
            realComparator.isHistoryEntryStartOfSameBook(slot1.captured, slot2.captured)
        }
    }

    @Test
    fun givenOtherEntry_insert_shouldNotDelete() {
        mockForSimilarity()
        // given other entry
        val entry = getHistoryEntry(bibleId = 1001, verseId = 2, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 2, historyEntryId = 4, timeStamp = System.currentTimeMillis())
        // do
        runBlocking {
            repo.insert(entry)
        }
        // should insert and delete nothing
        assertEquals(initialSize + 1, sourceList.size)
        assertEquals(true, sourceList.last() == entry)
        coVerify { sourceRepo.insert(entry) }
        coVerify(inverse = true) { sourceRepo.delete(any()) }
    }

    @Test
    fun givenSimilarEntry_insert_shouldDelete() {
        mockForSimilarity()
        // given similar entry
       val entry = getHistoryEntry(bibleId = 1000, verseId = 2, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 2, historyEntryId = 3, timeStamp = System.currentTimeMillis())
        // do
        runBlocking {
            repo.insert(entry)
        }
        // should delete and insert
        assertEquals(initialSize, sourceList.size)
        assertEquals(true, sourceList.last() == entry)
        coVerify { sourceRepo.insert(entry) }
        coVerify { sourceRepo.delete(any()) }
    }

    @Test
    fun testMaxLength() {
        mockForSimilarity()
        // given
        val entry1 = getHistoryEntry(bibleId = 1001, verseId = 2, bookNr = 1, bookName = "1. Mose", chapterNr = 1, verseNr = 2, historyEntryId = 50000, timeStamp = System.currentTimeMillis())
        val entry2 = getHistoryEntry(bibleId = 1001, verseId = 2, bookNr = 2, bookName = "1. Mose", chapterNr = 1, verseNr = 2, historyEntryId = 50001, timeStamp = System.currentTimeMillis())
        val entry3 = getHistoryEntry(bibleId = 1001, verseId = 2, bookNr = 3, bookName = "1. Mose", chapterNr = 1, verseNr = 2, historyEntryId = 50002, timeStamp = System.currentTimeMillis())

        runBlocking { repo.insert(entry1) }
        // should insert and delete nothing
        assertEquals(initialSize + 1, sourceList.size)
        coVerify { sourceRepo.insert(entry1) }
        coVerify(inverse = true) { sourceRepo.delete(any()) }

        runBlocking { repo.insert(entry2) }
        // should insert and delete nothing
        assertEquals(MAX_SIZE, sourceList.size)
        coVerify { sourceRepo.insert(entry2) }
        coVerify(inverse = true) { sourceRepo.delete(any()) }

        runBlocking { repo.insert(entry3) }
        // should insert and delete
        assertEquals(MAX_SIZE, sourceList.size)
        coVerify { sourceRepo.insert(entry3) }
        coVerify { sourceRepo.delete(any()) }
    }

    @Test
    fun testDeleteFirstOfChapter() {
        mockForPrevious()

        val entry1 = getHistoryEntry(bibleId = 1000, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1,
                verseNr = 1, historyEntryId = 0, timeStamp = System.currentTimeMillis() - 50000)
        val entry2 = getHistoryEntry(bibleId = 1000, verseId = 20, bookNr = 2, bookName = "2. Mose", chapterNr = 1,
                verseNr = 1, historyEntryId = 0, timeStamp = System.currentTimeMillis() - 20000)

        sourceList.clear()
        sourceList.add(entry1)
        sourceList.add(entry2)

        val entryNew = getHistoryEntry(bibleId = 1000, verseId = 21, bookNr = 2, bookName = "2. Mose", chapterNr = 15,
                        verseNr = 1, historyEntryId = 0, timeStamp = System.currentTimeMillis())
        // do
        runBlocking {
            repo.insert(entryNew)
        }
        // should delete and insert
        assertEquals(2, sourceList.size)
        assertEquals(true, sourceList[0] == entry1)
        assertEquals(true, sourceList[1] == entryNew)
        coVerify { sourceRepo.insert(entryNew) }
        coVerify { sourceRepo.delete(entry2) }
    }

    // tests if you are at Chapter 15 and then go back to Chapter 1 directly
    @Test
    fun testGoBackToFirstChapter() {
        mockForPrevious()

        val entry1 = getHistoryEntry(bibleId = 1000, verseId = 1, bookNr = 1, bookName = "1. Mose", chapterNr = 1,
                verseNr = 1, historyEntryId = 0, timeStamp = System.currentTimeMillis() - 50000)
        val entry2 = getHistoryEntry(bibleId = 1000, verseId = 21, bookNr = 2, bookName = "2. Mose", chapterNr = 15,
                verseNr = 1, historyEntryId = 0, timeStamp = System.currentTimeMillis() - 20000)

        sourceList.clear()
        sourceList.add(entry1)
        sourceList.add(entry2)

        val entryNew = getHistoryEntry(bibleId = 1000, verseId = 20, bookNr = 2, bookName = "2. Mose", chapterNr = 1,
                verseNr = 1, historyEntryId = 0, timeStamp = System.currentTimeMillis())

        // do
        runBlocking {
            repo.insert(entryNew)
        }
        // should insert and delete nothing
        assertEquals(3, sourceList.size)
        assertEquals(true, sourceList[0] == entry1)
        assertEquals(true, sourceList[1] == entry2)
        assertEquals(true, sourceList[2] == entryNew)
        coVerify { sourceRepo.insert(entryNew) }
        coVerify(inverse = true) { sourceRepo.delete(any()) }
    }

    @After
    fun tearDown() {
        println("[ManagedHistoryRepoTest] tearDown()")
        unmockkAll()
    }
}