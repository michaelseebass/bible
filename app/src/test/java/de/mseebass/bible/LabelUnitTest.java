package de.mseebass.bible;

import org.junit.Test;

import de.mseebass.bible.models.Label;
import de.mseebass.bible.models.Verse;

import static org.junit.Assert.assertEquals;

/**
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LabelUnitTest {

    @Test
    public void testMerge() {
        Verse v1 = new Verse(0,0,1, "1. Mose", 1, 1, "Text1");
        Verse v2 = new Verse(0,0,1, "1. Mose", 1, 2, "Text2");
        Verse v3 = new Verse(0,0,1, "1. Mose", 1, 3, "Text3");

        Label label1 = new Label("Allgemein");
//        label1.addBookMark(v1);
//        label1.addBookMark(v2);
//        label1.addBookMark(v3);

        Label label2 = new Label("Allgemein2");
//        label2.addBookMark(v1);

        try {
            v1.addLabel(label1.getName());
            v1.addLabel(label2.getName());

            v2.addLabel(label1.getName());

            v3.addLabel(label2.getName());
        } catch (Verse.DuplicateLabelException | Verse.IllegalLabelFormatException e) {
            e.printStackTrace();
        }


        v1.mergeLabelsAndColor(v2);
        assertEquals(2, v1.getLabelCount());

        v2.mergeLabelsAndColor(v3);
        assertEquals(2, v2.getLabelCount());
        assertEquals(1, v3.getLabelCount());

        v2.mergeLabelsAndColor(v1);
        assertEquals(2, v2.getLabelCount());
        //assertEquals(1, v1.getLabelCount());
    }

}