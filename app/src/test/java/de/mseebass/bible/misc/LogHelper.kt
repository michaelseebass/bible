package de.mseebass.bible.misc

import android.util.Log
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.slot

fun mockLogcat() {
    println("mockLogcat() --> Calls to BibleLog are mocked by println()")
    mockkStatic(Log::class)

    val slot = slot<String>()
    every { Log.v(any(), capture(slot)) } answers { logTest(slot.captured) }
    every { Log.v(any(), capture(slot), any()) } answers { logTest(slot.captured) }

    every { Log.d(any(), capture(slot)) } answers { logTest(slot.captured) }
    every { Log.d(any(), capture(slot), any()) } answers { logTest(slot.captured) }

    every { Log.i(any(), capture(slot)) } answers { logTest(slot.captured) }
    every { Log.i(any(), capture(slot), any()) } answers { logTest(slot.captured) }

    every { Log.w(any(), capture(slot)) } answers { logTest(slot.captured) }
    every { Log.w(any(), any<Throwable>()) } answers { logTest("not supported by mocking") }
    every { Log.w(any(), capture(slot), any()) } answers { logTest(slot.captured) }

    every { Log.e(any(), capture(slot)) } answers { logTest(slot.captured) }
    every { Log.e(any(), capture(slot), any()) } answers { logTest(slot.captured) }

    every { Log.wtf(any(), capture(slot)) } answers { logTest(slot.captured) }
    every { Log.wtf(any(), any<Throwable>()) } answers { logTest("not supported by mocking") }
    every { Log.wtf(any(), capture(slot), any()) } answers { logTest(slot.captured) }
}

fun logTest(string: String): Int {
    println(string)
    return 0
}