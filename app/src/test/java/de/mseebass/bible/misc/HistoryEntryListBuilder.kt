package de.mseebass.bible.misc

import de.mseebass.bible.models.HistoryEntry
import de.mseebass.bible.models.Verse

fun buildHistoryEntryList(size: Int, diff: Int) : ArrayList<HistoryEntry> {
    val result = ArrayList<HistoryEntry>()

    for (i in 0 until size) {
        val verseNr = (i * diff) + 1
        result.add(
            getHistoryEntry(
                    bibleId = 1000,
                    verseId = verseNr.toLong(),
                    bookNr = 1,
                    bookName = "1. Mose",
                    chapterNr = 1,
                    verseNr = verseNr,
                    historyEntryId = i.toLong(),
                    timeStamp = System.currentTimeMillis()
            )
        )
    }

    return result
}


fun getHistoryEntry(bibleId: Long, verseId: Long, bookNr: Int, bookName: String, chapterNr: Int, verseNr: Int, historyEntryId: Long, timeStamp: Long) : HistoryEntry {
    return HistoryEntry(bibleId, verseId, timeStamp, 0).apply {
        id = historyEntryId
        verse = Verse(id = verseId, bibleId = bibleId, bookNumber = bookNr, bookName = bookName, chapterNumber = chapterNr, number = verseNr, text = "test2")
    }
}